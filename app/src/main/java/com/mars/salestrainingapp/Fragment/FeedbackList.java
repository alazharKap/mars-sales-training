package com.mars.salestrainingapp.Fragment;


import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.adevole.customresources.CustomTextView;
import com.mars.salestrainingapp.Model.FeedbackModel;
import com.mars.salestrainingapp.Model.QuestionaryCategoryModel;
import com.mars.salestrainingapp.R;

import de.codecrafters.tableview.TableView;
import de.codecrafters.tableview.model.TableColumnWeightModel;
import de.codecrafters.tableview.toolkit.SimpleTableHeaderAdapter;
import de.codecrafters.tableview.toolkit.TableDataRowBackgroundProviders;
import io.realm.Realm;
import io.realm.RealmResults;

/**
 * Created by surve on 15-Dec-17.
 */

public class FeedbackList extends Fragment {

    int scheduleId = 0;

    private TableView table;
    RealmResults<QuestionaryCategoryModel> categoryResult;
    private CustomTextView feedbackByText;
    private RecyclerView recyclerView;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_feedback_list, null);
        recyclerView = (RecyclerView) rootView.findViewById(R.id.recycle);
        feedbackByText = (CustomTextView) rootView.findViewById(R.id.feedbackByText);
        return rootView;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        Bundle extra = getArguments();
        scheduleId = extra.getInt("scheduleId", 0);

        Realm.init(getContext());
        Realm realm = Realm.getDefaultInstance();
        categoryResult = realm.where(QuestionaryCategoryModel.class).equalTo("isDeleted",0).findAll();

        if (!categoryResult.isEmpty()) {
            setRecycle();
            FeedbackModel model = realm.where(FeedbackModel.class).equalTo("scheduleId", scheduleId).findFirst();
            feedbackByText.setText("By "+model.getBossName());
        }
    }

    private void setRecycle() {
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        recyclerView.setLayoutManager(new LinearLayoutManager(getActivity().getApplicationContext()));
   //     recyclerView.setAdapter(new FeedbackDataAdapter(getContext(), categoryResult, scheduleId));
    }

    private void setTable() {

        final String[] TABLE_HEADERS = { "Description", "Value" };

        TableColumnWeightModel columnModel = new TableColumnWeightModel(2);
        columnModel.setColumnCount(2);
        columnModel.setColumnWeight(0, 2);
        columnModel.setColumnWeight(1, 1);
        table.setColumnModel(columnModel);

        int colorEvenRows = getResources().getColor(R.color.white);
        int colorOddRows = getResources().getColor(R.color.cardview_light_background);
        table.setDataRowBackgroundProvider(TableDataRowBackgroundProviders.alternatingRowColors(colorEvenRows, colorOddRows));
        table.setHeaderAdapter(new SimpleTableHeaderAdapter(getContext(), TABLE_HEADERS));

        setTableData();
    }
    private void setTableData() {

//        List <FeedbackModel> feedbackCategory = categoryResult;
//        final String[][] DATA_TO_SHOW = new String[feedbackCategory.size()][2];
//
//        for (int i = 0; i< feedbackCategory.size(); i++){
//            DATA_TO_SHOW[i] = new String[]{feedbackCategory.get(i).getQuestion(), String.valueOf(feedbackCategory.get(i).getRating())};
//        }
//        table.setDataAdapter(new SimpleTableDataAdapter(getContext(), DATA_TO_SHOW));

    }
}
