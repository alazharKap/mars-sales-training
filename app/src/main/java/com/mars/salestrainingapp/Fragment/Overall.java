package com.mars.salestrainingapp.Fragment;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;

import com.adevole.customresources.CustomTextView;
import com.mars.salestrainingapp.API.APIRequest;
import com.mars.salestrainingapp.Adapter.OverallAdapter;
import com.mars.salestrainingapp.Model.OverallModel;
import com.mars.salestrainingapp.R;

import java.util.List;

import de.codecrafters.tableview.TableView;
import de.codecrafters.tableview.model.TableColumnWeightModel;
import de.codecrafters.tableview.toolkit.SimpleTableHeaderAdapter;
import de.codecrafters.tableview.toolkit.TableDataRowBackgroundProviders;

import static com.mars.salestrainingapp.API.APIRequest.getOverallData;

/**
 * Created by surve on 15-Dec-17.
 */

public class Overall extends Fragment {

    int empId;
    LinearLayout frame;
    private CustomTextView periodText;
    private RecyclerView recyclerView;
    private TableView table;

    CustomTextView gsvvalueText, productivityvalueText, visitvalueText, lastvalueText, focusvalueText, incentivevalueText, accvalueText,
            gsvvalueText2, productivityvalueText2, visitvalueText2, lastvalueText2, focusvalueText2, incentivevalueText2, accvalueText2,
            gsvvalueText3, productivityvalueText3, visitvalueText3, lastvalueText3, focusvalueText3, incentivevalueText3, accvalueText3;

    private CustomTextView date1, date2, date3;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_overall, null);
        frame = (LinearLayout) rootView.findViewById(R.id.frame);
        periodText = (CustomTextView) rootView.findViewById(R.id.periodText);
        recyclerView = (RecyclerView) rootView.findViewById(R.id.recycle);
        //   table = (TableView) rootView.findViewById(R.id.table);


        //   entered by vinay

        date1 = (CustomTextView) rootView.findViewById(R.id.date_1);
        date2 = (CustomTextView) rootView.findViewById(R.id.date_2);
        date3 = (CustomTextView) rootView.findViewById(R.id.date_3);


        gsvvalueText = (CustomTextView) rootView.findViewById(R.id.gsvvalueText);
        productivityvalueText = (CustomTextView) rootView.findViewById(R.id.productivityvalueText);
        visitvalueText = (CustomTextView) rootView.findViewById(R.id.visitvalueText);
        lastvalueText = (CustomTextView) rootView.findViewById(R.id.lastvalueText);
        focusvalueText = (CustomTextView) rootView.findViewById(R.id.focusvalueText);
        incentivevalueText = (CustomTextView) rootView.findViewById(R.id.incentivevalueText);
        accvalueText = (CustomTextView) rootView.findViewById(R.id.accvalueText);

        gsvvalueText2 = (CustomTextView) rootView.findViewById(R.id.gsvvalueText2);
        productivityvalueText2 = (CustomTextView) rootView.findViewById(R.id.productivityvalueText2);
        visitvalueText2 = (CustomTextView) rootView.findViewById(R.id.visitvalueText2);
        lastvalueText2 = (CustomTextView) rootView.findViewById(R.id.lastvalueText2);
        focusvalueText2 = (CustomTextView) rootView.findViewById(R.id.focusvalueText2);
        incentivevalueText2 = (CustomTextView) rootView.findViewById(R.id.incentivevalueText2);
        accvalueText2 = (CustomTextView) rootView.findViewById(R.id.accvalueText2);

        gsvvalueText3 = (CustomTextView) rootView.findViewById(R.id.gsvvalueText3);
        productivityvalueText3 = (CustomTextView) rootView.findViewById(R.id.productivityvalueText3);
        visitvalueText3 = (CustomTextView) rootView.findViewById(R.id.visitvalueText3);
        lastvalueText3 = (CustomTextView) rootView.findViewById(R.id.lastvalueText3);
        focusvalueText3 = (CustomTextView) rootView.findViewById(R.id.focusvalueText3);
        incentivevalueText3 = (CustomTextView) rootView.findViewById(R.id.incentivevalueText3);
        accvalueText3 = (CustomTextView) rootView.findViewById(R.id.accvalueText3);
        return rootView;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        Bundle extra = getArguments();
        empId = extra.getInt("emp_id", 0);

        //   setTable();

        APIRequest apiRequest = new APIRequest();
        apiRequest.setOverAllResponseListener(new APIRequest.OverAllResponse() {
            @Override
            public void onOverAllReceived(List<OverallModel> list) {
//                setRecycle(list);
                setTableData(list);
            }
        });
        getOverallData(frame, getContext(), empId);
    }

    private void setRecycle(List<OverallModel> list) {
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        recyclerView.setLayoutManager(new LinearLayoutManager(getActivity().getApplicationContext()));
        recyclerView.setAdapter(new OverallAdapter(getContext(), list));
    }

    private void setTable() {

        final String[] TABLE_HEADERS = {"Description", "Value"};

        TableColumnWeightModel columnModel = new TableColumnWeightModel(4);
        columnModel.setColumnCount(2);
        columnModel.setColumnWeight(1, 1);
        columnModel.setColumnWeight(2, 1);
        table.setColumnModel(columnModel);

        int colorEvenRows = getResources().getColor(R.color.white);
        int colorOddRows = getResources().getColor(R.color.cardview_light_background);
        table.setDataRowBackgroundProvider(TableDataRowBackgroundProviders.alternatingRowColors(colorEvenRows, colorOddRows));

        table.setHeaderAdapter(new SimpleTableHeaderAdapter(getContext(), TABLE_HEADERS));
    }

    private void setTableData(List<OverallModel> list) {



        try {
            String dateValue1 = list.get(0).getUpdated_at();
            String[] separated1 = dateValue1.split("-");
            date1.setText(separated1[0] + "-" + separated1[1]);
        }
        catch (Exception e)
        {

        }
        try {
            String dateValue2 = list.get(1).getUpdated_at();
            String[] separated2 = dateValue2.split("-");
            date2.setText(separated2[0] + "-" + separated2[1]);
        }
        catch (Exception e)
        {

        }
        try {
            String dateValue3 = list.get(2).getUpdated_at();
            String[] separated3 = dateValue3.split("-");
            date3.setText(separated3[0] + "-" + separated3[1]);
        }
        catch (Exception e)
        {

        }

        gsvvalueText.setText(list.get(0).getGsv());
        productivityvalueText.setText(list.get(0).getProductivity());
        visitvalueText.setText(list.get(0).getVisit_overage());
        lastvalueText.setText(list.get(0).getLast_qtr_ratings());
        focusvalueText.setText(list.get(0).getFoucs_course());
        incentivevalueText.setText(list.get(0).getIncentive() + "");
        accvalueText.setText(list.get(0).getAcc_addition() + "");

        gsvvalueText2.setText(list.get(1).getGsv());
        productivityvalueText2.setText(list.get(1).getProductivity());
        visitvalueText2.setText(list.get(1).getVisit_overage());
        lastvalueText2.setText(list.get(1).getLast_qtr_ratings());
        focusvalueText2.setText(list.get(1).getFoucs_course());
        incentivevalueText2.setText(list.get(1).getIncentive() + "");
        accvalueText2.setText(list.get(1).getAcc_addition() + "");


        gsvvalueText3.setText(list.get(2).getGsv());
        productivityvalueText3.setText(list.get(2).getProductivity());
        visitvalueText3.setText(list.get(2).getVisit_overage());
        lastvalueText3.setText(list.get(2).getLast_qtr_ratings());
        focusvalueText3.setText(list.get(2).getFoucs_course());
        incentivevalueText3.setText(list.get(2).getIncentive() + "");
        accvalueText3.setText(list.get(2).getAcc_addition() + "");

      /*  final String[][] DATA_TO_SHOW = new String[7][2];
        DATA_TO_SHOW[0][0] = "GSV";
        DATA_TO_SHOW[0][1] = list.get(0).getGsv();
        DATA_TO_SHOW[1][0] = "Productivity";
        DATA_TO_SHOW[1][1] = list.get(0).getProductivity();
        DATA_TO_SHOW[2][0] = "Visit Overage";
        DATA_TO_SHOW[2][1] = list.get(0).getVisit_overage();
        DATA_TO_SHOW[3][0] = "Last QTR Ratings";
        DATA_TO_SHOW[3][1] = list.get(0).getLast_qtr_ratings();
        DATA_TO_SHOW[4][0] = "Foucs Course";
        DATA_TO_SHOW[4][1] = list.get(0).getFoucs_course();
        DATA_TO_SHOW[5][0] = "Incentive";
        DATA_TO_SHOW[5][1] = String.valueOf(list.get(0).getIncentive());
        DATA_TO_SHOW[6][0] = "ACC Addition";
        DATA_TO_SHOW[6][1] = String.valueOf(list.get(0).getAcc_addition());

        table.setDataAdapter(new SimpleTableDataAdapter(getContext(), DATA_TO_SHOW));

        periodText.setText(list.get(0).getPeriod_id()+"");*/
    }
}