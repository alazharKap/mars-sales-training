package com.mars.salestrainingapp.Fragment;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.constraint.ConstraintLayout;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.adevole.customresources.CustomTextView;
import com.mars.salestrainingapp.API.APIRequest;
import com.mars.salestrainingapp.Adapter.FeedbackDataAdapter;
import com.mars.salestrainingapp.Adapter.PreviousViewPagerAdapter;
import com.mars.salestrainingapp.Model.FeedbackModel;
import com.mars.salestrainingapp.Model.QuestionaryCategoryModel;
import com.mars.salestrainingapp.R;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TreeSet;

import io.realm.Realm;
import io.realm.RealmResults;

import static com.mars.salestrainingapp.API.APIRequest.getRecentFeedback;
import static com.mars.salestrainingapp.Helper.Utility.log;

/**
 * Created by surve on 15-Dec-17.
 */

public class Feedback extends Fragment {

    private static final String TAG = "Feedback";

    ConstraintLayout frame;
    ViewPager feedbackViewpager;
    private TabLayout tabLayout;
    RealmResults<QuestionaryCategoryModel> categoryResult;
    private RecyclerView recyclerView;
    int scheduleId;

    int empId;
    public static List<FeedbackModel> feedbackModel;
    ArrayList<String> listDates = new ArrayList<String>();

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
       View rootView = inflater.inflate(R.layout.fragment_feedback, null);
  //      View rootView = inflater.inflate(R.layout.row_feedback_cat, null);
        frame = (ConstraintLayout) rootView.findViewById(R.id.frame);
//        feedbackViewpager = (ViewPager) rootView.findViewById(R.id.feedbackViewpager);
//        tabLayout = (TabLayout) rootView.findViewById(R.id.feedbackTab);
        recyclerView = (RecyclerView) rootView.findViewById(R.id.questions_recycle);
        return rootView;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        Bundle extra = getArguments();
        empId = extra.getInt("emp_id", 0);
        scheduleId = extra.getInt("scheduleId", 0);

        APIRequest apiRequest = new APIRequest();
        apiRequest.setOnFeedbackListener(new APIRequest.FeedbackResponse() {
            @Override
            public void onFeedbackSubmitted(boolean status) {

            }

            @Override
            public void onFeedbackReceived(List<FeedbackModel> list) {
                if (!list.isEmpty()) {
                    PreviousViewPagerAdapter adapter = new PreviousViewPagerAdapter(getChildFragmentManager());

                    HashMap<String, String> scheduleFilter = new HashMap<>();

                    for (FeedbackModel model : list) {
                        scheduleFilter.put(model.getScheduleDate(), "scheduleId");
                        log(TAG,"Schedule Id "+model.getScheduleId());
                    }

                    Set<Map.Entry<String, String>> scheduleSet = scheduleFilter.entrySet();

                    Log.d(" UnSSorted Set ", scheduleSet.toString());
                    TreeSet<Map.Entry<String, String>> ts
                            = new TreeSet<Map.Entry<String, String>>(new Comparator<Map.Entry<String, String>>() {
                        @Override
                        public int compare(Map.Entry<String, String> entry1, Map.Entry<String, String> entry2) {
                            return entry1.getKey().compareTo(entry2.getKey());
                        }
                    });

                    ts.addAll(scheduleSet);

                    Log.d(" Sorted Set ", ts.toString());
                    for (Map.Entry<String, String> scheduleEntry : ts) {

                        Log.d("Date ", String.valueOf(scheduleEntry.getKey()));


                        listDates.add(String.valueOf(scheduleEntry.getKey()));

                        Realm.init(getContext());
                        Realm realm = Realm.getDefaultInstance();
                        RealmResults<FeedbackModel> realmResults = realm.where(FeedbackModel.class)
                                .equalTo("scheduleDate", scheduleEntry.getKey())
                                .findAll();

                        if (realmResults.size() > 0) {
                            Bundle extra = new Bundle();
                            extra.putInt("scheduleId", realmResults.get(0).getScheduleId());
                            FeedbackList feedbackList = new FeedbackList();
                            feedbackList.setArguments(extra);

//                            adapter.addFrag(feedbackList, ddMMYYYY(realmResults.get(0).getScheduleDate()));
//                            feedbackViewpager.setAdapter(adapter);
//                            tabLayout.setupWithViewPager(feedbackViewpager);
                        }
                    }

                    Realm.init(getContext());
                    Realm realm = Realm.getDefaultInstance();
                    categoryResult = realm.where(QuestionaryCategoryModel.class).equalTo("isDeleted",0).findAll();

                    if (!categoryResult.isEmpty()) {
                        setRecycle();
                        FeedbackModel model = realm.where(FeedbackModel.class).equalTo("scheduleId", scheduleId).findFirst();
            //            feedbackByText.setText("By "+model.getBossName());
                    }

                }
            }
        });
        getRecentFeedback(frame, getContext(), empId);
    }

    private void setRecycle() {
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        recyclerView.setLayoutManager(new LinearLayoutManager(getActivity().getApplicationContext()));
        recyclerView.setAdapter(new FeedbackDataAdapter(getContext(), categoryResult, 31,listDates));
    }


    private void createTab(int position, String heading) {
        View headerView1 = LayoutInflater.from(getContext()).inflate(R.layout.custom_tab_header, null);
        CustomTextView tabOne = (CustomTextView) headerView1.findViewById(R.id.customHeaderText);

        tabOne.setText(heading);
        tabLayout.getTabAt(position).setCustomView(headerView1);

    }

}