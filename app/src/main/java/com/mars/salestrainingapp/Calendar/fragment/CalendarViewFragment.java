package com.mars.salestrainingapp.Calendar.fragment;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.GridView;
import android.widget.Toast;

import com.mars.salestrainingapp.Calendar.adapter.CalendarGridViewAdapter;
import com.mars.salestrainingapp.Calendar.controller.CalendarDateController;
import com.mars.salestrainingapp.Calendar.data.CalendarDate;
import com.mars.salestrainingapp.Calendar.utils.DateUtils;
import com.mars.salestrainingapp.Helper.Preference;
import com.mars.salestrainingapp.R;

import java.util.ArrayList;
import java.util.List;


/**
 * Created by joybar on 4/27/16.
 */
public class CalendarViewFragment extends Fragment {

    private static final String TAG = "CalendarViewFragment";

    private static final String YEAR = "year";
    private static final String MONTH = "month";
    private static final String CHOICE_MODE_SINGLE = "choice_mode_single";
    private boolean isChoiceModelSingle;
    private int mYear;
    private int mMonth;
    private GridView mGridView;
    private OnDateClickListener onDateClickListener;
    private OnDateCancelListener onDateCancelListener;
    private OnDateLoaded onDateLoaded;
    public List<CalendarDate> mListDataCalendar = new ArrayList<>();

    public CalendarViewFragment() {
    }

    public static CalendarViewFragment newInstance(int year, int month) {
        CalendarViewFragment fragment = new CalendarViewFragment();
        Bundle args = new Bundle();
        args.putInt(YEAR, year);
        args.putInt(MONTH, month);
        fragment.setArguments(args);
        return fragment;
    }

    public static CalendarViewFragment newInstance(int year, int month, boolean isChoiceModelSingle) {
        CalendarViewFragment fragment = new CalendarViewFragment();
        Bundle args = new Bundle();
        args.putInt(YEAR, year);
        args.putInt(MONTH, month);
        args.putBoolean(CHOICE_MODE_SINGLE, isChoiceModelSingle);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        try {
            onDateClickListener = (OnDateClickListener) context;
            onDateLoaded = (OnDateLoaded) context;

            if(!isChoiceModelSingle){
                //多选
                onDateCancelListener = (OnDateCancelListener) context;
            }
        } catch (ClassCastException e) {
            throw new ClassCastException(context.toString() + "must implement OnDateClickListener or OnDateCancelListener");
        }
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mYear = getArguments().getInt(YEAR);
            mMonth = getArguments().getInt(MONTH);
            isChoiceModelSingle = getArguments().getBoolean(CHOICE_MODE_SINGLE, false);
            mListDataCalendar = CalendarDateController.getCalendarDate(mYear, mMonth);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_calendar, container, false);
        mGridView = (GridView) view.findViewById(R.id.gv_calendar);
        return view;
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
//        final List<CalendarDate> finalMListDataCalendar = mListDataCalendar;


        List<CalendarDate> finalMListDataCalendar = onDateLoaded.outOffRange(mListDataCalendar);
        final List<CalendarDate> finalList = onDateLoaded.finalList(mListDataCalendar);
        finalMListDataCalendar = onDateLoaded.onDateLoaded(finalMListDataCalendar);
        mGridView.setAdapter(new CalendarGridViewAdapter(getContext(),finalMListDataCalendar));



        if (isChoiceModelSingle) {
            mGridView.setChoiceMode(GridView.CHOICE_MODE_SINGLE);
        } else {
            mGridView.setChoiceMode(GridView.CHOICE_MODE_MULTIPLE);
        }
        final List<CalendarDate> finalMListDataCalendar1 = finalMListDataCalendar;

        mGridView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

                CalendarDate calendarDate = ((CalendarGridViewAdapter) mGridView.getAdapter()).getListData().get(position);

                if (finalMListDataCalendar1.get(position).isInRange()) {
                    if (finalMListDataCalendar1.get(position).isInThisMonth() && finalMListDataCalendar1.get(position).isInRange()) {


                        if (finalMListDataCalendar1.get(position).isSelect()) {
                            finalMListDataCalendar1.get(position).setSelect(false);
                            onDateCancelListener.onDateCancel(calendarDate);

                        }
                        else {

                            if(finalMListDataCalendar1.get(position).isSameDSR() || finalMListDataCalendar1.get(position).getSelectedBy() == Preference.getInstance(getContext()).getUserId()) {

                                if (finalMListDataCalendar1.get(position).getScheduleStatus() ==0) {
                                    finalMListDataCalendar1.get(position).setRemoveCurrentSelection(true);
                                    onDateCancelListener.onDateCancel(calendarDate);
                                    finalMListDataCalendar1.get(position).setSelect(false);
                                    finalMListDataCalendar1.get(position).setSelectedBy(0);
                                    finalMListDataCalendar1.get(position).setSameDSR(false);
                                    finalMListDataCalendar1.get(position).setUnSelectable(false);
                                }
                                else if (finalMListDataCalendar1.get(position).getScheduleStatus() == 1)
                                    Toast.makeText(getContext(), "Day Already Started", Toast.LENGTH_SHORT).show();
                                else if (finalMListDataCalendar1.get(position).getScheduleStatus() == 2)
                                    Toast.makeText(getContext(), "Day Already Ended", Toast.LENGTH_SHORT).show();
                            }
                            else {
                                finalMListDataCalendar1.get(position).setSelect(true);
                                onDateClickListener.onDateClick(calendarDate);
                            }
                        }

                        mGridView.setAdapter(new CalendarGridViewAdapter(getContext(), finalMListDataCalendar1));
//                        else if (finalMListDataCalendar1.get(position).isUnSelectable()) {
//                            finalMListDataCalendar1.get(position).setRemoveCurrentSelection(true);
//                            onDateCancelListener.onDateCancel(calendarDate);
//                            finalMListDataCalendar1.get(position).setSelectedBy(0);
//                            finalMListDataCalendar1.get(position).setSameDSR(false);
//                            finalMListDataCalendar1.get(position).setUnSelectable(false);
//                            mGridView.setAdapter(new CalendarGridViewAdapter(getContext(), finalMListDataCalendar1));
//                            mGridView.setItemChecked(position, false);
//                        }
                    }
                }else {
                    mGridView.setItemChecked(position, false);
                }

            }

        });
//        mGridView.post(new Runnable() {
//            @Override
//            public void run() {
//                //Need to be selected by default the same day
//                List<CalendarDate> mListData = ((CalendarGridViewAdapter) mGridView.getAdapter()).getListData();
//                int count = mListData.size();
//                for (int i = 0; i < count; i++) {
//                    if (mListData.get(i).getSolar().solarDay == DateUtils.getDay()
//                            && mListData.get(i).getSolar().solarMonth == DateUtils.getMonth()
//                            && mListData.get(i).getSolar().solarYear == DateUtils.getYear()) {
//                        if (null != mGridView.getChildAt(i) && mListData.get(i).isInThisMonth()) {
//                            // mListData.get(i).setIsSelect(true);
//                            onDateClickListener.onDateClick(mListData.get(i));
//                            mGridView.setItemChecked(i, true);
//                        }
//                    }
//                }
//
//            }
//        });
    }

    @Override
    public void onResume() {
        super.onResume();
    }

    @Override
    public void onPause() {
        super.onPause();
    }

    @Override
    public void setUserVisibleHint(boolean isVisibleToUser) {
        super.setUserVisibleHint(isVisibleToUser);
        if (!isVisibleToUser) {
            if (null != mGridView) {
                // mGridView.setItemChecked(mCurrentPosition, false);
//                mGridView.clearChoices();
            }
        }
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
    }

    public interface OnDateClickListener {
        void onDateClick(CalendarDate calendarDate);
    }
    public interface OnDateCancelListener {
        void onDateCancel(CalendarDate calendarDate);
    }

    public interface OnDateLoaded {
        List<CalendarDate> onDateLoaded(List<CalendarDate> finalList);
        void dataLoaded();
        List<CalendarDate> outOffRange(List<CalendarDate> calendarDateList);
        List<CalendarDate> finalList(List<CalendarDate> finalList);
    }

}