package com.mars.salestrainingapp.Model;

import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;

/**
 * Created by Vinod on 11/29/2017.
 */

public class ScheduledOutlet extends RealmObject {
    @PrimaryKey
    private long id;

    private int schedule_id;
    private int outlet_id;
    private float rating;
    private String notes;
    private int spotAward;
    private String spotReason;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public int getSchedule_id() {
        return schedule_id;
    }

    public void setSchedule_id(int schedule_id) {
        this.schedule_id = schedule_id;
    }

    public int getOutlet_id() {
        return outlet_id;
    }

    public void setOutlet_id(int outlet_id) {
        this.outlet_id = outlet_id;
    }

    public float getRating() {
        return rating;
    }

    public void setRating(float rating) {
        this.rating = rating;
    }

    public String getNotes() {
        return notes;
    }

    public void setNotes(String notes) {
        this.notes = notes;
    }

    public int getSpotAward() {
        return spotAward;
    }
    public void setSpotAward(int spotAward) {
        this.spotAward = spotAward;
    }

    public String getSpotReason() {
        return spotReason;
    }

    public void setSpotReason(String spotReason) {
        this.spotReason = spotReason;
    }
}
