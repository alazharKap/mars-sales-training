package com.mars.salestrainingapp.Model;

import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;

/**
 * Created by home on 02-02-2018.
 */

public class TempOutlets extends RealmObject {
    @PrimaryKey
    private int id;
    private int userId;
    private String empId;
    private String region;
    public String outlet_name;
    private String address;
    private int regionPrimaryId;
    private String storeCode;
    private String storeName;
    private String city;
    private String emp_region;

    public int getUserId() {
        return userId;
    }

    public void setUserId(int userId) {
        this.userId = userId;
    }



    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getEmpId() {
        return empId;
    }

    public void setEmpId(String empId) {
        this.empId = empId;
    }

    public String getRegion() {
        return region;
    }

    public void setRegion(String region) {
        this.region = region;
    }

    public String getOutlet_name() {
        return outlet_name;
    }

    public void setOutlet_name(String outlet_name) {
        this.outlet_name = outlet_name;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public int getRegionPrimaryId() {
        return regionPrimaryId;
    }

    public void setRegionPrimaryId(int regionPrimaryId) {
        this.regionPrimaryId = regionPrimaryId;
    }

    public String getStoreCode() {
        return storeCode;
    }

    public void setStoreCode(String storeCode) {
        this.storeCode = storeCode;
    }

    public String getStoreName() {
        return storeName;
    }

    public void setStoreName(String storeName) {
        this.storeName = storeName;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getEmp_region() {
        return emp_region;
    }

    public void setEmp_region(String emp_region) {
        this.emp_region = emp_region;
    }
}

