package com.mars.salestrainingapp.Model;

/**
 * Created by surve on 16-Dec-17.
 */

public class OverallModel {

    private int id;
    private int empId;
    private String empCode;
    private String name;
    private String gsv;
    private String productivity;
    private String visit_overage;
    private String last_qtr_ratings;
    private String foucs_course;
    private int incentive;
    private int acc_addition;
    private int period_id;
    private String year;
    private String updated_at;

    public String getUpdated_at() {
        return updated_at;
    }

    public void setUpdated_at(String updated_at) {
        this.updated_at = updated_at;
    }



    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getEmpId() {
        return empId;
    }

    public void setEmpId(int empId) {
        this.empId = empId;
    }

    public String getEmpCode() {
        return empCode;
    }

    public void setEmpCode(String empCode) {
        this.empCode = empCode;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getGsv() {
        return gsv;
    }

    public void setGsv(String gsv) {
        this.gsv = gsv;
    }

    public String getProductivity() {
        return productivity;
    }

    public void setProductivity(String productivity) {
        this.productivity = productivity;
    }

    public String getVisit_overage() {
        return visit_overage;
    }

    public void setVisit_overage(String visit_overage) {
        this.visit_overage = visit_overage;
    }

    public String getLast_qtr_ratings() {
        return last_qtr_ratings;
    }

    public void setLast_qtr_ratings(String last_qtr_ratings) {
        this.last_qtr_ratings = last_qtr_ratings;
    }

    public String getFoucs_course() {
        return foucs_course;
    }

    public void setFoucs_course(String foucs_course) {
        this.foucs_course = foucs_course;
    }

    public int getIncentive() {
        return incentive;
    }

    public void setIncentive(int incentive) {
        this.incentive = incentive;
    }

    public int getAcc_addition() {
        return acc_addition;
    }

    public void setAcc_addition(int acc_addition) {
        this.acc_addition = acc_addition;
    }

    public int getPeriod_id() {
        return period_id;
    }

    public void setPeriod_id(int period_id) {
        this.period_id = period_id;
    }

    public String getYear() {
        return year;
    }

    public void setYear(String year) {
        this.year = year;
    }
}
