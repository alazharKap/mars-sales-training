package com.mars.salestrainingapp.Model;

import java.util.Date;

import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;

/**
 * Created by surve on 29-Nov-17.
 */

public class ScheduleModel extends RealmObject{

    @PrimaryKey
    private int id;

    private int salesmanId;
    private int bossId;
    private Date scheduleDate;
    private int period;
    private int status;  //If status = 0 no action performed, If status = 1 day start, If status = 2 day ended
    private int questionFlag; //If 0 question is not answered, If 1 question is answered
    private int isDeleted;

    public int getId() {
        return id;
    }
    public void setId(int id) {
        this.id = id;
    }

    public int getSalesmanId() {
        return salesmanId;
    }
    public void setSalesmanId(int salesmanId) {
        this.salesmanId = salesmanId;
    }

    public int getBossId() {
        return bossId;
    }
    public void setBossId(int bossId) {
        this.bossId = bossId;
    }

    public Date getScheduleDate() {
        return scheduleDate;
    }
    public void setScheduleDate(Date scheduleDate) {
        this.scheduleDate = scheduleDate;
    }

    public int getPeriod() {
        return period;
    }
    public void setPeriod(int period) {
        this.period = period;
    }

    public int getStatus() {
        return status;
    }
    public void setStatus(int status) {
        this.status = status;
    }

    public int getQuestionFlag() {
        return questionFlag;
    }
    public void setQuestionFlag(int questionFlag) {
        this.questionFlag = questionFlag;
    }

    public int getIsDeleted() {
        return isDeleted;
    }

    public void setIsDeleted(int isDeleted) {
        this.isDeleted = isDeleted;
    }
}
