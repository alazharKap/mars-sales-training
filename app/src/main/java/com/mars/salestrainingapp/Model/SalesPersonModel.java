package com.mars.salestrainingapp.Model;

import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;

/**
 * Created by surve on 28-Oct-17.
 */

public class SalesPersonModel extends RealmObject {

    @PrimaryKey
    private int empid;

    private String user_name;
    private int no_of_days;
    private int superior_emp_id;
    private int scheduledCount;
    private int isDeleted;
    private int flag;

    private int quadrant_id;
    public int getQuadrant_id() {
        return quadrant_id;
    }

    public void setQuadrant_id(int quadrant_id) {
        this.quadrant_id = quadrant_id;
    }



    public int getEmpid() {
        return empid;
    }
    public void setEmpid(int empid) {
        this.empid = empid;
    }

    public String getUser_name() {
        return user_name;
    }
    public void setUser_name(String user_name) {
        this.user_name = user_name;
    }

    public int getNo_of_days() {
        return no_of_days;
    }
    public void setNo_of_days(int no_of_days) {
        this.no_of_days = no_of_days;
    }

    public int getScheduledCount() {
        return scheduledCount;
    }
    public void setScheduledCount(int scheduledCount) {
        this.scheduledCount = scheduledCount;
    }

    public int getSuperior_emp_id() {
        return superior_emp_id;
    }
    public void setSuperior_emp_id(int superior_emp_id) {
        this.superior_emp_id = superior_emp_id;
    }

    public int getIsDeleted() {
        return isDeleted;
    }
    public void setIsDeleted(int isDeleted) {
        this.isDeleted = isDeleted;
    }

    public int getFlag() {
        return flag;
    }
    public void setFlag(int flag) {
        this.flag = flag;
    }
}
