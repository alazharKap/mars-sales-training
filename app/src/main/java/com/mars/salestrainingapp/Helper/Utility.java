package com.mars.salestrainingapp.Helper;

import android.content.Context;
import android.os.Build;
import android.support.design.widget.Snackbar;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.mars.salestrainingapp.R;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

/**
 * Created by surve on 16-Oct-17.
 */

public class Utility {

    private static final boolean d = true;

    public static void log(String TAG, Object o) {
        if (d) android.util.Log.d(TAG,o.toString());
    }

    public static void toast(View v, Context context, Object o) {
        Snackbar snackbar;
        snackbar = Snackbar.make(v.findViewById(R.id.frame), o.toString(), Snackbar.LENGTH_SHORT);
        View snackBarView = snackbar.getView();
        TextView textView = (TextView) snackBarView.findViewById(android.support.design.R.id.snackbar_text);

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            textView.setTextColor(context.getColor(R.color.white));
            snackBarView.setBackgroundColor(context.getColor(R.color.gray));
        }
        else {
            snackBarView.setBackgroundColor(context.getResources().getColor(R.color.gray));
            textView.setTextColor(context.getResources().getColor(R.color.white));
        }

        snackbar.show();
    }

    public static void longToast(View v, Context context, Object o) {
        Snackbar snackbar;
        snackbar = Snackbar.make(v.findViewById(R.id.frame), o.toString(), Snackbar.LENGTH_INDEFINITE);
        View snackBarView = snackbar.getView();
        TextView textView = (TextView) snackBarView.findViewById(android.support.design.R.id.snackbar_text);

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            textView.setTextColor(context.getColor(R.color.white));
            snackBarView.setBackgroundColor(context.getColor(R.color.gray));
        }
        else {
            snackBarView.setBackgroundColor(context.getResources().getColor(R.color.gray));
            textView.setTextColor(context.getResources().getColor(R.color.white));
        }

        snackbar.show();
    }

    public static void underConstruction(Context context) {
        Toast.makeText(context,"Under Construction",Toast.LENGTH_SHORT).show();
    }

    public static void internetError(View v, Context context) {
        Snackbar snackbar;
        snackbar = Snackbar.make(v.findViewById(R.id.frame), R.string.internet_error, Snackbar.LENGTH_SHORT);
        View snackBarView = snackbar.getView();
        TextView textView = (TextView) snackBarView.findViewById(android.support.design.R.id.snackbar_text);

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            textView.setTextColor(context.getColor(R.color.white));
            snackBarView.setBackgroundColor(context.getColor(R.color.red));
        }
        else {
            snackBarView.setBackgroundColor(context.getResources().getColor(R.color.red));
            textView.setTextColor(context.getResources().getColor(R.color.white));
        }

        snackbar.show();
    }

    public static void responseError(View v,Context context) {
        Snackbar snackbar;
        snackbar = Snackbar.make(v.findViewById(R.id.frame), context.getString(R.string.response_error), Snackbar.LENGTH_SHORT);
        View snackBarView = snackbar.getView();
        TextView textView = (TextView) snackBarView.findViewById(android.support.design.R.id.snackbar_text);

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            textView.setTextColor(context.getColor(R.color.white));
            snackBarView.setBackgroundColor(context.getColor(R.color.red));
        }
        else {
            snackBarView.setBackgroundColor(context.getResources().getColor(R.color.red));
            textView.setTextColor(context.getResources().getColor(R.color.white));
        }

        snackbar.show();
    }

    public static int getPosition(String position) {
        int selection = 0;
        switch(position) {
            case "Received":
                selection = 0;
                break;
            case "Accept":
                selection = 1;
                break;
            case "Dispatch":
                selection = 3;
                break;
            case "Closed":
                selection = 4;
                break;
            case "Decline":
                selection = 5;
                break;
        }
        return selection;
    }

    public static String formatDate(String date) {
        String day = date.split("-")[0];
        String month = date.split("-")[1];
        String year = date.split("-")[2];

        switch (month) {
            case "Jan":
                break;
        }
        return "";
    }

    public static String ddMMYYYY(String YYYYMMdd) {
        return YYYYMMdd.split("-")[2]+"-"+YYYYMMdd.split("-")[1]+"-"+YYYYMMdd.split("-")[0];
    }

    public static String toDDMMYY(String ddMMYYYY){
        String ddmm = ddMMYYYY.substring(0,5);

        String yy = ddMMYYYY.split("-")[2].substring(2);
        String ddmmyy = ddmm.concat("-"+yy);
        return  ddmmyy;
    }

    public static String YYYYMMdd(String ddMMYYYY) {
        if (ddMMYYYY.split("-").length == 3)
            return ddMMYYYY.split("-")[2]+"-"+ddMMYYYY.split("-")[1]+"-"+ddMMYYYY.split("-")[0];
        else
            return ddMMYYYY;
    }

    public static String currentYYYYMMdd() {
        Calendar c = Calendar.getInstance();
        SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd");
        return df.format(c.getTime());
    }

    public static Date convertToDate(String date) {
        Date d = null;
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
        try {
            d = sdf.parse(date);
        } catch (ParseException ex) {
            ex.printStackTrace();
        }
        return d;
    }

    public static String convertToString(Date scheduleDate) {
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
        String reportDate = sdf.format(scheduleDate);
        return reportDate;
    }

}
