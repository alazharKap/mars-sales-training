package com.mars.salestrainingapp.Helper;

import android.content.Context;
import android.content.SharedPreferences;
import android.util.Log;

import com.mars.salestrainingapp.Model.SalesPersonModel;
import com.mars.salestrainingapp.Model.ScheduledOutlet;

import io.realm.Realm;
import io.realm.RealmResults;

/**
 * Created by surve on 16-Oct-17.
 */

public class Preference {

    private static final String MARS_SALES = "com.mars.salestrainingapp";

    private static Preference instance;
    private static SharedPreferences sharedPreferences;

    private Context context;

    private static final String USER_ID = "user_id";
    private static final String EMP_ID = "emp_id";
    public static final String USERNAME = "user_name";
    public static final String NAME = "name";
    public static final String FCMID = "fcmid";
    public static final String EMAIL = "email";
    public static final String LEVEL = "level";
    public static final String ID = "id";
    public static final String ROLE = "role";
    public static final String MEMBER_TYPE = "member_type";
    public static final String HEAD_MEMBER = "head_member";
    public static final String ERROR = "error";
    public static final String STATUS = "status";
    public static final String MOBILE = "mobile";
    public static final String REGION = "region";
    public static final String REGION_ID = "region_id";
    public static final String DOJ = "doj";

    public static final String RESPONSE_DATA = "response_data";
    public static final String SUCCESS = "success";
    public static final String START_DATE = "start_date";
    public static final String SPOT_COUNT = "spot_count";
    public static final String ALLOWED_SPOT = "allowed_spot";
    public static final String END_DATE = "end_date";
    public static final String PLAN_PERIOD = "plan_period";

    public static final String ROLE_ID = "role_id";

    public Preference(Context context) {
        sharedPreferences = context.getSharedPreferences(MARS_SALES, Context.MODE_PRIVATE);
        this.context = context;
    }
    public static Preference getInstance(Context context) {
        if (instance == null)
            instance = new Preference(context);
        return instance;
    }

    public void setUserId(int id) {
        final SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putInt(USER_ID, id);
        editor.apply();
    }
    public int getUserId() { return sharedPreferences.getInt(USER_ID, 0);}

    public void setEmpId(String id) {
        final SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putString(EMP_ID, id);
        editor.apply();
    }
    public String getEmpId() { return sharedPreferences.getString(EMP_ID, "");}




    public void setUsername(String username) {
        final SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putString(USERNAME, username);
        editor.apply();
    }
    public String getUsername() { return sharedPreferences.getString(USERNAME, "");}

    public void setStartDate(String startDate){
        final SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putString(START_DATE, startDate);
        editor.apply();
    }
    public String getStartDate() {
        return sharedPreferences.getString(START_DATE, "");
    }

    public void setEmail(String email) {

        final SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putString(EMAIL, email);
        editor.apply();
    }
    public String getEmail() { return sharedPreferences.getString(EMAIL, "");}

    public void setName(String name) {
        final SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putString(NAME, name);
        editor.apply();
    }
    public String getName() { return sharedPreferences.getString(NAME, "");}

    public void setFcmid(String fcmToken) {
        final SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putString(FCMID,fcmToken);
        editor.apply();
    }
    public String getFcmid() {
        return sharedPreferences.getString(FCMID,"");
    }

    public void setMemberType(String memberType) {
        final SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putString(MEMBER_TYPE,memberType);
        editor.apply();
    }
    public String getMemberType() {
        return sharedPreferences.getString(MEMBER_TYPE,"");
    }

    public void setHeadMember(String headMember) {
        final SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putString(HEAD_MEMBER,headMember);
        editor.apply();
    }
    public String getHeadMember() {
        return sharedPreferences.getString(HEAD_MEMBER,"");
    }

    public void setSpotCount(int count) {
        final SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putInt(SPOT_COUNT, count);
        editor.apply();;
    }
    public int getSpotCount() { return sharedPreferences.getInt(SPOT_COUNT, 0); }

    public void setMaxSpot(int count)  {
        final SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putInt(ALLOWED_SPOT, count);
        editor.apply();;
    }
    public int getMaxSpot() { return sharedPreferences.getInt(ALLOWED_SPOT, 0); }

    public void setRole(String role) {
        final SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putString(ROLE, role);
        editor.apply();
    }
    public String getRole() { return sharedPreferences.getString(ROLE, ""); }

    public void setEndDate(String endDate) {
        final SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putString(END_DATE, endDate);
        editor.apply();
    }
    public String getEndDate() { return sharedPreferences.getString(END_DATE,""); }

    public void setPlanPeriod(String planPeriod) {
        final SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putString(PLAN_PERIOD, planPeriod);
        editor.apply();
    }
    public String getPlanPeriod() { return sharedPreferences.getString(PLAN_PERIOD,""); }

    public void setMobile(String mobile) {
        final SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putString(MOBILE, mobile);
        editor.apply();
    }
    public String getMobile() { return sharedPreferences.getString(MOBILE,""); }

    public void setRegionId(int regionId) {
        final SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putInt(REGION_ID, regionId);
        editor.apply();
    }
    public int getRegionId() { return sharedPreferences.getInt(REGION_ID,0); }

    public void setRegion(String region) {
        final SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putString(REGION, region);
        editor.apply();
    }
    public String getRegion() { return sharedPreferences.getString(REGION,""); }

    public void setDoj(String doj) {
        final SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putString(DOJ, doj);
        editor.apply();
    }
    public String getDoj() { return sharedPreferences.getString(DOJ,""); }


    public void clear() {
        final SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.clear();
        editor.apply();
    }

    public void setRoleId(String id) {
        final SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putString(ROLE_ID, id);
        editor.apply();
    }
    public String getRoleId() { return sharedPreferences.getString(ROLE_ID, "");}

    public void calculateSpotAward(Context context) {
        Realm.init(context);
        Realm realm = Realm.getDefaultInstance();
        RealmResults<ScheduledOutlet> results = realm.where(ScheduledOutlet.class).equalTo("spotAward",1).findAll();

        Log.d("Calculate Spot Award",results.toString());
        setSpotCount(results.size());
    }


    public int getSelectedUser(String userName) {
        Realm.init(context);
        Realm realm = Realm.getDefaultInstance();
        SalesPersonModel model = realm.where(SalesPersonModel.class).equalTo("user_name", userName).findFirst();
        return model.getEmpid();
    }


}
