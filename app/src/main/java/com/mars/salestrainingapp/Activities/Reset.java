package com.mars.salestrainingapp.Activities;

import android.content.Intent;
import android.net.Uri;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.RelativeLayout;

import com.adevole.customresources.CustomEditText;
import com.adevole.customresources.CustomTextView;
import com.google.gson.JsonObject;
import com.mars.salestrainingapp.API.API;
import com.mars.salestrainingapp.API.APIList;
import com.mars.salestrainingapp.Helper.InternetConnection;
import com.mars.salestrainingapp.R;

import br.com.simplepass.loading_button_lib.customViews.CircularProgressButton;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

import static com.mars.salestrainingapp.Helper.Preference.ERROR;
import static com.mars.salestrainingapp.Helper.Preference.SUCCESS;
import static com.mars.salestrainingapp.Helper.Utility.internetError;
import static com.mars.salestrainingapp.Helper.Utility.log;
import static com.mars.salestrainingapp.Helper.Utility.responseError;
import static com.mars.salestrainingapp.Helper.Utility.toast;

public class Reset extends AppCompatActivity implements View.OnClickListener, Callback<JsonObject>{

    private static final String TAG = "Reset";

    private String appkey, username, password;

    private RelativeLayout frame;
    private CustomEditText passwordEdit, confirmEdit;
    private CustomTextView title;
    private CircularProgressButton submitButton;

    private void initCreate() {
        Intent appLinkIntent = getIntent();
        String appLinkAction = appLinkIntent.getAction();
        Uri appLinkData = appLinkIntent.getData();

        if (appLinkData != null) {
            log(TAG, "appLinkData - " + appLinkData.toString());
            log(TAG, "appLinkAction - " + appLinkAction);
            appkey = getAppKey(appLinkData.toString());
            username = getUsername(appLinkData.toString());
            log(TAG,"AppKey - "+appkey);
            log(TAG,"UserName - "+username);
        }

        title = (CustomTextView) findViewById(R.id.title);
        title.setText(getString(R.string.reset_password));

        passwordEdit = (CustomEditText) findViewById(R.id.password);
        confirmEdit = (CustomEditText) findViewById(R.id.confirm_password);

        submitButton = (CircularProgressButton) findViewById(R.id.submit);
        frame = (RelativeLayout) findViewById(R.id.frame);

        submitButton.setOnClickListener(this);
    }

//    http://www.adevole.com/clients/mars/api/change_password.php?appkey=1998128931&username=surveshoeb@gmail.com
    private String getUsername(String s) {
        String username = s.split("&username=")[1];
        return username;
    }

    private String getAppKey(String s) {
        String appKey = s.replaceAll("http://52.172.26.109/api/change_password.php[?]appkey=","");
        appKey = appKey.split("&username=")[0];
        return appKey;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_reset);

        initCreate();
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.submit :
                if (validatePassword()) {
                    if (InternetConnection.checkConnection(getApplicationContext())) {
                        updatePassword();
                    } else
                        internetError(frame, getApplicationContext());
                }
            break;
        }
    }

    private void updatePassword() {
        Retrofit retrofit = new Retrofit.Builder().baseUrl(APIList.BASE_URL).addConverterFactory(GsonConverterFactory.create()).build();
        API api = retrofit.create(API.class);
        Call<JsonObject> call = api.reset(appkey, password, username);
        startAnimate();
        log(TAG, "Reset URL - "+call.request().url().toString());
        call.enqueue(this);
    }

    private void startAnimate() {
        passwordEdit.setEnabled(false);
        confirmEdit.setEnabled(false);
        submitButton.startAnimation();
    }

    private void stopAnimate() {
        passwordEdit.setEnabled(true);
        confirmEdit.setEnabled(true);
        submitButton.revertAnimation();
    }

    private boolean validatePassword() {
        String p1 = passwordEdit.getText().toString();
        String p2 = confirmEdit.getText().toString();

        if (!p1.isEmpty() && !p2.isEmpty()) {
            if (p1.equals(p2)) {
                password = p1;
                return true;
            }
            else {
                toast(frame, getApplicationContext(), getString(R.string.password_doesnt_match));
                return false;
            }
        }
        else
            toast(frame, getApplicationContext(), getString(R.string.enter_password));

        return false;
    }

    @Override
    public void onResponse(Call<JsonObject> call, Response<JsonObject> response) {
        if (response.isSuccessful()) {
            stopAnimate();
            JsonObject object = response.body();
            if (object != null) {
                if (object.has(SUCCESS)) {
                    toast(frame, getApplicationContext(), object.get(SUCCESS).getAsString());
                    new Handler().postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            callLogin();
                        }
                    },2000);
                }
                else if (object.has(ERROR))
                    toast(frame, getApplicationContext(), object.get(ERROR).getAsString());
            }
            else {
                responseError(frame, getApplicationContext());
                stopAnimate();
            }
        }
        else{
            responseError(frame, getApplicationContext());
            stopAnimate();
        }
    }

    private void callLogin() {
        Intent i = new Intent(this, Login.class);
        i.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        startActivity(i);
        finish();
    }

    @Override
    public void onFailure(Call<JsonObject> call, Throwable t) {
        stopAnimate();
        responseError(frame, getApplicationContext());
        t.printStackTrace();
    }
}
