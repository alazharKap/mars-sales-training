package com.mars.salestrainingapp.Activities;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.ActionBar;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.Button;
import android.widget.RelativeLayout;
import android.widget.Toast;

import com.mars.salestrainingapp.API.APIRequest;
import com.mars.salestrainingapp.Adapter.ScheduledOutletAdapter;
import com.mars.salestrainingapp.Helper.InternetConnection;
import com.mars.salestrainingapp.Model.OutletModel;
import com.mars.salestrainingapp.Model.ScheduleModel;
import com.mars.salestrainingapp.Model.ScheduledOutlet;
import com.mars.salestrainingapp.R;

import io.realm.Realm;
import io.realm.RealmResults;

import static com.mars.salestrainingapp.Helper.Utility.internetError;
import static com.mars.salestrainingapp.Helper.Utility.toast;

public class Outlet extends AppCompatActivity implements View.OnClickListener{

    private Toolbar toolbar;
    private RelativeLayout frame;
    private RecyclerView recyclerView;
    Button btnAddOutlet, btnEndDay;
    Intent i;
    RealmResults<ScheduledOutlet> results;

    int empId, bossId, scheduleId;

    boolean apiCall = false;

    private void initCreate() {

        Bundle extra = getIntent().getExtras();
        if (extra!=null) {
            empId = extra.getInt("empId");
            bossId = extra.getInt("bossId");
            scheduleId = extra.getInt("scheduleId");
        }

        frame = (RelativeLayout) findViewById(R.id.frame);
        btnAddOutlet = (Button) findViewById(R.id.btnAddOutlet);
        btnEndDay = (Button) findViewById(R.id.btnEndDay);
        toolbar = (android.support.v7.widget.Toolbar) findViewById(R.id.toolbar);
        btnAddOutlet.setOnClickListener(this);
        btnEndDay.setOnClickListener(this);

        recyclerView = (RecyclerView) findViewById(R.id.recycle);
        recyclerView.setLayoutManager(new LinearLayoutManager(getApplicationContext()));
        recyclerView.setItemAnimator(new DefaultItemAnimator());
    }

    private void setToolbar() {
        ActionBarDrawerToggle mDrawerToggle;
        setSupportActionBar(toolbar);

        ActionBar actionBar = getSupportActionBar();
        if (actionBar != null) {
            actionBar.setDisplayHomeAsUpEnabled(true);
            actionBar.setTitle("Outlet List");
            actionBar.setHomeAsUpIndicator(getResources().getDrawable(R.drawable.back));
        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_outlet);

        initCreate();
        setToolbar();
//        getOutlets();
    }

    private void getOutlets() {
        if (InternetConnection.checkConnection(getApplicationContext())) {
            APIRequest apiRequest = new APIRequest();
            apiRequest.setOutletResponse(new APIRequest.OutletResponse() {
                @Override
                public void onOutletReceived(boolean status) {
                    apiCall = false;
                }
            });
            apiCall = true;
            apiRequest.getOutlets(getApplicationContext(), empId);
        }
        else
            internetError(frame, getApplicationContext());
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btnAddOutlet:

                Realm.init(getApplicationContext());
                Realm realm = Realm.getDefaultInstance();
                ScheduleModel model =  realm.where(ScheduleModel.class).equalTo("id", scheduleId).findFirst();

                if (model.getStatus() == 2)
                    toast(frame, getApplicationContext(), "Schedule Already Ended You can add anymore Outlets!");
                else {
                    i = new Intent(getApplicationContext(), CreateOutlet.class);
                    i.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                    i.putExtra("empId", empId);
                    i.putExtra("bossId", bossId);
                    i.putExtra("scheduleId", scheduleId);
                    startActivity(i);
                }
                break;
            case R.id.btnEndDay:
                if (!apiCall)
                    validateBack();
                else
                    toast(frame, getApplicationContext(), getString(R.string.hold_in_process));
                break;
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        fromRealm();
    }

    private void fromRealm() {
        Realm.init(getApplicationContext());
        Realm realm = Realm.getDefaultInstance();
        results = realm.where(ScheduledOutlet.class).equalTo("schedule_id", scheduleId).findAll();
        recyclerView.setAdapter(new ScheduledOutletAdapter(getApplicationContext(), results));
    }

    @Override
    public void onBackPressed() {
        validateBack();
    }

    @Override
    public boolean onSupportNavigateUp() {
        //validateBack();
        finish();
        return true;
    }

    private void validateBack() {
        if (results != null) {
            if (results.size() < 16) {
                AlertDialog dialog;

                AlertDialog.Builder builder = new AlertDialog.Builder(Outlet.this);
                builder.setMessage(getString(R.string.day_end_message));
                builder.setPositiveButton(R.string.confirm, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        dialog.dismiss();
                        endDay();
                    }
                });
                builder.setNegativeButton(R.string.cancel, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        dialog.dismiss();
                    }
                });
                dialog = builder.create();
                dialog.show();
            }
            else
                endDay();
        }
    }

    private void endDay() {
        APIRequest apiRequest = new APIRequest();
        apiRequest.setChangeDayResponse(new APIRequest.ChangeDayStatusResponse() {
            @Override
            public void onStatusChanges(boolean status) {
                apiCall = false;
                if (status) {
                    Toast.makeText(getApplicationContext(), getString(R.string.day_ended), Toast.LENGTH_SHORT).show();

                    Realm.init(getApplicationContext());
                    Realm realm = Realm.getDefaultInstance();
                    realm.beginTransaction();
                    realm.delete(OutletModel.class);
                    realm.commitTransaction();

                    Intent i = new Intent(Outlet.this, Questionary.class);
                    i.putExtra("scheduleId", scheduleId);
                    i.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                    startActivity(i);
                }
            }
        });
        apiCall = true;
        toast(frame, getApplicationContext(), getString(R.string.ending_date));
        APIRequest.changeDayStatus(frame, getApplicationContext(), scheduleId,2);
    }
}
