package com.mars.salestrainingapp.Activities;

import android.os.Bundle;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;

import com.adevole.customresources.CustomEditText;
import com.mars.salestrainingapp.API.APIRequest;
import com.mars.salestrainingapp.Helper.InternetConnection;
import com.mars.salestrainingapp.R;

import br.com.simplepass.loading_button_lib.customViews.CircularProgressButton;

import static com.mars.salestrainingapp.Helper.Utility.internetError;
import static com.mars.salestrainingapp.Helper.Utility.toast;

public class ChangePassword extends AppCompatActivity {

    private RelativeLayout frame;
    private CustomEditText currentText, newText, confirmText;
    private CircularProgressButton submitButton;
    Toolbar toolbar;
    private void initCreate() {
        frame = (RelativeLayout) findViewById(R.id.frame);
        currentText = (CustomEditText) findViewById(R.id.current_text);
        newText = (CustomEditText) findViewById(R.id.new_text);
        confirmText = (CustomEditText) findViewById(R.id.confirm_text);
        toolbar = (Toolbar)findViewById(R.id.toolbar1);

        ImageView back = (ImageView) findViewById(R.id.imageView3);
        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
        submitButton = (CircularProgressButton) findViewById(R.id.submit);
        submitButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                change();
            }
        });
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_change_password);

        initCreate();
        toolbar.setNavigationIcon(R.drawable.back);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
    }

    private void change() {

        String currentPass = currentText.getText().toString();
        String newPass = newText.getText().toString();
        String confirmPass = confirmText.getText().toString();

        if (InternetConnection.checkConnection(getApplicationContext())) {
            if ( !currentPass.isEmpty() && !newPass.isEmpty() && !confirmPass.isEmpty()) {
                APIRequest apiRequest = new APIRequest();
                submitButton.startAnimation();
                apiRequest.setChangePasswordListener(new APIRequest.ChangePassword() {
                    @Override
                    public void onPasswordChanged(boolean status) {
                        submitButton.revertAnimation();
                        Log.d("Status------------>",status+"");
                        if (status) {
                            toast(frame, getApplicationContext(), getString(R.string.password_changed_successfully));
                            new Handler().postDelayed(new Runnable() {
                                @Override
                                public void run() {
                                    finish();
                                }
                            },2000);
                        }
                        else
                            toast(frame, getApplicationContext(), getString(R.string.unable_to_change_password));
                    }
                });
                APIRequest.changePassword(frame, getApplicationContext(), currentPass, newPass);
            }
            else
                toast(frame, getApplicationContext(), getString(R.string.enter_details));
        }
        else
            internetError(frame, getApplicationContext());
    }
}