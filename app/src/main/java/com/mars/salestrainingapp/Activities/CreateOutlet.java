package com.mars.salestrainingapp.Activities;

import android.content.DialogInterface;
import android.graphics.Color;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.app.ActionBar;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.text.InputFilter;
import android.text.Spanned;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.EditText;
import android.widget.RatingBar;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.Toast;

import com.adevole.customresources.CustomButton;
import com.adevole.customresources.CustomTextView;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.mars.salestrainingapp.API.API;
import com.mars.salestrainingapp.API.APIClient;
import com.mars.salestrainingapp.API.APIRequest;
import com.mars.salestrainingapp.Adapter.QuestionsRatingAdapter;
import com.mars.salestrainingapp.Helper.Preference;
import com.mars.salestrainingapp.Model.GetRatingQuestionsModel;
import com.mars.salestrainingapp.Model.OutletModel;
import com.mars.salestrainingapp.Model.RegionModel;
import com.mars.salestrainingapp.Model.SalesPersonModel;
import com.mars.salestrainingapp.Model.ScheduledOutlet;
import com.mars.salestrainingapp.R;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

import io.realm.Realm;
import io.realm.RealmQuery;
import io.realm.RealmResults;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.mars.salestrainingapp.Helper.Utility.log;
import static com.mars.salestrainingapp.Helper.Utility.toast;

public class CreateOutlet extends AppCompatActivity implements View.OnClickListener {

    Spinner spinnerOutletName, spinnerRegion;
    private RelativeLayout frame;
    private Toolbar toolbar;
    private CustomTextView editTextUserName;
    private EditText editTextNotes;
    private AutoCompleteTextView outletNameEdit;
    private CustomButton btnSave, spotButton;
    private RatingBar ratingBarOutlet;
    private int outlet_id = 0;
    private String blockCharacterSet = "~#^|$%&*!{}[]/,`:;'@%()-_+|£¥";
    private int empId, bossId, scheduleId, spotAward = 0;
    private String spotReason = "";
    private RealmResults<OutletModel> outlets;
    private ArrayList<String> regionList;
    private ArrayList<String> outletList;
    private RatingBar ratinBar;

    /* edited  by vinay    */


    private RecyclerView recyclerViewQuestions;

    private QuestionsRatingAdapter mAdapter;
    private RecyclerView.LayoutManager mLayoutManager;

    private void initCreate() {
        Bundle extras = getIntent().getExtras();
        if (extras != null) {
            empId = extras.getInt("empId");
            bossId = extras.getInt("bossId");
            scheduleId = extras.getInt("scheduleId");
        }

        outletList = new ArrayList<String>();
        ratinBar = (RatingBar) findViewById(R.id.rating);
        spotButton = (CustomButton) findViewById(R.id.btnGiveSpotAward);
        frame = (RelativeLayout) findViewById(R.id.frame);
        editTextUserName = (CustomTextView) findViewById(R.id.editTextUserName);
        editTextNotes = (EditText) findViewById(R.id.editTextNotes);
        outletNameEdit = (AutoCompleteTextView) findViewById(R.id.outletName);
        ratingBarOutlet = (RatingBar) findViewById(R.id.ratingBarOutlet);
        btnSave = (CustomButton) findViewById(R.id.btnSave);
        spinnerOutletName = (Spinner) findViewById(R.id.spinnerOutletName);
        spinnerRegion = (Spinner) findViewById(R.id.spinnerRegion);
        toolbar = (android.support.v7.widget.Toolbar) findViewById(R.id.toolbar);
        editTextNotes.setFilters(new InputFilter[]{filter});


        /* by vinay */

        recyclerViewQuestions = (RecyclerView) findViewById(R.id.recycler_question_rating);

        getRecyclerViewContent();


//        String[] outletArray = getResources().getStringArray(R.array.outletsArray);
//        Log.d("Array ---------------->",outletArray.toString());
//        ArrayAdapter<String> adapter1 = new ArrayAdapter<String>(this,android.R.layout.simple_dropdown_item_1line,outletArray);
//        outletNameEdit.setAdapter(adapter1);

        spotButton.setOnClickListener(this);
        spotButton.setText(getString(R.string.spot_award)
                .concat(" " + String.valueOf(Preference.getInstance(getApplicationContext()).getSpotCount()))
                .concat("/" + String.valueOf(Preference.getInstance(getApplicationContext()).getMaxSpot())));

        if (Preference.getInstance(getApplicationContext()).getSpotCount() <= Preference.getInstance(getApplicationContext()).getMaxSpot())
            spotButton.setVisibility(View.VISIBLE);
        else
            spotButton.setVisibility(View.GONE);


    }

    private void setToolbar() {
        ActionBarDrawerToggle mDrawerToggle;
        setSupportActionBar(toolbar);

        ActionBar actionBar = getSupportActionBar();
        if (actionBar != null) {
            actionBar.setDisplayHomeAsUpEnabled(true);
            actionBar.setTitle("Add Outlet");
            actionBar.setHomeAsUpIndicator(getResources().getDrawable(R.drawable.back));
        }
    }

    private InputFilter filter = new InputFilter() {
        @Override
        public CharSequence filter(CharSequence source, int start, int end, Spanned dest, int dstart, int dend) {
            if (source != null && blockCharacterSet.contains(("" + source))) {
                return "";
            }
            return null;
        }
    };

    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_create_outlet);

        initCreate();
        setToolbar();
        setAutoCompleteTextView();
        btnSave.setOnClickListener(this);

        fromRealm();
    }

    private void setRegionSpinner() {
        regionList = new ArrayList<>();
        regionList.add(0, "Select Region");
//        regionList.add(1,"North");
//        regionList.add(2,"West");
        Realm.init(getApplicationContext());
        Realm realm = Realm.getDefaultInstance();

        RealmResults<RegionModel> results = realm.where(RegionModel.class).findAll();
        for (int i = 0; i < results.size(); i++) {
            RegionModel model = results.get(i);
            regionList.add(model.getValue());
        }

        ArrayAdapter<String> adapter =
                new ArrayAdapter<String>(CreateOutlet.this, R.layout.support_simple_spinner_dropdown_item, regionList);
        adapter.setDropDownViewResource(R.layout.support_simple_spinner_dropdown_item);
        spinnerRegion.setAdapter(adapter);
        spinnerRegion.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                outletNameEdit.setText("");
                outletList.clear();

                Realm.init(getApplicationContext());
                Realm realm = Realm.getDefaultInstance();

                RealmResults<OutletModel> results = realm.where(OutletModel.class).equalTo("region", regionList.get(position).toString()).findAll();
                for (int i = 0; i < results.size(); i++) {
                    OutletModel model = results.get(i);
                    assert model != null;
                    outletList.add(model.getOutlet_name());
                }

                ArrayAdapter<String> adapter =
                        new ArrayAdapter<String>(CreateOutlet.this, R.layout.support_simple_spinner_dropdown_item, outletList);
                adapter.setDropDownViewResource(R.layout.support_simple_spinner_dropdown_item);
                outletNameEdit.setAdapter(adapter);

            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

    }

    private void fromRealm() {
        Realm.init(getApplicationContext());
        Realm realm = Realm.getDefaultInstance();
        SalesPersonModel salesPersonModel = realm.where(SalesPersonModel.class).equalTo("empid", empId).findFirst();
        editTextUserName.setText(salesPersonModel.getUser_name());
    }

    private void setOutletSpinner() {
        outletList = new ArrayList<String>();

        Realm.init(getApplicationContext());
        Realm realm = Realm.getDefaultInstance();
        RealmResults<ScheduledOutlet> scheduledOutlets = realm.where(ScheduledOutlet.class).equalTo("schedule_id", scheduleId).findAll();
        RealmQuery outletQuery = realm.where(OutletModel.class);

        for (ScheduledOutlet scheduledOutlet : scheduledOutlets)
            outletQuery = outletQuery.notEqualTo("id", scheduledOutlet.getOutlet_id());

        final RealmResults<OutletModel> outlets = outletQuery.findAll();

        outletList.add(0, "Select Outlet");
        for (OutletModel outlet : outlets)
            outletList.add(outlet.getStoreCode() + "-" + outlet.getOutlet_name());

        ArrayAdapter<String> adapter = new ArrayAdapter<String>(CreateOutlet.this, android.R.layout.select_dialog_item, outletList);
        adapter.setDropDownViewResource(android.R.layout.select_dialog_item);
        spinnerOutletName.setAdapter(adapter);
        spinnerOutletName.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                if (i != 0)
                    outlet_id = outlets.get(i - 1).getId();
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {
            }
        });
    }

    private void setAutoCompleteTextView() {
        outletList = new ArrayList<String>();

        Realm.init(getApplicationContext());
        Realm realm = Realm.getDefaultInstance();
        RealmResults<ScheduledOutlet> scheduledOutlets = realm.where(ScheduledOutlet.class).equalTo("schedule_id", scheduleId).findAll();
        RealmQuery outletQuery = realm.where(OutletModel.class);

        for (ScheduledOutlet scheduledOutlet : scheduledOutlets)
            outletQuery = outletQuery.notEqualTo("id", scheduledOutlet.getOutlet_id());

        final RealmResults<OutletModel> outlets = outletQuery.findAll();

        for (OutletModel outlet : outlets) {
            outletList.add(outlet.getOutlet_name() + "-" + outlet.getStoreCode());
            log("OUTLETS", outlet.getOutlet_name() + "-" + outlet.getStoreCode());
        }

        ArrayAdapter<String> adapter = new ArrayAdapter<String>(CreateOutlet.this, android.R.layout.select_dialog_item, outletList);
        outletNameEdit.setAdapter(adapter);
        outletNameEdit.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
//                outlet_id = outlets.get(position).getId();
                String selection = (String) parent.getItemAtPosition(position);
                if (outletList.contains(selection)) {
                    outlet_id = outlets.get(outletList.indexOf(selection)).getId();
                }
            }
        });
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btnGiveSpotAward:
                openDialog();
                break;
            case R.id.btnSave:
                String username = editTextUserName.getText().toString();
                String notes = editTextNotes.getText().toString();
                float outletRating = ratingBarOutlet.getRating();

                if (username.equals("")) {
                    Toast.makeText(getApplicationContext(), "Username cannot be empty",
                            Toast.LENGTH_SHORT).show();
                }
 //               else if (outlet_id == 0) {
//                    Toast.makeText(getApplicationContext(), "Select an Outlet",
//                            Toast.LENGTH_SHORT).show();
//
//                    //if spinner is not selected
//                    String queryString = outletNameEdit.getText().toString();

//                } else if (outletRating == 0) {
//                    Toast.makeText(getApplicationContext(), "Provide Outlet Rating",
//                            Toast.LENGTH_SHORT).show();
          //      }
//                else if(notes.equals("")){
//                    Toast.makeText(getApplicationContext(), "Notes cannot be empty",
//                            Toast.LENGTH_SHORT).show();
//                }
                else {

                    APIRequest apiRequest = new APIRequest();
                    apiRequest.setSchduleOutletResponse(new APIRequest.ScheduleOutletResponse() {
                        @Override
                        public void onOutletScheduled() {
                          //  btnSave.setClickable(false);
                            toast(frame, getApplicationContext(), getString(R.string.outlet_added_successfully));
                            new Handler().postDelayed(new Runnable() {
                                @Override
                                public void run() {
                                    finish();
                                }
                            }, 2000);
                        }
                    });

                    Set<Map.Entry<String, String>> keySet = QuestionsRatingAdapter.hashMap.entrySet();
                    JsonArray array = new JsonArray();

                    if (!QuestionsRatingAdapter.hashMap.isEmpty()){
                        keySet = QuestionsRatingAdapter.hashMap.entrySet();
                        Iterator<Map.Entry<String, String>> keyIterator = keySet.iterator();
                        for (Map.Entry<String, String> entry : keySet) {
                            String key = entry.getKey().toString();
                            String value = QuestionsRatingAdapter.hashMap.get(key);
                            JsonObject object = new JsonObject();
                            object.addProperty("id", key);
                            object.addProperty("rating", value);
                            array.add(object);

                        }
//            keyIterator.next().getKey().toString();
                    }
                    APIRequest.scheduleOutlet(frame, getApplicationContext(), outlet_id, scheduleId, outletRating, notes, spotAward, spotReason, array.toString());
                }
                break;
        }
    }

    private void openDialog() {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle(getString(R.string.reason_for_award));

        final EditText input = new EditText(getApplicationContext());
        input.setTextColor(Color.BLACK);
        builder.setView(input);

        builder.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                spotReason = input.getText().toString();
                if (!spotReason.isEmpty()) {
                    spotAward = 1;
                    spotButton.setVisibility(View.GONE);
                } else {
                    toast(frame, getApplicationContext(), getString(R.string.reason_compulsory));
                    spotAward = 0;
                }
            }
        });
        builder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.cancel();
                spotAward = 0;
            }
        });

        builder.show();


    }


    public void getRecyclerViewContent() {
        final List<GetRatingQuestionsModel> list = new ArrayList<>();
        mAdapter = new QuestionsRatingAdapter(this, list);
        recyclerViewQuestions.setLayoutManager(new LinearLayoutManager(this));


        mLayoutManager = new LinearLayoutManager(getApplicationContext());
        recyclerViewQuestions.setLayoutManager(mLayoutManager);
        recyclerViewQuestions.setItemAnimator(new DefaultItemAnimator());
        recyclerViewQuestions.setAdapter(mAdapter);
        if (ratinBar != null) {
            ratinBar.getRating();
            Log.i("Ratin Values", String.valueOf(ratinBar.getRating()));
        }
        API api = APIClient.getAPI();
        Call<JsonObject> call = api.getQuestionsData();
        log("QUESTIONS RATING URL", call.request().url().toString());
        Log.i("Package name", getApplicationContext().getPackageName());

        call.enqueue(new Callback<JsonObject>() {
            @Override
            public void onResponse(Call<JsonObject> call, Response<JsonObject> response) {
                if (response.isSuccessful()) {
                    JsonObject object = response.body();
                    if (object != null) {
                        JsonArray array = object.get("response_data").getAsJsonArray();
                        if (array.size() > 0) {


                            for (int i = 0; i < array.size(); i++) {
                                JsonObject innerObject = array.get(i).getAsJsonObject();
                                GetRatingQuestionsModel model = new GetRatingQuestionsModel();
                                model.setId(innerObject.get("id").getAsInt());
                                model.setQuestion(innerObject.get("question").getAsString());
                                model.setFlag(innerObject.get("flag").getAsInt());
                                model.setStatus(innerObject.get("status").getAsInt());
                                model.setCreated_at(innerObject.get("created_at").getAsString());
                                model.setUpdated_at(innerObject.get("updated_at").getAsString());

                                list.add(model);
                                mAdapter.notifyDataSetChanged();
                            }
                        }
                    }
//
                }

            }

            @Override
            public void onFailure(Call<JsonObject> call, Throwable t) {
//                responseError(frame, context);
                t.printStackTrace();
            }
        });
    }

//    public void getRatingFromAdapter(ArrayList<Float> listRating) {
//        Log.i("Rating 1", String.valueOf(listRating.get(0)));
//        Log.i("Rating 2", String.valueOf(listRating.get(1)));
//        Log.i("Rating 3", String.valueOf(listRating.get(2)));
//        Log.i("Rating 4", String.valueOf(listRating.get(3)));
//
////        listRating.get(1);
////        listRating.get(2);
////        listRating.get(3);
//    }

}