package com.mars.salestrainingapp.Activities;

import android.content.Intent;
import android.support.constraint.ConstraintLayout;
import android.support.design.widget.TabLayout;
import android.support.v4.view.ViewPager;
import android.support.v7.app.ActionBar;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.View;

import com.adevole.customresources.CustomButton;
import com.adevole.customresources.CustomTextView;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.mars.salestrainingapp.API.API;
import com.mars.salestrainingapp.API.APIList;
import com.mars.salestrainingapp.API.APIRequest;
import com.mars.salestrainingapp.Adapter.PreviousViewPagerAdapter;
import com.mars.salestrainingapp.Fragment.Feedback;
import com.mars.salestrainingapp.Fragment.Overall;
import com.mars.salestrainingapp.Helper.InternetConnection;
import com.mars.salestrainingapp.Model.OutletModel;
import com.mars.salestrainingapp.Model.TempOutlets;
import com.mars.salestrainingapp.R;

import io.realm.Realm;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

import static com.mars.salestrainingapp.Helper.Utility.internetError;
import static com.mars.salestrainingapp.Helper.Utility.log;
import static com.mars.salestrainingapp.Helper.Utility.toast;

public class Previous extends AppCompatActivity {

    private TabLayout tabLayout;
    private Feedback feedback;
    private Overall overall;
    private ConstraintLayout frame;
    private int empId, bossId,scheduleId;
    private Toolbar toolbar;

    private boolean apiCall = false;

    private void setToolbar() {
        ActionBarDrawerToggle mDrawerToggle;
        setSupportActionBar(toolbar);

        ActionBar actionBar = getSupportActionBar();
        if (actionBar != null) {
            actionBar.setDisplayHomeAsUpEnabled(true);
            actionBar.setTitle(getString(R.string.previous_data));
            actionBar.setHomeAsUpIndicator(getResources().getDrawable(R.drawable.back));
        }
    }

    @Override
    public boolean onSupportNavigateUp() {
       // onBackPressed();
        Intent intent =new Intent(Previous.this,Home.class);
        startActivity(intent);
        finish();
        return true;
    }

    private void initCreate() {
        Bundle extra = getIntent().getExtras();
        if (extra!=null) {
            empId = extra.getInt("empId");
            bossId = extra.getInt("bossId");
            scheduleId = extra.getInt("scheduleId");
        }
        else
            finish();

        Bundle empBundle = new Bundle();
        empBundle.putInt("emp_id", empId);

        feedback = new Feedback();
        feedback.setArguments(empBundle);
        overall = new Overall();
        overall.setArguments(empBundle);

        toolbar = (Toolbar) findViewById(R.id.toolbar);
        frame = (ConstraintLayout) findViewById(R.id.frame);
        ViewPager viewPager = (ViewPager) findViewById(R.id.viewpager);
        createViewPager(viewPager);
        tabLayout = (TabLayout) findViewById(R.id.tabs);
        tabLayout.setupWithViewPager(viewPager);
        createTabIcons();

        CustomButton continueButton = (CustomButton) findViewById(R.id.continueButton);
        continueButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!apiCall) {
                    Intent i = new Intent(Previous.this, Outlet.class);
                    i.putExtra("empId", empId);
                    i.putExtra("bossId", bossId);
                    i.putExtra("scheduleId", scheduleId);
                    i.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                    startActivity(i);
                }
                else
                    toast(frame, getApplicationContext(), getString(R.string.hold_in_process));
            }
        });

        setToolbar();
    }

    private void createTabIcons() {

        View headerView1 = LayoutInflater.from(getApplicationContext()).inflate(R.layout.custom_tab_header, null);
        CustomTextView tabOne = (CustomTextView) headerView1.findViewById(R.id.customHeaderText);
        View headerView2 = LayoutInflater.from(getApplicationContext()).inflate(R.layout.custom_tab_header, null);
        CustomTextView tabTwo = (CustomTextView) headerView2.findViewById(R.id.customHeaderText);

        tabOne.setText("Feedback");
        tabLayout.getTabAt(0).setCustomView(headerView1);

        tabTwo.setText("Overall Data");
        tabLayout.getTabAt(1).setCustomView(headerView2);
    }

    private void createViewPager(ViewPager viewPager) {
        PreviousViewPagerAdapter adapter = new PreviousViewPagerAdapter(getSupportFragmentManager());
        adapter.addFrag(feedback, "Feedback");
        adapter.addFrag(overall, "Overall Data");
        viewPager.setAdapter(adapter);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_previous);

        initCreate();
        getOutlets();
    }

    private void getOutlets() {
        if (InternetConnection.checkConnection(getApplicationContext())) {
            APIRequest apiRequest = new APIRequest();
            apiRequest.setOutletResponse(new APIRequest.OutletResponse() {
                @Override
                public void onOutletReceived(boolean status) {
                    apiCall = false;
                }
            });
            apiCall = true;
            apiRequest.getOutlets(getApplicationContext(), empId);
        }
        else
            internetError(frame, getApplicationContext());
    }
}