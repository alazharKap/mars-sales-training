package com.mars.salestrainingapp.Activities;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
//import android.support.v7.widget.Toolbar;
import android.support.v7.widget.Toolbar;
import android.support.v7.widget.ToolbarWidgetWrapper;
import android.view.View;
import android.widget.ImageView;

import com.adevole.customresources.CustomTextView;
import com.mars.salestrainingapp.Helper.Preference;
import com.mars.salestrainingapp.R;

public class Profile extends AppCompatActivity implements View.OnClickListener{

    private CustomTextView nameText, empCodeText, emailText, mobileText, designationText, regionText;
    Toolbar toolbar;
    private void initCreate() {
        //ImageView back = (ImageView) findViewById(R.id.back);
        //back.setOnClickListener(this);

        CustomTextView changePassword = (CustomTextView) findViewById(R.id.change_password);
        changePassword.setOnClickListener(this);

        toolbar = (Toolbar)findViewById(R.id.toolbar1);
        nameText = (CustomTextView) findViewById(R.id.emp_name);
        empCodeText = (CustomTextView) findViewById(R.id.emp_code);
        emailText = (CustomTextView) findViewById(R.id.email);
        mobileText = (CustomTextView) findViewById(R.id.mobile);
        designationText = (CustomTextView) findViewById(R.id.designation);
        regionText = (CustomTextView) findViewById(R.id.region);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_profile);

        initCreate();

        nameText.setText(Preference.getInstance(getApplicationContext()).getName());
        empCodeText.setText(Preference.getInstance(getApplicationContext()).getEmpId());
        emailText.setText(Preference.getInstance(getApplicationContext()).getEmail());
        mobileText.setText(Preference.getInstance(getApplicationContext()).getMobile());
        designationText.setText(Preference.getInstance(getApplicationContext()).getRole());
        regionText.setText(Preference.getInstance(getApplicationContext()).getRegion());
        setSupportActionBar(toolbar);
        toolbar.setTitle("Profile");
        toolbar.setNavigationIcon(R.drawable.back);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
//            case R.id.back:
//                finish();
//                break;
            case R.id.change_password:
                Intent i = new Intent(Profile.this, ChangePassword.class);
                i.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(i);
                break;
        }
    }
}
