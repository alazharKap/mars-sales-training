package com.mars.salestrainingapp.Activities;

import android.content.Intent;
import android.os.Bundle;
import android.provider.Settings;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.FrameLayout;

import com.adevole.customresources.CustomEditText;
import com.adevole.customresources.CustomTextView;
import com.google.firebase.iid.FirebaseInstanceId;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.mars.salestrainingapp.API.API;
import com.mars.salestrainingapp.API.APIList;
import com.mars.salestrainingapp.API.APIRequest;
import com.mars.salestrainingapp.Helper.InternetConnection;
import com.mars.salestrainingapp.Helper.Preference;
import com.mars.salestrainingapp.R;

import br.com.simplepass.loading_button_lib.customViews.CircularProgressButton;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

import static com.mars.salestrainingapp.Helper.Preference.EMAIL;
import static com.mars.salestrainingapp.Helper.Preference.ERROR;
import static com.mars.salestrainingapp.Helper.Preference.ID;
import static com.mars.salestrainingapp.Helper.Preference.USERNAME;
import static com.mars.salestrainingapp.Helper.Utility.internetError;
import static com.mars.salestrainingapp.Helper.Utility.log;
import static com.mars.salestrainingapp.Helper.Utility.longToast;
import static com.mars.salestrainingapp.Helper.Utility.responseError;
import static com.mars.salestrainingapp.Helper.Utility.toast;

public class Login extends AppCompatActivity implements View.OnClickListener, Callback<JsonArray>{

    private static final String TAG = "Login";

    private FrameLayout frame;
    private CircularProgressButton loginButton;
    private CustomEditText userEdit, passEdit;

    private String fcmid;
    String role;
    
    private void initCreate() {
        frame = (FrameLayout) findViewById(R.id.frame);
        userEdit = (CustomEditText) findViewById(R.id.username); 
        passEdit = (CustomEditText) findViewById(R.id.password); 
        loginButton = (CircularProgressButton) findViewById(R.id.login);
        loginButton.setOnClickListener(this);

        CustomTextView titleText = (CustomTextView) findViewById(R.id.title);
        titleText.setText(getString(R.string.mars));
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        initCreate();

        fcmid = FirebaseInstanceId.getInstance().getToken();
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.login:
                validate();
                break;
        }
    }

    private void validate() {
        if (InternetConnection.checkConnection(getApplicationContext())) {
            if (userEdit.getText().length()>0) {
                if (passEdit.getText().length()>0) {
                    loginButton.startAnimation();
                    userEdit.setEnabled(false);
                    passEdit.setEnabled(false);


                    Retrofit retrofit = new Retrofit.Builder().baseUrl(APIList.BASE_URL).addConverterFactory(GsonConverterFactory.create()).build();
                    API api = retrofit.create(API.class);
                    String deviceId = getDeviceId();

                    Call<JsonArray> call = api.login(userEdit.getText().toString(), passEdit.getText().toString(), fcmid, deviceId);
        //            Call<JsonArray> call = api.login("userBM1@mars.co.in", "123", fcmid);
                    call.enqueue(this);
                    log(TAG,"Login - "+call.request().url().toString());
                }
                else
                    toast(frame, getApplicationContext(), getString(R.string.enter_password));
                }
            else
                toast(frame, getApplicationContext(), getString(R.string.enter_username));
            }
        else
            internetError(frame,getApplicationContext());
    }

    private String getDeviceId() {
        return Settings.Secure.getString(getApplicationContext().getContentResolver(), Settings.Secure.ANDROID_ID);
    }

    @Override
    public void onResponse(Call<JsonArray> call, Response<JsonArray> response) {

        loginButton.revertAnimation();
        userEdit.setEnabled(true);
        passEdit.setEnabled(true);

        if (response.isSuccessful()) {
            JsonArray array = response.body();

            if (array !=null) {
                log(TAG, array.toString());
                JsonObject object = array.get(0).getAsJsonObject();

                if (object.has(ERROR)) {
                    toast(frame, getApplicationContext() , object.get(ERROR).getAsString());
                }

                else {

                    role=object.get("role").getAsString();
                    Preference.getInstance(getApplicationContext()).setUserId(object.get(ID).getAsInt());
                    Preference.getInstance(getApplicationContext()).setEmpId(object.get("empid").getAsString());
                    Preference.getInstance(getApplicationContext()).setUsername(object.get("user_name").getAsString());
                    Preference.getInstance(getApplicationContext()).setEmail(object.get(EMAIL).getAsString());
                    Preference.getInstance(getApplicationContext()).setName(object.get(USERNAME).getAsString());
                    Preference.getInstance(getApplicationContext()).setFcmid("");
                    Preference.getInstance(getApplicationContext()).setRole(object.get("role_name").getAsString());
                    Preference.getInstance(getApplicationContext()).setHeadMember("");
                    Preference.getInstance(getApplicationContext()).setMobile(object.get("mobile_no").getAsString());
//                    Preference.getInstance(getApplicationContext()).setRegionId(object.get("region").getAsString());
                    Preference.getInstance(getApplicationContext()).setRegion(object.get("region").getAsString());
                    Preference.getInstance(getApplicationContext()).setDoj(object.get("doj").getAsString());

                    Preference.getInstance(getApplicationContext()).setRoleId(object.get("role").getAsString());

                    userEdit.setEnabled(false);
                    passEdit.setEnabled(false);
                    loginButton.startAnimation();

                    APIRequest apiRequest = new APIRequest();
                    apiRequest.setPreviousResponse(new APIRequest.PreviousData() {
                        @Override
                        public void onDataReceived(boolean status) {
                            loginButton.revertAnimation();
                            userEdit.setEnabled(true);
                            passEdit.setEnabled(true);
                            loginButton.revertAnimation();
                            if (status) {
                                Intent i = new Intent(Login.this, Home.class);
//                                //Create the bundle
//                                Bundle bundle = new Bundle();
////Add your data from getFactualResults method to bundle
//                                bundle.putString("role", role);
////Add the bundle to the intent
//                                i.putExtras(bundle);

                                i.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                                startActivity(i);
                                finish();
                            }
                        }
                    });
                    longToast(frame, getApplicationContext(), getString(R.string.getting_data));
                    APIRequest.getPreviousData(frame, getApplicationContext());
                }
            }
            else
                responseError(frame, getApplicationContext());
        }
        else
            responseError(frame, getApplicationContext());
    }

    @Override
    public void onFailure(Call<JsonArray> call, Throwable t) {
        loginButton.revertAnimation();
        userEdit.setEnabled(true);
        passEdit.setEnabled(true);
        responseError(frame, getApplicationContext());
        t.printStackTrace();
    }

    public void forgot(View v) {
        Intent i = new Intent(this, ForgotPassword.class);
        i.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(i);
    }
}