package com.mars.salestrainingapp.Activities;

import android.content.Context;
import android.os.Bundle;
import android.support.constraint.ConstraintLayout;
import android.support.design.widget.Snackbar;
import android.support.v7.widget.CardView;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;

import com.adevole.customresources.CustomTextView;
import com.github.mikephil.charting.charts.HorizontalBarChart;
import com.github.mikephil.charting.data.BarData;
import com.github.mikephil.charting.data.BarDataSet;
import com.github.mikephil.charting.data.BarEntry;
import com.github.mikephil.charting.utils.ColorTemplate;
import com.google.firebase.analytics.FirebaseAnalytics;
import com.mars.salestrainingapp.API.APIRequest;
import com.mars.salestrainingapp.Adapter.SalesPersonAdapter;
import com.mars.salestrainingapp.Adapter.TargetAdapter;
import com.mars.salestrainingapp.Helper.Preference;
import com.mars.salestrainingapp.Helper.Utility;
import com.mars.salestrainingapp.Model.SalesPersonModel;
import com.mars.salestrainingapp.Model.ScheduleModel;
import com.mars.salestrainingapp.Model.TargetModel;
import com.mars.salestrainingapp.R;

import java.util.ArrayList;
import java.util.List;

import io.realm.Realm;
import io.realm.RealmResults;
import io.realm.Sort;

import static com.mars.salestrainingapp.API.APIRequest.deleteSchedule;
import static com.mars.salestrainingapp.API.APIRequest.getAllTargetUsers;
import static com.mars.salestrainingapp.Helper.Utility.YYYYMMdd;
import static com.mars.salestrainingapp.Helper.Utility.convertToDate;
import static com.mars.salestrainingapp.Helper.Utility.currentYYYYMMdd;
import static com.mars.salestrainingapp.Helper.Utility.toast;

public class Home extends Drawer {

    private static final String TAG = "Home list";

    private SalesPersonAdapter todayAdapter;
    private SalesPersonAdapter upcomingAdapter;

    RealmResults<ScheduleModel> todaySchedules;
    List<ScheduleModel> todayList;
    RealmResults<ScheduleModel> upcomingSchedules;
    List<ScheduleModel> upcomingList;

    private RecyclerView todayRecycle, upcomingRecycle, targetRecycle;
    private CustomTextView periodText;
    private ConstraintLayout frame;
    private HorizontalBarChart workBar, spotBar;
    CustomTextView periodTextSpot;
    ImageView deleteShedule;
    LinearLayout spotLinear, workLinear, planLinear;
    CustomTextView spotHeader, start, end, textView4, planHeader, textTarget, noTargetText;

    CardView targetCard;

    private void initCreate() {
        frame = (ConstraintLayout) findViewById(R.id.frame);
        periodText = (CustomTextView) findViewById(R.id.periodText);
        spotBar = (HorizontalBarChart) findViewById(R.id.spotBar);
        workBar = (HorizontalBarChart) findViewById(R.id.workBar);
        periodTextSpot = (CustomTextView) findViewById(R.id.periodTextSpot);
        deleteShedule = (ImageView) findViewById(R.id.delete_schdule);
        spotHeader = (CustomTextView) findViewById(R.id.spotHeader);
        planHeader = (CustomTextView) findViewById(R.id.planHeader);

        spotLinear = (LinearLayout) findViewById(R.id.spotLinear);
        workLinear = (LinearLayout) findViewById(R.id.workLinear);
        planLinear = (LinearLayout) findViewById(R.id.planLinear);
        start = (CustomTextView) findViewById(R.id.start);
        end = (CustomTextView) findViewById(R.id.end);
        textView4 = (CustomTextView) findViewById(R.id.textView4);
        textTarget = (CustomTextView) findViewById(R.id.textTarget);
        noTargetText = (CustomTextView) findViewById(R.id.noTargetText);
        targetCard = (CardView) findViewById(R.id.targetCard);


        initRecyclerView();
        String role = Preference.getInstance(getApplicationContext()).getRoleId();
        if (role.equals("1") || role.equals("2")) {

            targetCard.setVisibility(View.VISIBLE);
            targetRecycle.setVisibility(View.VISIBLE);
            textTarget.setVisibility(View.VISIBLE);
            spotLinear.setVisibility(View.GONE);
            spotHeader.setVisibility(View.GONE);
            workLinear.setVisibility(View.GONE);
            textView4.setVisibility(View.GONE);
            planHeader.setVisibility(View.GONE);
            planLinear.setVisibility(View.GONE);
        }
    }

    private void initRecyclerView() {
        todayRecycle = (RecyclerView) findViewById(R.id.todayRecycle);
        todayRecycle.setItemAnimator(new DefaultItemAnimator());
        todayRecycle.setLayoutManager(new LinearLayoutManager(getApplicationContext()));

        upcomingRecycle = (RecyclerView) findViewById(R.id.upcomingRecycle);
        upcomingRecycle.setItemAnimator(new DefaultItemAnimator());
        upcomingRecycle.setLayoutManager(new LinearLayoutManager(getApplicationContext()));


        targetRecycle = (RecyclerView) findViewById(R.id.targetRecycle);
        targetRecycle.setItemAnimator(new DefaultItemAnimator());
        targetRecycle.setLayoutManager(new LinearLayoutManager(getApplicationContext()));

//        ItemTouchHelper.SimpleCallback todayTouchCallback = new RecyclerItemTouchHelper(0, ItemTouchHelper.LEFT, new RecyclerItemTouchHelper.RecyclerItemTouchHelperListener() {
//            @Override
//            public void onSwiped(RecyclerView.ViewHolder viewHolder, int direction, int position) {
//                if (InternetConnection.checkConnection(getApplicationContext()))
//                    removeFromToday((SalesPersonAdapter.ViewHolder) viewHolder, position);
//            }
//        });
//        new ItemTouchHelper(todayTouchCallback).attachToRecyclerView(todayRecycle);
//
//        ItemTouchHelper.SimpleCallback upcomingTouchCallback = new RecyclerItemTouchHelper(0, ItemTouchHelper.LEFT, new RecyclerItemTouchHelper.RecyclerItemTouchHelperListener() {
//            @Override
//            public void onSwiped(RecyclerView.ViewHolder viewHolder, int direction, int position) {
//                if (InternetConnection.checkConnection(getApplicationContext()))
//                    removeFromUpcoming((SalesPersonAdapter.ViewHolder) viewHolder, position);
//            }
//        });
//        new ItemTouchHelper(upcomingTouchCallback).attachToRecyclerView(upcomingRecycle);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        LayoutInflater inflater = (LayoutInflater) this.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View contentView = inflater.inflate(R.layout.activity_home_test, null, false);
        container.addView(contentView);

        FirebaseAnalytics mFirebaseAnalytics = FirebaseAnalytics.getInstance(this);


        initCreate();

        setWorkPlan();
        setSpotAward();

        Realm.init(getApplicationContext());
        Realm realm = Realm.getDefaultInstance();
        RealmResults<SalesPersonModel> results = realm.where(SalesPersonModel.class).findAll();
        if (results.size() > 0) {
            displayData();
            displayTarget();
        }

        APIRequest api = new APIRequest();
        api.setOverTargetAllResponseListener(new APIRequest.OverTargetAllResponse() {
            @Override
            public void onTargetAllReceived(List<TargetModel> list) {
                displayTarget();
            }


        });
        getAllTargetUsers(frame, getApplicationContext());

        APIRequest.getPlanPeriod(getApplicationContext());

        if (Preference.getInstance(getApplicationContext()).getRoleId().equals("1") || Preference.getInstance(getApplicationContext()).getRoleId().equals("2")) {

            APIRequest apiRequest = new APIRequest();
            apiRequest.setUsersResponse(new APIRequest.UsersResponse() {
                @Override
                public void onUserResponse() {
                    displayData();
                }
            });
            APIRequest.getUsers(getApplicationContext());
        } else {
            APIRequest.getUsers(getApplicationContext());
            APIRequest.getQuestionaryCategory(frame, getApplicationContext());
        }


    }

    public void setWorkPlan() {
        ArrayList<BarEntry> workBarEntry = new ArrayList<>();

        ArrayList<String> workBarLabel = new ArrayList<String>();
        workBarLabel.add("Required");
        workBarLabel.add("Planned");
        workBarLabel.add("Finished");


        int totalSchedules = 0, completedSchedules = 0, maxSchedules = 0;

        if (Preference.getInstance(getApplicationContext()).getStartDate() != null) {
            Realm.init(getApplicationContext());
            Realm realm = Realm.getDefaultInstance();
            RealmResults<ScheduleModel> scheduleModels = realm.where(ScheduleModel.class).findAll();

            if (!scheduleModels.isEmpty()) {
                if (Preference.getInstance(getApplicationContext()).getStartDate() != null) {
                    scheduleModels = realm.where(ScheduleModel.class)
                            .greaterThan("scheduleDate", convertToDate(YYYYMMdd(Preference.getInstance(getApplicationContext()).getStartDate())))
                            .and()
                            .equalTo("bossId", Preference.getInstance(getApplicationContext()).getUserId())
                            .and()
                            .notEqualTo("isDeleted", 1)
                            .findAll();
                    totalSchedules = scheduleModels.size();

                    scheduleModels = realm.where(ScheduleModel.class)
                            .lessThanOrEqualTo("scheduleDate", convertToDate(currentYYYYMMdd()))
                            .and()
                            .equalTo("status", 2)
                            .and()
                            .equalTo("bossId", Preference.getInstance(getApplicationContext()).getUserId())
                            .and()
                            .notEqualTo("isDeleted", 1)
                            .findAll();
                    completedSchedules = scheduleModels.size();

                    RealmResults<SalesPersonModel> salesPersonModels = realm.where(SalesPersonModel.class).findAll();
                    for (SalesPersonModel model : salesPersonModels)
                        maxSchedules = maxSchedules + model.getNo_of_days();
                }
            }
        }
        workBarEntry.add(new BarEntry(maxSchedules, 0));
        workBarEntry.add(new BarEntry(totalSchedules, 1));
        workBarEntry.add(new BarEntry(completedSchedules, 2));


        BarDataSet workBarDataSet = new BarDataSet(workBarEntry, "");
        workBarDataSet.setColors(ColorTemplate.COLORFUL_COLORS);

        workBar.notifyDataSetChanged();
        workBar.invalidate();
        BarData workBarData = new BarData(workBarLabel, workBarDataSet);
        workBar.setData(workBarData);

        workBar.setDescription("");  // set the description
        workBar.setTouchEnabled(false);
        workBar.getLegend().setEnabled(false);
    }

    public void setSpotAward() {

        ArrayList<BarEntry> spotBarEntry = new ArrayList<>();

        periodTextSpot.setText("Total Award" + " : " + String.valueOf(Preference.getInstance(getApplicationContext()).getMaxSpot()));
        spotBarEntry.add(new BarEntry(Preference.getInstance(getApplicationContext()).getMaxSpot(), 1));
        spotBarEntry.add(new BarEntry(Preference.getInstance(getApplicationContext()).getSpotCount(), 0));
        Log.d("Max Spot ", String.valueOf(Preference.getInstance(getApplicationContext()).getMaxSpot()));
        Log.d(" Spot Count ", String.valueOf(Preference.getInstance(getApplicationContext()).getSpotCount()));
        ArrayList<String> spotLabel = new ArrayList<String>();
        spotLabel.add("Award Given");
        spotLabel.add("Total Award");

        BarDataSet spotBarDataSet = new BarDataSet(spotBarEntry, "");
        spotBarDataSet.setColors(ColorTemplate.COLORFUL_COLORS);

        spotBar.notifyDataSetChanged();
        spotBar.invalidate();

        BarData spotBarData = new BarData(spotLabel, spotBarDataSet);
//        YAxis yAxis = spotBar.getAxisLeft();
//        yAxis.setAxisMinValue(0.0f);

        spotBar.setData(spotBarData);
        spotBar.setDescription("");  // set the description
        spotBar.setTouchEnabled(false);
        spotBar.getLegend().setEnabled(false);
    }

    @Override
    protected void onResume() {
        super.onResume();
        APIRequest apiRequest = new APIRequest();
        apiRequest.setUsersResponse(new APIRequest.UsersResponse() {
            @Override
            public void onUserResponse() {
                displayData();
            }
        });
        updateData();
    }

    private void updateData() {
        APIRequest.getSchedule(getApplicationContext());
        Preference.getInstance(getApplicationContext()).calculateSpotAward(getApplicationContext());
        setWorkPlan();
        setSpotAward();
        displayData();
    }

    private void displayData() {
        Realm.init(getApplicationContext());
        Realm realm = Realm.getDefaultInstance();

        String role = Preference.getInstance(getApplicationContext()).getRoleId();
        if (role.equals("1") || role.equals("2")) {
            todaySchedules = realm.where(ScheduleModel.class)
                    .equalTo("scheduleDate", convertToDate(currentYYYYMMdd()))
                    .and()
                    .equalTo("isDeleted", 0).findAll();

            upcomingSchedules = realm.where(ScheduleModel.class)
                    .greaterThan("scheduleDate", convertToDate(currentYYYYMMdd()))
                    .and()
                    .notEqualTo("status", 2)
                    .and()
                    .equalTo("isDeleted", 0)
                    .findAllSorted("scheduleDate", Sort.ASCENDING);

            todayList = todaySchedules;
            upcomingList = upcomingSchedules;
        } else {
            todaySchedules = realm.where(ScheduleModel.class)
                    .equalTo("scheduleDate", convertToDate(currentYYYYMMdd()))
                    .and()
                    .equalTo("bossId", Preference.getInstance(getApplicationContext()).getUserId())
                    .and()
                    .equalTo("isDeleted", 0).findAll();

            upcomingSchedules = realm.where(ScheduleModel.class)
                    .greaterThan("scheduleDate", convertToDate(currentYYYYMMdd()))
                    .and()
                    .equalTo("bossId", Preference.getInstance(getApplicationContext()).getUserId())
                    .and()
                    .notEqualTo("status", 2)
                    .and()
                    .equalTo("isDeleted", 0)
                    .findAllSorted("scheduleDate", Sort.ASCENDING);

            todayList = todaySchedules;
            upcomingList = upcomingSchedules;
        }

        todayAdapter = new SalesPersonAdapter(getApplicationContext(), frame, todayList, true);
        todayAdapter.setOnDeleteListener(new SalesPersonAdapter.DeleteSchedule() {
            @Override
            public void deleteSchedule(boolean today, int position) {
                if (today)
                    removeFromToday(position);
            }
        });
        todayRecycle.setAdapter(todayAdapter);

        upcomingList = upcomingSchedules;
        upcomingAdapter = new SalesPersonAdapter(getApplicationContext(), frame, upcomingList, false);
        upcomingAdapter.setOnDeleteListener(new SalesPersonAdapter.DeleteSchedule() {
            @Override
            public void deleteSchedule(boolean today, int position) {
                if (!today)
                    removeFromUpcoming(position);
            }
        });
        upcomingRecycle.setAdapter(upcomingAdapter);

        setWorkPlan();
        setSpotAward();
        setPeriod();
    }

    private void displayTarget() {
        Realm.init(getApplicationContext());
        Realm realm = Realm.getDefaultInstance();
        String role = Preference.getInstance(getApplicationContext()).getRoleId();
        if (role.equals("1") || role.equals("2")) {
            targetRecycle.setVisibility(View.VISIBLE);
            targetCard.setVisibility(View.VISIBLE);
            RealmResults<TargetModel> targetSchedules = realm.where(TargetModel.class)
                    .equalTo("period", Preference.getInstance(getApplicationContext()).getPlanPeriod())
                    .findAll();

            if (targetSchedules.size() > 0) {
                noTargetText.setVisibility(View.GONE);
                targetRecycle.setAdapter(new TargetAdapter(getApplicationContext(), targetSchedules));
            } else
                noTargetText.setVisibility(View.VISIBLE);
        }
    }

    private void setPeriod() {
        String planPeriod = Preference.getInstance(getApplicationContext()).getPlanPeriod();
        String startDate = Preference.getInstance(getApplicationContext()).getStartDate();
        String endDate = Preference.getInstance(getApplicationContext()).getEndDate();
        periodText.setText(planPeriod + " " + Utility.toDDMMYY(startDate) + " to " + Utility.toDDMMYY(endDate));
    }

    private void removeFromToday(final SalesPersonAdapter.ViewHolder viewHolder, int position) {
        // get the removed item name to display it in snack bar
        int salesmanId = todayList.get(viewHolder.getAdapterPosition()).getSalesmanId();

        Realm.init(getApplicationContext());
        Realm realm = Realm.getDefaultInstance();
        final SalesPersonModel salesPersonModels = realm.where(SalesPersonModel.class)
                .equalTo("empid", salesmanId)
                .and()
                .equalTo("isDeleted", 0)
                .findFirst();

        // backup of removed item for undo purpose
        final ScheduleModel deletedItem = todayList.get(viewHolder.getAdapterPosition());
        final int deletedIndex = viewHolder.getAdapterPosition();

        APIRequest apiRequest = new APIRequest();
        apiRequest.setOnSchduleDeleteListener(new APIRequest.DeleteResponse() {
            @Override
            public void onSchdeuleDeleted(boolean status) {
                if (status) {
                    // remove the item from recycler view
//                    todayAdapter.removeItem(viewHolder.getAdapterPosition());

                    // showing snack bar with Undo option
                    Snackbar snackbar = Snackbar.make(frame, salesPersonModels.getUser_name() + " schedule deleted!", Snackbar.LENGTH_LONG);
//        snackbar.setAction("UNDO", new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//
//                // undo is selected, restore the deleted item
//                todayAdapter.restoreItem(deletedItem, deletedIndex);
//            }
//        });
//        snackbar.setActionTextColor(Color.YELLOW);
                    snackbar.show();
                    updateData();
                }
            }
        });
        deleteSchedule(frame, getApplicationContext(), todayList.get(viewHolder.getAdapterPosition()).getId());
        toast(frame, getApplicationContext(), "Deleting Schedule of " + salesPersonModels.getUser_name());
    }

    private void removeFromToday(final int position) {
        // get the removed item name to display it in snack bar
        int salesmanId = todayList.get(position).getSalesmanId();

        Realm.init(getApplicationContext());
        Realm realm = Realm.getDefaultInstance();
        final SalesPersonModel salesPersonModels = realm.where(SalesPersonModel.class)
                .equalTo("empid", salesmanId)
                .and()
                .equalTo("isDeleted", 0)
                .findFirst();

        // backup of removed item for undo purpose
        final ScheduleModel deletedItem = todayList.get(position);
        final int deletedIndex = position;
        APIRequest apiRequest = new APIRequest();
        apiRequest.setOnSchduleDeleteListener(new APIRequest.DeleteResponse() {
            @Override
            public void onSchdeuleDeleted(boolean status) {

                if (status) {
                    // remove the item from recycler view
//                    todayAdapter.removeItem(position);

                    // showing snack bar with Undo option
                    Snackbar snackbar = Snackbar.make(frame, salesPersonModels.getUser_name() + " schedule deleted!", Snackbar.LENGTH_LONG);
//        snackbar.setAction("UNDO", new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//
//                // undo is selected, restore the deleted item
//                todayAdapter.restoreItem(deletedItem, deletedIndex);
//            }
//        });
//        snackbar.setActionTextColor(Color.YELLOW);
                    snackbar.show();
                    updateData();
                }
            }
        });
        deleteSchedule(frame, getApplicationContext(), todayList.get(position).getId());
        toast(frame, getApplicationContext(), "Deleting Schedule of " + salesPersonModels.getUser_name());
    }

    private void removeFromUpcoming(final SalesPersonAdapter.ViewHolder viewHolder, int position) {
        // get the removed item name to display it in snack bar
        int salesmanId = upcomingList.get(viewHolder.getAdapterPosition()).getSalesmanId();

        Realm.init(getApplicationContext());
        Realm realm = Realm.getDefaultInstance();
        final SalesPersonModel salesPersonModels = realm.where(SalesPersonModel.class)
                .equalTo("empid", salesmanId)
                .and()
                .equalTo("isDeleted", 0)
                .findFirst();

        // backup of removed item for undo purpose
        final ScheduleModel deletedItem = upcomingList.get(viewHolder.getAdapterPosition());
        final int deletedIndex = viewHolder.getAdapterPosition();

        APIRequest apiRequest = new APIRequest();
        apiRequest.setOnSchduleDeleteListener(new APIRequest.DeleteResponse() {
            @Override
            public void onSchdeuleDeleted(boolean status) {

                if (status) {
                    // remove the item from recycler view
//                    upcomingAdapter.removeItem(viewHolder.getAdapterPosition());

                    // showing snack bar with Undo option
                    Snackbar snackbar = Snackbar.make(frame, salesPersonModels.getUser_name() + " schedule deleted!", Snackbar.LENGTH_LONG);
//        snackbar.setAction("UNDO", new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//
//                // undo is selected, restore the deleted item
//                todayAdapter.restoreItem(deletedItem, deletedIndex);
//            }
//        });
//        snackbar.setActionTextColor(Color.YELLOW);
                    snackbar.show();
                    updateData();
                }
            }
        });
        deleteSchedule(frame, getApplicationContext(), upcomingList.get(viewHolder.getAdapterPosition()).getId());
        toast(frame, getApplicationContext(), "Deleting Schedule of " + salesPersonModels.getUser_name());
    }

    private void removeFromUpcoming(final int position) {
        // get the removed item name to display it in snack bar
        int salesmanId = upcomingList.get(position).getSalesmanId();

        Realm.init(getApplicationContext());
        Realm realm = Realm.getDefaultInstance();
        final SalesPersonModel salesPersonModels = realm.where(SalesPersonModel.class)
                .equalTo("empid", salesmanId)
                .and()
                .equalTo("isDeleted", 0)
                .findFirst();

        // backup of removed item for undo purpose
        final ScheduleModel deletedItem = upcomingList.get(position);
        final int deletedIndex = position;
        APIRequest apiRequest = new APIRequest();
        apiRequest.setOnSchduleDeleteListener(new APIRequest.DeleteResponse() {
            @Override
            public void onSchdeuleDeleted(boolean status) {

                if (status) {
                    // remove the item from recycler view
//                    upcomingAdapter.removeItem(position);

                    // showing snack bar with Undo option
                    Snackbar snackbar = Snackbar.make(frame, salesPersonModels.getUser_name() + " schedule deleted!", Snackbar.LENGTH_LONG);
//        snackbar.setAction("UNDO", new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//
//                // undo is selected, restore the deleted item
//                todayAdapter.restoreItem(deletedItem, deletedIndex);
//            }
//        });
//        snackbar.setActionTextColor(Color.YELLOW);
                    snackbar.show();
                    updateData();
                }
            }
        });
        deleteSchedule(frame, getApplicationContext(), upcomingList.get(position).getId());
        toast(frame, getApplicationContext(), "Deleting Schedule of " + salesPersonModels.getUser_name());
    }
}