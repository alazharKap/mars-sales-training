package com.mars.salestrainingapp.Activities;

import android.app.ProgressDialog;
import android.os.Bundle;
import android.support.v7.app.ActionBar;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SearchView;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;

import com.mars.salestrainingapp.API.APIRequest;
import com.mars.salestrainingapp.Adapter.PlanPeriodAdapter;
import com.mars.salestrainingapp.Model.SalesPersonModel;
import com.mars.salestrainingapp.R;

import io.realm.Case;
import io.realm.Realm;
import io.realm.RealmResults;

public class PlanPeriod extends AppCompatActivity implements SearchView.OnQueryTextListener{

    private Realm mRealm;

    private static final String TAG = "PlanPeriod";

    private RecyclerView recycle;
    private Toolbar toolbar;
    private SearchView searchView;
    RealmResults<SalesPersonModel> results;


    ProgressDialog progressBar;

    private void initCreate(){
        recycle = (RecyclerView) findViewById(R.id.recycle);
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        recycle.setItemAnimator(new DefaultItemAnimator());
        recycle.setLayoutManager(new LinearLayoutManager(getApplicationContext()));
        searchView = (SearchView) findViewById(R.id.search_user);
        progressBar = new ProgressDialog(PlanPeriod.this);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_plan_period);
        Realm.init(this);
        mRealm = Realm.getDefaultInstance();

        initCreate();
        setToolbar();

        results = mRealm.where(SalesPersonModel.class).findAll();
        searchView.setOnQueryTextListener(this);

        showUsers();
    }

    private void setToolbar() {
        ActionBarDrawerToggle mDrawerToggle;
        setSupportActionBar(toolbar);

        ActionBar actionBar = getSupportActionBar();
        if (actionBar != null) {
            actionBar.setDisplayHomeAsUpEnabled(true);
            actionBar.setTitle("Plan Period");
            actionBar.setHomeAsUpIndicator(getResources().getDrawable(R.drawable.back));
        }
    }

    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater menuInflater = getMenuInflater();
        menuInflater.inflate(R.menu.search_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                back();
                break;
            case R.id.action_search:
                searchView.setVisibility(View.VISIBLE);
                break;
        }
        return true;
    }

    @Override
    public void onBackPressed() {
      back();
    }

    private void back() {
        if (searchView.getVisibility() == View.VISIBLE) {
            searchView.clearFocus();
            searchView.setVisibility(View.GONE);
        }
        else
            finish();
    }

    @Override
    protected void onResume() {
        super.onResume();
        APIRequest apiRequest = new APIRequest();
        apiRequest.setUsersResponse(new APIRequest.UsersResponse() {
            @Override
            public void onUserResponse() {
                results = mRealm.where(SalesPersonModel.class).findAll();
                showUsers();
                progressBar.dismiss();
            }
        });

        progressBar.setMessage("Loading...");
        progressBar.setCancelable(false);
        progressBar.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        progressBar.show();
        APIRequest.getSchedule(getApplicationContext());
    }

    private void showUsers() {

        recycle.setAdapter(new PlanPeriodAdapter(getApplicationContext(), this, results));
    }

    @Override
    public boolean onQueryTextSubmit(String query) {
        searchUser(query);
        return false;
    }

    private void searchUser(String query) {
        if (query.isEmpty())
            results = mRealm.where(SalesPersonModel.class).equalTo("isDeleted",0).findAll();
        else
            results = mRealm
                .where(SalesPersonModel.class)
                .contains("user_name", query, Case.INSENSITIVE)
                .and()
                .equalTo("isDeleted",0)
                .findAll();
        showUsers();
    }

    @Override
    public boolean onQueryTextChange(String newText) {
        searchUser(newText);
        return false;
    }
}
