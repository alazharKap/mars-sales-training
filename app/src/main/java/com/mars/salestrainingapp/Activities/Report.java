package com.mars.salestrainingapp.Activities;

import android.os.Bundle;
import android.support.v7.app.ActionBar;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SearchView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.Spinner;

import com.adevole.customresources.CustomButton;
import com.mars.salestrainingapp.API.APIRequest;
import com.mars.salestrainingapp.Adapter.ReportAdapter;
import com.mars.salestrainingapp.Helper.InternetConnection;
import com.mars.salestrainingapp.Model.AllUsers;
import com.mars.salestrainingapp.R;

import java.util.ArrayList;
import java.util.List;

import io.realm.Case;
import io.realm.Realm;
import io.realm.RealmResults;

import static com.mars.salestrainingapp.API.APIRequest.getAllUsers;
import static com.mars.salestrainingapp.API.APIRequest.sendMail;
import static com.mars.salestrainingapp.Helper.Utility.internetError;

public class Report extends AppCompatActivity implements SearchView.OnQueryTextListener{

    private Realm mRealm;

    private static final String TAG = "Report";

    private RecyclerView recycle;
    private List<AllUsers> allUsersList = new ArrayList<>();
    private Toolbar toolbar;
    private SearchView searchView;
    private LinearLayout frame;
    RealmResults<AllUsers> results;
    public static View view;
    public static AlertDialog.Builder popup;
    public static AlertDialog alertDialog;
    static public Spinner reportSpinner;
    public static CustomButton generate;
    private void initCreate(){
        frame = (LinearLayout) findViewById(R.id.frame);
        recycle = (RecyclerView) findViewById(R.id.recycle);
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        recycle.setItemAnimator(new DefaultItemAnimator());
        recycle.setLayoutManager(new LinearLayoutManager(getApplicationContext()));
        searchView = (SearchView) findViewById(R.id.search_user);

        view = getLayoutInflater().inflate(R.layout.report_popup,null);
        reportSpinner = (Spinner) view.findViewById(R.id.report_spinner);
        generate = (CustomButton) view.findViewById(R.id.generate_report_button);
        popup = new AlertDialog.Builder(Report.this);
        popup.setView(view);
        popup.setCancelable(true);
        alertDialog = popup.create();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_report);

        Realm.init(this);
        mRealm = Realm.getDefaultInstance();

        initCreate();
        setToolbar();

        if (allUsersList != null)
            allUsersList.clear();
        results = mRealm.where(AllUsers.class).findAll();
        allUsersList.addAll(results);
        searchView.setOnQueryTextListener(this);

        showUsers();

        APIRequest apiRequest = new APIRequest();
        apiRequest.setAllUsersResponse(new APIRequest.AllUsersResponse() {
            @Override
            public void onAllUserResponse() {
                showUsers();
            }
        });


        getAllUsers(getApplicationContext());

        generate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                    if (InternetConnection.checkConnection(getApplicationContext())) {
                      sendMail(frame,getApplicationContext(),allUsersList.get(ReportAdapter.pos).getId());
                      Log.d("adapter list index"," "+ReportAdapter.pos);

                    }
                    else
                        internetError(frame, getApplicationContext());

                    alertDialog.dismiss();
            }
        });
    }

    private void setToolbar() {
        ActionBarDrawerToggle mDrawerToggle;
        setSupportActionBar(toolbar);

        ActionBar actionBar = getSupportActionBar();
        if (actionBar != null) {
            actionBar.setDisplayHomeAsUpEnabled(true);
            actionBar.setTitle("Reports");
            actionBar.setHomeAsUpIndicator(getResources().getDrawable(R.drawable.back));
        }
    }

    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater menuInflater = getMenuInflater();
        menuInflater.inflate(R.menu.search_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                back();
                break;
            case R.id.action_search:
                searchView.setVisibility(View.VISIBLE);
                break;
        }
        return true;
    }

    @Override
    public void onBackPressed() {
        back();
    }

    private void back() {
        if (searchView.getVisibility() == View.VISIBLE) {
            searchView.clearFocus();
            searchView.setVisibility(View.GONE);
        }
        else
            finish();
    }

    @Override
    protected void onResume() {
        super.onResume();
        if (allUsersList != null)
            allUsersList.clear();
        results = mRealm.where(AllUsers.class).findAll();
        allUsersList.addAll(results);
        showUsers();
    }

    private void showUsers() {
        recycle.setAdapter(new ReportAdapter(frame, getApplicationContext(), results));
    }

    @Override
    public boolean onQueryTextSubmit(String query) {
        searchUser(query);
        return false;
    }

    private void searchUser(String query) {
        if (query.isEmpty()) {
            if (allUsersList != null)
                allUsersList.clear();
            results = mRealm.where(AllUsers.class).equalTo("isDeleted", 0).findAll();
            allUsersList.addAll(results);
        }
        else
            if (allUsersList != null)
                allUsersList.clear();
            results = mRealm
                    .where(AllUsers.class)
                    .contains("userName", query, Case.INSENSITIVE)
                    .and()
                    .equalTo("isDeleted",0)
                    .findAll();
            allUsersList.addAll(results);
        showUsers();
    }

    @Override
    public boolean onQueryTextChange(String newText) {
        searchUser(newText);
        return false;
    }
}
