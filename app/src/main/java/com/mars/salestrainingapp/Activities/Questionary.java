package com.mars.salestrainingapp.Activities;

import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.support.design.widget.TextInputEditText;
import android.support.v7.app.ActionBar;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.EditText;
import android.widget.RelativeLayout;

import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.mars.salestrainingapp.API.APIRequest;
import com.mars.salestrainingapp.Adapter.QuestionaryAdapter;
import com.mars.salestrainingapp.Helper.InternetConnection;
import com.mars.salestrainingapp.Model.FeedbackModel;
import com.mars.salestrainingapp.Model.QuestionaryCategoryModel;
import com.mars.salestrainingapp.Model.QuestionaryModel;
import com.mars.salestrainingapp.R;

import java.util.HashMap;
import java.util.List;

import br.com.simplepass.loading_button_lib.customViews.CircularProgressButton;
import io.realm.Realm;
import io.realm.RealmResults;

import static com.mars.salestrainingapp.API.APIRequest.submitFeedback;
import static com.mars.salestrainingapp.Helper.Utility.internetError;
import static com.mars.salestrainingapp.Helper.Utility.log;
import static com.mars.salestrainingapp.Helper.Utility.toast;

public class Questionary extends AppCompatActivity implements View.OnClickListener{

    private int scheduleId;

    private RelativeLayout frame;
    private Toolbar toolbar;
    private RecyclerView recyclerView;
    private CircularProgressButton submitButton;
    private TextInputEditText etStrength,etDevelopment;
//    private CustomEditText questionNoteEdit;

    private String note = "";

    public static HashMap<Integer, Integer> feedback = new HashMap<>();

    private void initCreate() {
        Bundle extra = getIntent().getExtras();
        if (extra != null) {
            scheduleId = extra.getInt("scheduleId");
        }
        else
            finish();

        frame = (RelativeLayout) findViewById(R.id.frame);
        toolbar = (Toolbar) findViewById(R.id.toolbar);


        etStrength=(TextInputEditText) findViewById(R.id.et_strength);
        etDevelopment=(TextInputEditText) findViewById(R.id.et_development);

        recyclerView = (RecyclerView) findViewById(R.id.recycle);
        recyclerView.setLayoutManager(new LinearLayoutManager(getApplicationContext()));
        recyclerView.setItemAnimator(new DefaultItemAnimator());

//        questionNoteEdit = (CustomEditText) findViewById(R.id.questionNoteEdit);
//        questionNoteEdit.setVisibility(View.GONE);
        submitButton = (CircularProgressButton) findViewById(R.id.submit);
        submitButton.setOnClickListener(this);
        submitButton.setVisibility(View.GONE);
    }

    private void setToolbar() {
        ActionBarDrawerToggle mDrawerToggle;
        setSupportActionBar(toolbar);

        ActionBar actionBar = getSupportActionBar();
        if (actionBar != null) {
            actionBar.setDisplayHomeAsUpEnabled(true);
            actionBar.setTitle(getString(R.string.questionary));
            actionBar.setHomeAsUpIndicator(getResources().getDrawable(R.drawable.back));
        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_questionary);

        initCreate();
        setToolbar();

        fromRealm();

        APIRequest apiRequest = new APIRequest();
        apiRequest.setQuestionaryResponse(new APIRequest.QuestionaryResponse() {
            @Override
            public void onQuestionary() {
                fromRealm();
            }
        });
        APIRequest.getQuestionaryCategory(frame, getApplicationContext());
    }

    private void fromRealm() {
        Realm.init(getApplicationContext());
        Realm realm = Realm.getDefaultInstance();
        RealmResults<QuestionaryCategoryModel> results = realm.where(QuestionaryCategoryModel.class).equalTo("isDeleted",0).findAll();
        RealmResults<QuestionaryModel> questionaryResult = realm.where(QuestionaryModel.class).and().equalTo("isDelete",0).findAll();

        for (QuestionaryModel questionaryModel : questionaryResult)
           feedback.put(questionaryModel.getId(),2);

        if (!results.isEmpty())
            submitButton.setVisibility(View.VISIBLE);

        recyclerView.setAdapter(new QuestionaryAdapter(getApplicationContext(), results));
    }

    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
//        backError();
    }

    private void backError() {
        toast(frame, getApplicationContext(), getString(R.string.questionary_back_error));
    }

    @Override
    public void onClick(View v) {
        if (v.getId() == R.id.submit)
        {
            submitData();
        }
           // openDialog();
    }

    private void submitData() {
        Realm.init(getApplicationContext());
        Realm realm = Realm.getDefaultInstance();
        RealmResults<QuestionaryModel> questionaryResult = realm.where(QuestionaryModel.class).findAll();

        JsonArray array = new JsonArray();

        for (int i = 0 ; i < questionaryResult.size(); i++) {
            log("Question Feedback", "Id - " + questionaryResult.get(i).getId() + " Value - " + feedback.get(questionaryResult.get(i).getId()));

            JsonObject object = new JsonObject();
            object.addProperty("id", questionaryResult.get(i).getId());
            object.addProperty("rating", feedback.get(questionaryResult.get(i).getId()));
            array.add(object);
        }

        if (InternetConnection.checkConnection(getApplicationContext())) {
            APIRequest apiRequest = new APIRequest();
            apiRequest.setOnFeedbackListener(new APIRequest.FeedbackResponse() {
                @Override
                public void onFeedbackSubmitted(boolean status) {
                    submitButton.revertAnimation();
                    if (status) {
                        Intent i = new Intent(Questionary.this, Home.class);
                        i.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                        startActivity(i);
                        finish();
                    }
                }

                @Override
                public void onFeedbackReceived(List<FeedbackModel> feedbackModelList) {

                }
            });
            submitButton.startAnimation();
//            submitFeedback(frame, getApplicationContext(), scheduleId, array.toString(), questionNoteEdit.getText().toString());
            String strength=etStrength.getText().toString();
            String development=etDevelopment.getText().toString();
            submitFeedback(frame, getApplicationContext(), scheduleId, array.toString(),strength,development);
        }
        else
            internetError(frame, getApplicationContext());
    }

    private void openDialog() {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
     //   AlertDialog dialog;
        builder.setTitle(getString(R.string.feedback));


        final EditText input = new EditText(getApplicationContext());
        input.setTextColor(Color.BLACK);
        input.setHeight(200);
        builder.setView(input);


       /* InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.showSoftInput(input, InputMethodManager.SHOW_IMPLICIT);
//        dialog = builder.create();
//        dialog.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_VISIBLE);
//        dialog.show();
*/
        builder.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                note = input.getText().toString();
                submitData();

            }
        });
        builder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.cancel();
            }
        });

        builder.show();


    }
}