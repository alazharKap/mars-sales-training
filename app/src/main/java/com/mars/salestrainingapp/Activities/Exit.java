package com.mars.salestrainingapp.Activities;

import android.app.DatePickerDialog;
import android.app.Dialog;
import android.app.DialogFragment;
import android.content.SharedPreferences;
import android.os.Handler;
import android.support.constraint.ConstraintLayout;
import android.support.v7.app.ActionBar;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.text.InputFilter;
import android.text.Spanned;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.DatePicker;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.SpinnerAdapter;

import com.adevole.customresources.CustomEditText;
import com.adevole.customresources.CustomTextView;
import com.mars.salestrainingapp.API.APIRequest;
import com.mars.salestrainingapp.Helper.InternetConnection;
import com.mars.salestrainingapp.Helper.Preference;
import com.mars.salestrainingapp.Model.SalesPersonModel;
import com.mars.salestrainingapp.R;

import java.util.ArrayList;
import java.util.Calendar;

import br.com.simplepass.loading_button_lib.customViews.CircularProgressButton;
import io.realm.Realm;
import io.realm.RealmQuery;
import io.realm.RealmResults;

import static com.mars.salestrainingapp.API.APIRequest.exitUser;
import static com.mars.salestrainingapp.Helper.Utility.internetError;
import static com.mars.salestrainingapp.Helper.Utility.toast;

public class Exit extends AppCompatActivity implements View.OnClickListener{

    private ConstraintLayout frame;
    private Spinner userSpinner;
    private static CustomTextView exitDateText;
    private CustomEditText exitNoteEdit;
    private CircularProgressButton exitSubmitButton;
    private Toolbar toolbar;
    private static int currYear, currMonth, currDate;

    private String exitDate, exitNote;
    private int exitUserId;
    private String blockCharacterSet = "~#^|$%&*!{}[]/,`:;'@%()-_+|";
    private void initCreate() {
        frame = (ConstraintLayout) findViewById(R.id.frame);
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        userSpinner = (Spinner) findViewById(R.id.userSpinner);
        exitDateText = (CustomTextView) findViewById(R.id.exitDateText);
        exitDateText.setOnClickListener(this);
        ImageView exitDatePicker = (ImageView) findViewById(R.id.exitDatePicker);
        exitDatePicker.setOnClickListener(this);
        exitNoteEdit = (CustomEditText) findViewById(R.id.exitNoteEdit);
        exitSubmitButton = (CircularProgressButton) findViewById(R.id.exitSubmitButton);
        exitSubmitButton.setOnClickListener(this);
        userSpinner.setAdapter(getUserListSpinner());
        exitNoteEdit.setFilters(new InputFilter[]{filter});
    }
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_exit);

        initCreate();
        setToolbar();
        setCalender();

        exitDateText.setText(currDate+"-"+(currMonth+1)+"-"+currYear);
    }

    public SpinnerAdapter getUserListSpinner() {
        Realm.init(getApplicationContext());
        Realm realm = Realm.getDefaultInstance();
        RealmQuery<SalesPersonModel> query = realm.where(SalesPersonModel.class);
        RealmResults<SalesPersonModel> results = query.equalTo("flag",0).findAll();

        ArrayList<String> users = new ArrayList<>();
        users.add(0,getString(R.string.select_user));
        for (SalesPersonModel model : results)
            users.add(model.getUser_name());

        ArrayAdapter<String> spinnerUserAdapter = new ArrayAdapter<String>(Exit.this, android.R.layout.simple_spinner_dropdown_item, users);

        return spinnerUserAdapter;
    }

    private void setToolbar() {
        ActionBarDrawerToggle mDrawerToggle;
        setSupportActionBar(toolbar);

        ActionBar actionBar = getSupportActionBar();
        if (actionBar != null) {
            actionBar.setDisplayHomeAsUpEnabled(true);
            actionBar.setTitle(getString(R.string.exit));
            actionBar.setHomeAsUpIndicator(getResources().getDrawable(R.drawable.back));
        }
    }

    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }

    private void setCalender() {
        final Calendar c = Calendar.getInstance();
        currYear = (c.get(Calendar.YEAR));
        currMonth = c.get(Calendar.MONTH);
        currDate = c.get(Calendar.DAY_OF_MONTH);
    }
    private InputFilter filter = new InputFilter() {
        @Override
        public CharSequence filter(CharSequence source, int start, int end, Spanned dest, int dstart, int dend) {
            if (source != null && blockCharacterSet.contains(("" + source))) {
                return "";
            }
            return null;
        }
    };

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.exitDatePicker:
            case R.id.exitDateText:
                DatePickerFragment dialog = new DatePickerFragment();
                dialog.show(getFragmentManager(), "Date Picker");
                break;
            case R.id.exitSubmitButton:
                if (InternetConnection.checkConnection(getApplicationContext())) {
                    exitDate = exitDateText.getText().toString();
                    if (userSpinner.getSelectedItemPosition() != 0) {
                        exitUserId = Preference.getInstance(getApplicationContext()).getSelectedUser(userSpinner.getSelectedItem().toString());
                        exitNote = exitNoteEdit.getText().toString();
                        if (!exitNote.isEmpty()) {
                            exitSubmitButton.startAnimation();
                            APIRequest apiRequest = new APIRequest();
                            apiRequest.setExitResponse(new APIRequest.ExitResponse() {
                                @Override
                                public void exitResponse(boolean status) {
                                    exitSubmitButton.revertAnimation();
                                    if (status) {
                                        toast(frame, getApplicationContext(), "Data Added Successfully");
                                        new Handler().postDelayed(new Runnable() {
                                            @Override
                                            public void run() {
                                                finish();
                                            }
                                        }, 1000);
                                    }
                                }
                            });
                            exitUser(frame, getApplicationContext(), exitUserId, exitDate, exitNote, Preference.getInstance(getApplicationContext()).getUserId());
                        } else
                            toast(frame, getApplicationContext(), "Notes cannot be empty");
                    } else
                        toast(frame, getApplicationContext(), "Select User");
                }
                else
                    internetError(frame, getApplicationContext());
                break;
        }
    }

    private boolean validDate(int selYear, int selMonth, int selDate) {
        if ((currYear-selYear) == 0) {
            //if same year, chk month
            if ((currMonth-selMonth) == 0) {
                //if same month, chk date
                if ((currDate-selDate) > 0)
                    return true;
                else
                    return false;
            }
            else if ((currMonth-selMonth) > 0) {
                return true;
            }
            else
                return false;
        }
        else if ((currYear-selYear) > 0) {
            return true;
        }
        return false;
    }

    public static class DatePickerFragment extends DialogFragment implements DatePickerDialog.OnDateSetListener {

        @Override
        public Dialog onCreateDialog(Bundle savedInstanceState) {
            return new DatePickerDialog(getActivity(), this, currYear, currMonth, currDate);
        }

        @Override
        public void onDateSet(DatePicker datePicker, int selYear, int selMonth, int selDate) {
            exitDateText.setText(selDate + "-" + (selMonth + 1) +"-"+ selYear + "");
        }
    }

}
