package com.mars.salestrainingapp.Activities;

import android.app.DatePickerDialog;
import android.app.Dialog;
import android.app.DialogFragment;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.app.ActionBar;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.InputFilter;
import android.text.Spanned;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.Toast;

import com.adevole.customresources.CustomEditText;
import com.adevole.customresources.CustomTextView;
import com.google.gson.JsonObject;
import com.mars.salestrainingapp.API.API;
import com.mars.salestrainingapp.API.APIList;
import com.mars.salestrainingapp.Helper.InternetConnection;
import com.mars.salestrainingapp.Helper.Preference;
import com.mars.salestrainingapp.R;

import java.util.Calendar;

import br.com.simplepass.loading_button_lib.customViews.CircularProgressButton;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

import static android.view.View.GONE;
import static com.mars.salestrainingapp.Helper.Preference.ERROR;
import static com.mars.salestrainingapp.Helper.Utility.internetError;
import static com.mars.salestrainingapp.Helper.Utility.log;
import static com.mars.salestrainingapp.Helper.Utility.responseError;
import static com.mars.salestrainingapp.Helper.Utility.toast;

public class Induction extends AppCompatActivity {

    private Toolbar toolbar;
    private CircularProgressButton btnInductionCreate;
    private RelativeLayout frame;
    static EditText editTextInductionName, editTextInductionNotes;
    static CustomTextView textInductionDate;
    String inductionName, inductionNotes, inductionDate;
    String inductonDistributorName, inductionEfxId, thirdPartyId, dsrSpinnerValue, dsrNewSpinnerValue;
    ImageButton btnInductionDatePicker;
    DatePickerFragment dialog;
    private String blockCharacterSet = "~#^|$%&*!{}[]/,`:;'@%()-_+|";
    private static final String TAG = "Induction";
    Spinner dsrSpinner, newDsrSpinner;
    CustomTextView efxIdText, thirdPartyText;
    CustomEditText efxIdEditText, thirdPartyEditText, disEditText;

    private void initCreate() {
        frame = (RelativeLayout) findViewById(R.id.frame);
        btnInductionCreate = findViewById(R.id.btnCreateInduction);

        editTextInductionName = findViewById(R.id.editTextInductionName);
        editTextInductionNotes = findViewById(R.id.editTextInductionNotes);
        btnInductionDatePicker = findViewById(R.id.btnInductionDatePicker);

        textInductionDate = findViewById(R.id.textInductionDate);
        toolbar = (Toolbar) findViewById(R.id.toolbar);


        efxIdText = (CustomTextView) findViewById(R.id.efxIdText);
        thirdPartyText = (CustomTextView) findViewById(R.id.thirdPartyText);


        efxIdEditText = (CustomEditText) findViewById(R.id.efxIdEditText);
        thirdPartyEditText = (CustomEditText) findViewById(R.id.thirdPartyEditText);
        disEditText = (CustomEditText) findViewById(R.id.disEditText);

        dsrSpinner = (Spinner) findViewById(R.id.dsrSpinner);
        newDsrSpinner = (Spinner) findViewById(R.id.newDsrSpinner);


        ArrayAdapter<CharSequence> adapter = ArrayAdapter.createFromResource(this,
                R.array.dsr, android.R.layout.simple_spinner_item);
// Specify the layout to use when the list of choices appears
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
// Apply the adapter to the spinner
        dsrSpinner.setAdapter(adapter);

        ArrayAdapter<CharSequence> adapter1 = ArrayAdapter.createFromResource(this,
                R.array.newdsr, android.R.layout.simple_spinner_item);
// Specify the layout to use when the list of choices appears
        adapter1.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
// Apply the adapter to the spinner
        newDsrSpinner.setAdapter(adapter1);


        dsrSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {

            public void onItemSelected(AdapterView<?> adapterView,
                                       View view, int i, long l) {
                if (i == 0) {


                    efxIdText.setVisibility(GONE);
                    efxIdEditText.setVisibility(GONE);

                    thirdPartyText.setVisibility(GONE);
                    thirdPartyEditText.setVisibility(GONE);
                    newDsrSpinner.setVisibility(GONE);
                } else if (i == 1) {
                    newDsrSpinner.setVisibility(View.VISIBLE);
                    efxIdText.setVisibility(GONE);
                    efxIdEditText.setVisibility(GONE);

                    thirdPartyText.setVisibility(GONE);
                    thirdPartyEditText.setVisibility(GONE);
                    newDsrSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                        @Override
                        public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {

                            if (i == 0) {
                                efxIdText.setVisibility(GONE);
                                efxIdEditText.setVisibility(GONE);

                                thirdPartyText.setVisibility(GONE);
                                thirdPartyEditText.setVisibility(GONE);
                            } else if (i == 1 ) {
                                efxIdText.setVisibility(View.VISIBLE);
                                efxIdEditText.setVisibility(View.VISIBLE);


                                thirdPartyText.setVisibility(GONE);
                                thirdPartyEditText.setVisibility(GONE);
                            }
                            else if (i == 2 ) {
                                efxIdText.setVisibility(View.VISIBLE);
                                efxIdEditText.setVisibility(View.VISIBLE);


                                thirdPartyText.setVisibility(GONE);
                                thirdPartyEditText.setVisibility(GONE);
                            }
                        }

                        @Override
                        public void onNothingSelected(AdapterView<?> adapterView) {

                        }

                    });

                } else if (i == 2) {
                    newDsrSpinner.setVisibility(GONE);

                    thirdPartyText.setVisibility(View.VISIBLE);
                    thirdPartyEditText.setVisibility(View.VISIBLE);

                    efxIdText.setVisibility(GONE);
                    efxIdEditText.setVisibility(GONE);
                }
                // TODO Auto-generated method stub
//                toast(MainActivity.this,"You Selected : "
//                        + difficultyLevelOptionsList.gframe, et(i)+" Level ",Toast.LENGTH_SHORT);

            }

            // If no option selected
            public void onNothingSelected(AdapterView<?> adapterView) {
                // TODO Auto-generated method stub

            }

        });


    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_induction);

        initCreate();
        setToolbar();

        dialog = new DatePickerFragment();

        btnInductionDatePicker.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.show(getFragmentManager(), "Date Picker");
            }
        });

        editTextInductionName.setFilters(new InputFilter[]{filter});
        editTextInductionNotes.setFilters(new InputFilter[]{filter});

        btnInductionCreate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                inductionName = editTextInductionName.getText().toString();
                inductionNotes = editTextInductionNotes.getText().toString();
                inductionDate = textInductionDate.getText().toString();


                inductonDistributorName = disEditText.getText().toString();
                inductionEfxId = efxIdEditText.getText().toString();
                thirdPartyId = thirdPartyEditText.getText().toString();

                dsrSpinnerValue = dsrSpinner.getSelectedItem().toString();

                dsrNewSpinnerValue = newDsrSpinner.getSelectedItem().toString();

                if (inductionName.equals(""))
                    toast(frame,getApplicationContext(), "Name cannot be empty");
                else if (inductionDate.equals(""))
                    toast(frame,getApplicationContext(), "Date cannot be empty");
                else if (inductonDistributorName.equals(""))
                    toast(frame,getApplicationContext(), "Distributor name  cannot be empty");
                else if (inductionNotes.equals("") || inductionNotes == null)
                    toast(frame,getApplicationContext(), "Notes cannot be empty");
                else if (dsrSpinnerValue.equals("Select Joinee Type"))
                    toast(frame,getApplicationContext(), "Please Select Joinee Type");
                else if (dsrSpinnerValue.equals("DSR")) {
                    if (dsrSpinner.getSelectedItemPosition() == 0)
                        toast(frame,getApplicationContext(), "Please Select DSR Type");
                    else if (dsrNewSpinnerValue.equals("New DSR") || dsrNewSpinnerValue.equals("Replacing")) {
                        if (inductionEfxId.equals("") || inductionEfxId == null)
                            toast(frame,getApplicationContext(), "EFX ID  cannot be empty");
                        else
                            submitData();
                    }
                    else
                        toast(frame,getApplicationContext(), "Please Select DSR Type");
                } else if (dsrSpinnerValue.equals("FSA")) {
                    if (thirdPartyId.equals("") || thirdPartyId == null)
                        toast(frame,getApplicationContext(), "Third Party ID cannot be empty");

                     else
                        submitData();
                }

//                else if(dsrNewSpinnerValue.equals("Select DSR Type")) {
//                    toast(getApplicationContext(), "Please Select DSR Type",
//            frame,                 Toast.LENGTH_SHORT);
//                }
//                else if(thirdPartyId.equals("")) {
//                    toast(getApplicationContext(), "Third Party ID  cannot be empty",
//            frame,                 Toast.LENGTH_SHORT);
//                }
//

            }


        });
    }

    private void submitData() {

        if (InternetConnection.checkConnection(getApplicationContext())) {

            btnInductionCreate.startAnimation();

            Retrofit retrofit = new Retrofit.Builder().baseUrl(APIList.BASE_URL).addConverterFactory(GsonConverterFactory.create()).build();
            API api = retrofit.create(API.class);


            Call<JsonObject> call = null;
            if (inductionEfxId.equals("") || inductionEfxId == null) {
                call = api.createUserInduction(inductionName, inductionNotes, inductionDate, Preference.getInstance(getApplicationContext()).getUserId(),
                        inductonDistributorName, dsrSpinnerValue, "", thirdPartyId);

                log(TAG, "Induction - " + call.request().url().toString());
            } else if (thirdPartyId.equals("") || thirdPartyId == null) {

                call = api.createUserInduction(inductionName, inductionNotes, inductionDate, Preference.getInstance(getApplicationContext()).getUserId(),
                        inductonDistributorName, dsrSpinnerValue, dsrNewSpinnerValue, inductionEfxId);
            }

            call.enqueue(new Callback<JsonObject>() {
                @Override
                public void onResponse(Call<JsonObject> call, Response<JsonObject> response) {
                    btnInductionCreate.revertAnimation();
                    if (response.isSuccessful()) {
                        JsonObject obj = response.body();

                        if (obj != null) {
                            log(TAG, obj.toString());

                            if (obj.has(ERROR)) {
                                toast(frame, getApplicationContext(), obj.get(ERROR).getAsString());
                            } else {
                                toast(frame, getApplicationContext(), "Data added successfully!");
                                new Handler().postDelayed(new Runnable() {
                                    @Override
                                    public void run() {
                                        finish();
                                    }
                                }, 1000);
                            }
                        } else
                            responseError(frame, getApplicationContext());
                    } else
                        responseError(frame, getApplicationContext());
                }

                @Override
                public void onFailure(Call<JsonObject> call, Throwable t) {
                    toast(frame, getApplicationContext(), "Failure");
                }
            });

            log(TAG, "Induction - " + call.request().url().toString());
        } else
            internetError(frame, getApplicationContext());
    }


    private void setToolbar() {
        ActionBarDrawerToggle mDrawerToggle;
        setSupportActionBar(toolbar);

        ActionBar actionBar = getSupportActionBar();
        if (actionBar != null) {
            actionBar.setDisplayHomeAsUpEnabled(true);
            actionBar.setTitle(getString(R.string.induction));
            actionBar.setHomeAsUpIndicator(getResources().getDrawable(R.drawable.back));
        }
    }

    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }

    private InputFilter filter = new InputFilter() {
        @Override
        public CharSequence filter(CharSequence source, int start, int end, Spanned dest, int dstart, int dend) {
            if (source != null && blockCharacterSet.contains(("" + source))) {
                return "";
            }
            return null;
        }
    };


    public static class DatePickerFragment extends DialogFragment implements DatePickerDialog.OnDateSetListener {

        int currYear, currMonth, currDate;

        @Override
        public Dialog onCreateDialog(Bundle savedInstanceState) {
            final Calendar c = Calendar.getInstance();

            currYear = (c.get(Calendar.YEAR));
            currMonth = c.get(Calendar.MONTH);
            currDate = c.get(Calendar.DAY_OF_MONTH);


            // Create a new instance of DatePickerDialog and return it
            return new DatePickerDialog(getActivity(), this, currYear, currMonth, currDate);
        }

        @Override
        public void onDateSet(DatePicker datePicker, int selYear, int selMonth, int selDate) {

            //if (validDate(selYear, selMonth, selDate)) {
            textInductionDate.setText(selDate + "-" + (selMonth + 1) + "-" + selYear + "");


            //}
//            else
//                toast(frame, getContext(), getString(R.string.select_valid_date));
        }

        private boolean validDate(int selYear, int selMonth, int selDate) {
            if ((currYear - selYear) == 0) {
                //if same year, chk month
                if ((currMonth - selMonth) == 0) {
                    //if same month, chk date
                    if ((currDate - selDate) > 0)
                        return true;
                    else
                        return false;
                } else if ((currMonth - selMonth) > 0) {
                    return true;
                } else
                    return false;
            } else if ((currYear - selYear) > 0) {
                return true;
            }
            return false;
        }
    }

}


