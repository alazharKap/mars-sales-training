package com.mars.salestrainingapp.Activities;

import android.app.ActivityManager;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.NavigationView;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBar;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.ImageView;

import com.adevole.customresources.CustomTextView;
import com.mars.salestrainingapp.API.APIRequest;
import com.mars.salestrainingapp.Helper.Preference;
import com.mars.salestrainingapp.R;

import java.util.List;

import io.realm.Realm;

import static com.mars.salestrainingapp.API.APIRequest.logoutUser;

public class Drawer extends AppCompatActivity implements NavigationView.OnNavigationItemSelectedListener, View.OnClickListener{

    protected FrameLayout container;
    protected Toolbar toolbar;
    private DrawerLayout drawerLayout;
    private NavigationView navigationView;
    private View navHeader;
    private ImageView close;
    private CustomTextView userName, email;
    APIRequest.LogoutResponse logoutResponse;

    private void initCreate() {

        navigationView = (NavigationView) findViewById(R.id.navigation_view);
        drawerLayout = (DrawerLayout) findViewById(R.id.drawer);
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        container = (FrameLayout) findViewById(R.id.container);
        navHeader = navigationView.getHeaderView(0);
        navHeader.setOnClickListener(this);
        userName = (CustomTextView) navHeader.findViewById(R.id.username);
        email = (CustomTextView) navHeader.findViewById(R.id.email);
        close = (ImageView) navHeader.findViewById(R.id.close);

        String role=  Preference.getInstance(getApplicationContext()).getRoleId();
        if(role.equals("1") || role.equals("2")) {
            Menu nav_Menu = navigationView.getMenu();
            nav_Menu.findItem(R.id.home).setVisible(false);
            nav_Menu.findItem(R.id.plan_period).setVisible(false);
            nav_Menu.findItem(R.id.induction).setVisible(false);
            nav_Menu.findItem(R.id.exit).setVisible(false);
        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_drawer);

        initCreate();
        setToolbar();
        setHeader();
    }

    private void setToolbar() {
        ActionBarDrawerToggle mDrawerToggle;
        setSupportActionBar(toolbar);

        ActionBar actionBar = getSupportActionBar();
        if (actionBar != null)
        {
            actionBar.setDisplayHomeAsUpEnabled(true);
//            actionBar.setTitle(getString(R.string.mars));
            mDrawerToggle = new ActionBarDrawerToggle(this, drawerLayout, toolbar, R.string.mars, R.string.mars)
            {

                public void onDrawerClosed(View view)
                {
                    supportInvalidateOptionsMenu();
                    //drawerOpened = false;
                }

                public void onDrawerOpened(View drawerView)
                {
                    supportInvalidateOptionsMenu();
                    //drawerOpened = true;
                }
            };
            mDrawerToggle.setDrawerIndicatorEnabled(true);
            mDrawerToggle.getDrawerArrowDrawable().setColor(getResources().getColor(R.color.white));
            drawerLayout.setDrawerListener(mDrawerToggle);
            mDrawerToggle.syncState();
        }
    }

    private void setHeader() {
        userName.setText(Preference.getInstance(getApplicationContext()).getName());
        email.setText(Preference.getInstance(getApplicationContext()).getEmail());
        close.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                drawerLayout.closeDrawers();
            }
        });
        navigationView.setNavigationItemSelectedListener(this);
    }

    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem item) {

        Intent i = null;

        switch (item.getItemId()) {
            case R.id.home:
                if (currentActivity("Home"))
                    drawerLayout.closeDrawers();
                else {
                    i = new Intent(this, Home.class);
                    i.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                    startActivity(i);
                    drawerLayout.closeDrawers();
                }
                break;
            case R.id.plan_period:
                if (currentActivity("PlanPeriod"))
                    drawerLayout.closeDrawers();
                else if (currentActivity("PlanPeriodDetail"))
                    finish();
                else {
                    i = new Intent(this, PlanPeriod.class);
                    i.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                    startActivity(i);
                    drawerLayout.closeDrawers();
                }
                break;
            case R.id.report:
                String roleId = Preference.getInstance(getApplicationContext()).getRoleId();
                if (roleId.equals("1") || roleId.equals("2")) {
                    drawerLayout.closeDrawers();
                    APIRequest.sendMail(container, getApplicationContext(), Preference.getInstance(getApplicationContext()).getUserId());
                }
                else {
                    i = new Intent(this, Report.class);
                    i.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                    startActivity(i);
                }
                break;
            case R.id.induction:
                i = new Intent(this, Induction.class);
                i.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(i);
                break;
            case R.id.exit:
                i = new Intent(this, Exit.class);
                i.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(i);
                break;
            case R.id.logout:
                drawerLayout.closeDrawers();
                APIRequest apiRequest = new APIRequest();
                apiRequest.setLogoutResponse(new APIRequest.LogoutResponse() {
                    @Override
                    public void onLogout() {
                        logout();
                    }
                });
                logoutUser(getApplicationContext());
                break;
        }

        return false;
    }

    private void logout() {
        Preference.getInstance(getApplicationContext()).clear();
        Realm.init(getApplicationContext());
        Realm realm = Realm.getDefaultInstance();
        realm.beginTransaction();
        realm.deleteAll();
        realm.commitTransaction();
        Intent i = new Intent(this, Login.class);
        i.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        Preference.getInstance(getApplicationContext()).setUserId(0);
        startActivity(i);
        finishAffinity();
    }

    private boolean currentActivity(String activity) {
        ActivityManager am = (ActivityManager) this.getSystemService(ACTIVITY_SERVICE);
        List<ActivityManager.RunningTaskInfo> taskInfo = am.getRunningTasks(1);
        if (taskInfo.get(0).topActivity.getClassName().equals("com.mars.salestrainingapp.Activities."+activity))
            return true;
        else
            return false;
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.nav_header:
                drawerLayout.closeDrawers();
                Intent i = new Intent(getApplicationContext(), Profile.class);
                i.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(i);
                break;
        }
    }
}