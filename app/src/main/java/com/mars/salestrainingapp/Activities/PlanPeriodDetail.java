package com.mars.salestrainingapp.Activities;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.ActionBar;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.RelativeLayout;
import android.widget.Toast;

import com.adevole.customresources.CustomTextView;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.mars.salestrainingapp.API.API;
import com.mars.salestrainingapp.API.APIList;
import com.mars.salestrainingapp.API.APIRequest;
import com.mars.salestrainingapp.Calendar.data.CalendarDate;
import com.mars.salestrainingapp.Calendar.data.Solar;
import com.mars.salestrainingapp.Calendar.fragment.CalendarViewFragment;
import com.mars.salestrainingapp.Calendar.fragment.CalendarViewPagerFragment;
import com.mars.salestrainingapp.Helper.InternetConnection;
import com.mars.salestrainingapp.Helper.Preference;
import com.mars.salestrainingapp.Helper.Utility;
import com.mars.salestrainingapp.Model.SalesPersonModel;
import com.mars.salestrainingapp.Model.ScheduleModel;
import com.mars.salestrainingapp.R;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import br.com.simplepass.loading_button_lib.customViews.CircularProgressButton;
import io.realm.Realm;
import io.realm.RealmResults;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

import static com.mars.salestrainingapp.Helper.Utility.YYYYMMdd;
import static com.mars.salestrainingapp.Helper.Utility.convertToDate;
import static com.mars.salestrainingapp.Helper.Utility.convertToString;
import static com.mars.salestrainingapp.Helper.Utility.currentYYYYMMdd;
import static com.mars.salestrainingapp.Helper.Utility.ddMMYYYY;
import static com.mars.salestrainingapp.Helper.Utility.log;
import static com.mars.salestrainingapp.Helper.Utility.toDDMMYY;
import static com.mars.salestrainingapp.Helper.Utility.toast;

public class PlanPeriodDetail extends AppCompatActivity implements View.OnClickListener,
            CalendarViewPagerFragment.OnPageChangeListener,
            CalendarViewFragment.OnDateClickListener,
            CalendarViewFragment.OnDateCancelListener,
            CalendarViewFragment.OnDateLoaded{

    private static final String TAG = "PlanPeriodDetail";

    int max, dateCount=0;

    SalesPersonModel model;

    boolean apiCall = false;

    CalendarViewFragment.OnDateCancelListener onDateCancelListener;
    private boolean isChoiceModelSingle = false;
    public List<CalendarDate> mListDate = new ArrayList<>();
    Fragment fragment;


    CustomTextView tv_name,tv_date;
    private String startRange = "";
    private String todaysDate = "";
    private String endRange = "";
    Realm realm;
    private Toolbar toolbar;

    List<ScheduleModel> deletedSchList = new ArrayList<>();

    int empid;

    private RelativeLayout frame;
    private FrameLayout frameLayout;
    private View frameCover;
    private CircularProgressButton planSaveBtn;

    private void initCreate() {
        frameCover  = (View) findViewById(R.id.frameCover);
        frameCover.setVisibility(View.INVISIBLE);

        frame  = (RelativeLayout) findViewById(R.id.frame);
        frameLayout = (FrameLayout) findViewById(R.id.calender_container);
        tv_name = (CustomTextView)findViewById(R.id.name);
        tv_date = (CustomTextView)findViewById(R.id.dates);
        planSaveBtn = (CircularProgressButton)findViewById(R.id.plan_save_btn);
        planSaveBtn.setOnClickListener(this);
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        fragment = CalendarViewPagerFragment.newInstance(isChoiceModelSingle);

        Bundle extras = getIntent().getExtras();
        empid = extras.getInt("empid");
    }

    private void setToolbar() {
        ActionBarDrawerToggle mDrawerToggle;
        setSupportActionBar(toolbar);

        ActionBar actionBar = getSupportActionBar();
        if (actionBar != null) {
            actionBar.setDisplayHomeAsUpEnabled(true);
            actionBar.setTitle("");
            actionBar.setHomeAsUpIndicator(getResources().getDrawable(R.drawable.back));
        }
    }

    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_plan_period_detial);

        initCreate();
        initFragment();
        setToolbar();

        startRange = Preference.getInstance(getApplicationContext()).getStartDate();
        todaysDate = ddMMYYYY(currentYYYYMMdd());
        endRange = getEndRange();

        fromRealm();
    }

    private void fromRealm() {
        Realm.init(this);
        realm = Realm.getDefaultInstance();
        model = realm.where(SalesPersonModel.class).equalTo("empid",empid).findFirst();

        dateCount = model.getScheduledCount();
        String numbers_to_display = dateCount+"/"+model.getNo_of_days();

        max = model.getNo_of_days();
        tv_name.setText(model.getUser_name());
        tv_date.setText(numbers_to_display);
    }

    private void initFragment() {
        FragmentManager fm = getSupportFragmentManager();
        FragmentTransaction tx = fm.beginTransaction();
        // Fragment fragment = new CalendarViewPagerFragment();
        tx.replace(frameLayout.getId(), fragment);
        tx.commit();
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.plan_save_btn:
                if (!mListDate.isEmpty() || !deletedSchList.isEmpty()) {
                    for (CalendarDate scheduledDate : mListDate )
                        log(TAG, "Schedule Date - "+scheduledDate.getSolar().solarDay+"-"+scheduledDate.getSolar().solarMonth+"-"+scheduledDate.getSolar().solarYear);
                    for (ScheduleModel model : deletedSchList)
                        log(TAG, "Deleted Data :: Schedule Id - "+model.getId()+" Schedule Date - "+convertToString(model.getScheduleDate()));
                    savePlan();
                }
                else
                    toast(frame, getApplicationContext(), "Select date");
                break;
        }
    }

    private String changeDateFormat(String s) {  // yyyy-mm-dd to dd-mm-yyyy
        String[] x = s.split("-");
        String newDate=x[2]+"-"+x[1]+"-"+x[0];
        return newDate;
    }

    private String getEndRange() {
        SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy");

        String endDate = "";

//        for (int i = 0; i < 28; i++) {
            final Date date;
            try {
                date = sdf.parse(startRange);
                final Calendar calendar = Calendar.getInstance();
                calendar.setTime(date);
                calendar.add(Calendar.DAY_OF_YEAR, 27);
                endDate = sdf.format(calendar.getTime());
            } catch (ParseException e) {
                e.printStackTrace();
            }
//        }
        return endDate;
    }

    @Override
    public void onDateClick(CalendarDate calendarDate) {
        if (!calendarDate.isUnSelectable()) {
            int year = calendarDate.getSolar().solarYear;
            int month = calendarDate.getSolar().solarMonth;
            int day = calendarDate.getSolar().solarDay;
            if (!isChoiceModelSingle) {
                if (!mListDate.isEmpty()) {
                    if (!mListDate.contains(calendarDate))
                        mListDate.add(calendarDate);
                }
                else
                    mListDate.add(calendarDate);

                dateCount++;
            }
        }
        else {
//            int year = calendarDate.getSolar().solarYear;
//            int month = calendarDate.getSolar().solarMonth;
//            int day = calendarDate.getSolar().solarDay;
//            if (!isChoiceModelSingle) {
//                Realm.init(getApplicationContext());
//                Realm realm = Realm.getDefaultInstance();
//                ScheduleModel model = realm.where(ScheduleModel.class)
//                        .equalTo("scheduleDate", convertToDate(year+"-"+month+"-"+day)).findFirst();
//                if (model != null)
//                    deletedSchList.add(model);
//            }
        }
    }

    @Override
    public void onDateCancel(CalendarDate calendarDate) {
        if (!calendarDate.isRemoveCurrentSelection()) {
            int count = mListDate.size();
            for (int i = 0; i < count; i++) {
                CalendarDate date = mListDate.get(i);
                if (date.getSolar().solarDay == calendarDate.getSolar().solarDay) {
                    mListDate.remove(i);
                    dateCount--;
                    break;
                }
            }
        }
        else {
            int year = calendarDate.getSolar().solarYear;
            int month = calendarDate.getSolar().solarMonth;
            int day = calendarDate.getSolar().solarDay;
            Realm.init(getApplicationContext());
            Realm realm = Realm.getDefaultInstance();
            ScheduleModel model = realm.where(ScheduleModel.class)
                    .equalTo("scheduleDate", convertToDate(year+"-"+month+"-"+day))
                    .and()
                    .equalTo("isDeleted",0)
                    .findFirst();
            if (model != null) {
                if (!deletedSchList.isEmpty()) {
                    if (!deletedSchList.contains(model))
                        deletedSchList.add(model);
                }
                else
                    deletedSchList.add(model);
            }

        }
    }

    @Override
    public void onPageChange(int year, int month) {
//        tv_date.setText(year + "-" + month);
        mListDate.clear();
    }

    private static String listToString(List<CalendarDate> list) {
        StringBuffer stringBuffer = new StringBuffer();
        for (CalendarDate date : list) {
            stringBuffer.append(date.getSolar().solarYear + "-" + date.getSolar().solarMonth + "-" + date.getSolar().solarDay).append(" ");
        }
        return stringBuffer.toString();
    }

    @Override
    public List<CalendarDate> onDateLoaded(List<CalendarDate> finalList) {
        Realm.init(getApplicationContext());
        RealmResults<ScheduleModel> results = realm.where(ScheduleModel.class)
                .and()
                .equalTo("bossId",Preference.getInstance(getApplicationContext()).getUserId())
                .and()
                .notEqualTo("isDeleted",1).findAll();
        for(int i=0; i<results.size(); i++) {
            finalList = setUnSelecteable(ddMMYYYY(convertToString(results.get(i).getScheduleDate())), finalList);
        }
        finalList = setSelectedBy(finalList);

        for (int i = 0; i< mListDate.size(); i++)
            finalList.get(i).setSelect(true);

        return finalList;
    }

    private List<CalendarDate> setSelectedBy(List<CalendarDate> finalList) {
        RealmResults<ScheduleModel> results = realm.where(ScheduleModel.class).and()
                .notEqualTo("isDeleted",1).and().equalTo("salesmanId",empid).findAll();
        for (int i = 0; i < results.size(); i++) {
            String scheduledDate = ddMMYYYY(convertToString(results.get(i).getScheduleDate()));
            for (int j = 0; j < finalList.size(); j++) {
                CalendarDate date = finalList.get(j);
                String formatedDate = date.getSolar().solarDay+"-"+date.getSolar().solarMonth+"-"+date.getSolar().solarYear;

                String tempDate = formatedDate.split("-")[0];
                String tempMonth = formatedDate.split("-")[1];
                if (tempDate.length() == 1)
                    formatedDate = "0"+tempDate.charAt(0)+"-"+formatedDate.split("-")[1]+"-"+formatedDate.split("-")[2];
                if (tempMonth.length() == 1)
                    formatedDate = formatedDate.split("-")[0]+"-"+"0"+tempMonth.charAt(0)+"-"+formatedDate.split("-")[2];

                if (formatedDate.equals(scheduledDate)) {
                    finalList.get(j).setSelectedBy(results.get(i).getBossId());
                    if (results.get(i).getSalesmanId() == empid)
                        finalList.get(j).setSameDSR(true);
                    else
                        finalList.get(j).setSameDSR(false);

                    finalList.get(j).setScheduleStatus(results.get(i).getStatus());
                    break;
                }
            }
        }
        return finalList;
    }

    public List<CalendarDate> setUnSelecteable(String unSelecteable, List<CalendarDate> finalList) {
        String tempDate = unSelecteable.split("-")[0];
        String tempMonth = unSelecteable.split("-")[1];
        if (tempDate.charAt(0) == '0')
            unSelecteable = tempDate.charAt(1)+"-"+unSelecteable.split("-")[1]+"-"+unSelecteable.split("-")[2];
        if (tempMonth.charAt(0) == '0')
            unSelecteable = unSelecteable.split("-")[0]+"-"+tempMonth.charAt(1)+"-"+unSelecteable.split("-")[2];
        for (int i = 0; i < finalList.size(); i++) {
            CalendarDate date = finalList.get(i);
            String formatedDate = date.getSolar().solarDay+"-"+date.getSolar().solarMonth+"-"+date.getSolar().solarYear;
            if (formatedDate.equals(unSelecteable)) {
                finalList.get(i).setUnSelectable(true);
                break;
            }
        }
        return finalList;
    }

    @Override
    public void dataLoaded() {
    }

    @Override
    public List<CalendarDate> outOffRange(List<CalendarDate> calendarDateList) {
        List<CalendarDate> updatedList = calendarDateList;
        SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy");
        Date currDate = new Date(), starDate = new Date(), endDate = new Date();
        try {
            currDate = sdf.parse(todaysDate);
            starDate = sdf.parse(startRange);
            endDate = sdf.parse(endRange);
        } catch (ParseException e) {
            e.printStackTrace();
        }


        for (int i = 0; i < calendarDateList.size(); i++) {
            int year = updatedList.get(i).getSolar().solarYear;
            int month = updatedList.get(i).getSolar().solarMonth;
            int day = updatedList.get(i).getSolar().solarDay;

            try {
                Date pointerDate = sdf.parse(day+"-"+month+"-"+year);

                if (pointerDate.before(currDate) && pointerDate.after(starDate) || (starDate.equals(pointerDate) && pointerDate.before(currDate) )) {
                    updatedList.get(i).setInRange(false);
                }
                else if ( (starDate.before(pointerDate)) && (endDate.after(pointerDate) || endDate.equals(pointerDate))) {
                    updatedList.get(i).setInRange(true);
                }
                else{
                    updatedList.get(i).setInRange(false);
                }
            } catch (ParseException e) {
                e.printStackTrace();
            }
        }
        return updatedList;
    }

    @Override
    public List<CalendarDate> finalList(List<CalendarDate> finalList) {
        return finalList;
    }

    private void savePlan() {
        if (InternetConnection.checkConnection(getApplicationContext())) {
            planSaveBtn.startAnimation();

            int bossId = Preference.getInstance(getApplicationContext()).getUserId();

            String selectedDate = "";
            if (!mListDate.isEmpty()) {
                selectedDate = getDateFromSolar(mListDate.get(0).getSolar());
                for(int i = 1; i < mListDate.size(); i++)
                    selectedDate = selectedDate + ","+ getDateFromSolar(mListDate.get(i).getSolar());
            }

            String deleted = "";
            if (!deletedSchList.isEmpty()) {
                deleted = String.valueOf(deletedSchList.get(0).getId());
                for (int j = 1; j < deletedSchList.size(); j++)
                    deleted = deleted + ","+deletedSchList.get(j).getId();
            }


            frameCover.setVisibility(View.VISIBLE);
            frameLayout.setEnabled(false);
            frameLayout.setClickable(false);

            Retrofit retrofit = new Retrofit.Builder().baseUrl(APIList.BASE_URL).addConverterFactory(GsonConverterFactory.create()).build();
            API api = retrofit.create(API.class);
            Call<JsonObject> call = api.ceateSchedule(empid,bossId,selectedDate, deleted);

            apiCall = true;

            log("Plan period detail","URL - "+call.request().url().toString());
            call.enqueue(new Callback<JsonObject>() {
                @Override
                public void onResponse(Call<JsonObject> call, Response<JsonObject> response) {

                    apiCall = false;
                    JsonObject object = response.body();
                    if (object.get("status").getAsString().equals("success")) {
                        APIRequest apiRequest = new APIRequest();
                        apiRequest.setUsersResponse(new APIRequest.UsersResponse() {
                            @Override
                            public void onUserResponse() {
                                planSaveBtn.revertAnimation();
                                Intent i = new Intent(PlanPeriodDetail.this, PlanPeriodDetail.class);
                                i.addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
                                i.putExtra("empid",empid);
                                startActivity(i);
                                finish();
                            }
                        });
                        APIRequest.getSchedule(getApplicationContext());
                    }
                    else {
                        frameCover.setVisibility(View.INVISIBLE);
                        frameLayout.setEnabled(true);
                        planSaveBtn.revertAnimation();
                        toast(frame, getApplicationContext(), getString(R.string.error_while_processing));
                    }
                }
                @Override
                public void onFailure(Call<JsonObject> call, Throwable t) {
                    apiCall = false;
                    frameCover.setVisibility(View.INVISIBLE);
                    planSaveBtn.revertAnimation();
                    frameLayout.setEnabled(true);
                    t.printStackTrace();
                }
            });
        }
    }

    private String getDateFromSolar(Solar solar) {
        return solar.solarDay+"-"+solar.solarMonth+"-"+solar.solarYear;
    }

    @Override
    public void onBackPressed() {
        if (!apiCall)
            super.onBackPressed();
        else
            toast(frame, getApplicationContext(), getString(R.string.hold_in_process));
    }
}