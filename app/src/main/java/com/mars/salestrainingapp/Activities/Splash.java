package com.mars.salestrainingapp.Activities;

import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.support.design.widget.CoordinatorLayout;
import android.support.v7.app.AppCompatActivity;
import android.transition.Fade;

import com.mars.salestrainingapp.API.APIRequest;
import com.mars.salestrainingapp.Helper.InternetConnection;
import com.mars.salestrainingapp.Helper.Preference;
import com.mars.salestrainingapp.Model.ScheduleModel;
import com.mars.salestrainingapp.R;

import io.realm.Realm;
import io.realm.RealmConfiguration;

import static com.mars.salestrainingapp.Helper.Utility.longToast;

public class Splash extends AppCompatActivity {

    CoordinatorLayout frame;
   // public static final String BROADCAST = "com.mars.salestrainingapp.Helper.InternetConnectionContinous.android.net.conn.CONNECTIVITY_CHANGE";
    @Override

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);
      //  InternetConnectionContinous i=new InternetConnectionContinous();
       // IntentFilter intentFilter = new IntentFilter(BROADCAST);
      //  Intent intent = new Intent(BROADCAST);


        frame = (CoordinatorLayout) findViewById(R.id.frame);

        Realm.init(getApplicationContext());
        RealmConfiguration configuration = new RealmConfiguration.Builder().deleteRealmIfMigrationNeeded().build();
        Realm.setDefaultConfiguration(configuration);
if(InternetConnection.checkConnection(getApplicationContext())) {
    new Handler().postDelayed(new Runnable() {
        @Override
        public void run() {
            if (Preference.getInstance(getApplicationContext()).getUserId() == 0)
                callLogin();
            else {
                APIRequest apiRequest = new APIRequest();
                apiRequest.setValidatePlanPeriod(new APIRequest.ValidatePlanPeriodResponse() {
                    @Override
                    public void changeInPlanPeriod(boolean status) {
                        if (status) {
                            Realm realm = Realm.getDefaultInstance();
                            realm.beginTransaction();
                            realm.deleteAll();
                            realm.commitTransaction();
                            callHome();
                        } else {
//                            Realm.init(getApplicationContext());
//                            Realm realm = Realm.getDefaultInstance();
//
//                            //Validating Day is completed...................................................
//                            ScheduleModel schedule = realm.where(ScheduleModel.class)
//                                    .equalTo("status", 1).findFirst();
//                            if (schedule != null)
                                //callOutlet(schedule);
                                callHome();
//                            else {
//                                //Validating Questions is completed.........................................
//                                schedule = realm.where(ScheduleModel.class)
//                                        .equalTo("status", 2)
//                                        .and()
//                                        .equalTo("questionFlag", 0).findFirst();
////                                    if (schedule != null)
////                                        callQuestionary(schedule);
////                                    else
//                                callHome();
//                            }
                        }
                    }
                });

                APIRequest.validatePeriod(frame, getApplicationContext());
            }
        }
    }, 3000);
}
else
{
    longToast(frame, getApplicationContext(), "Please Check Your Internet Connection !");
}
    }

    private void callHome() {
        Intent i = new Intent(Splash.this, Home.class);
        i.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        startActivity(i);
        finish();
    }

    private void callQuestionary(ScheduleModel schedule) {
        Intent i = new Intent(Splash.this, Questionary.class);
        i.putExtra("scheduleId", schedule.getId());
        i.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        startActivity(i);
        finish();
    }

    private void callOutlet(ScheduleModel schedule) {
        Intent i = new Intent(Splash.this, Previous.class);
        i.putExtra("empId", schedule.getSalesmanId());
        i.putExtra("bossId", schedule.getBossId());
        i.putExtra("scheduleId", schedule.getId());
        i.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        startActivity(i);
        finish();
    }

    private void callLogin() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            Fade fade = new Fade();
            fade.setDuration(1000);
            getWindow().setEnterTransition(fade);
        }
        Intent i = new Intent(Splash.this, Login.class);
        i.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        startActivity(i);
        finish();
    }
}