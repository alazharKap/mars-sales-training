package com.mars.salestrainingapp.Activities;

import android.content.Intent;
import android.net.Uri;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.RelativeLayout;

import com.adevole.customresources.CustomEditText;
import com.adevole.customresources.CustomTextView;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.mars.salestrainingapp.API.API;
import com.mars.salestrainingapp.API.APIList;
import com.mars.salestrainingapp.Helper.InternetConnection;
import com.mars.salestrainingapp.R;

import br.com.simplepass.loading_button_lib.customViews.CircularProgressButton;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

import static com.mars.salestrainingapp.Helper.Preference.ERROR;
import static com.mars.salestrainingapp.Helper.Preference.SUCCESS;
import static com.mars.salestrainingapp.Helper.Utility.internetError;
import static com.mars.salestrainingapp.Helper.Utility.log;
import static com.mars.salestrainingapp.Helper.Utility.responseError;
import static com.mars.salestrainingapp.Helper.Utility.toast;

public class ForgotPassword extends AppCompatActivity {

    private static final String TAG = "ForgotPassword";

    private RelativeLayout frame;
    private CustomEditText userEdit;
    private CustomTextView title;
    private CircularProgressButton nextButton;

    private void initCreate() {
        title = (CustomTextView) findViewById(R.id.title);
        title.setText(getString(R.string.forget_password));
        userEdit = (CustomEditText) findViewById(R.id.forgot_username);
        nextButton = (CircularProgressButton) findViewById(R.id.next);
        frame = (RelativeLayout) findViewById(R.id.frame);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_forgot_password);

        initCreate();

        nextButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (userEdit.getText().length()>0) {
                    if (InternetConnection.checkConnection(getApplicationContext()))
                        requestServer(userEdit.getText().toString());
                    else
                        internetError(frame, getApplicationContext());
                }
                else
                    toast(frame, getApplicationContext(), getString(R.string.enter_username_or_emailId));
            }
        });

    }

    private void requestServer(String s) {
        Retrofit retrofit = new Retrofit.Builder().baseUrl(APIList.BASE_URL).addConverterFactory(GsonConverterFactory.create()).build();
        API api = retrofit.create(API.class);
        Call<JsonArray> call = api.forget(s);
        nextButton.startAnimation();
        userEdit.setEnabled(false);
        log(TAG, "Forgot URL - "+call.request().url().toString());
        call.enqueue(new Callback<JsonArray>() {
            @Override
            public void onResponse(Call<JsonArray> call, Response<JsonArray> response) {
                nextButton.revertAnimation();
                userEdit.setEnabled(true);
                if (response.isSuccessful()) {
                    JsonArray array = response.body();
                    JsonObject object = array.get(0).getAsJsonObject();

                    if (object.has(ERROR))
                        toast(frame, getApplicationContext(), object.get(ERROR).getAsString());
                    else if (object.has(SUCCESS)) {
                        toast(frame, getApplicationContext(), object.get(SUCCESS).getAsString());

                        new Handler().postDelayed(new Runnable() {
                            @Override
                            public void run() {
                                finish();
                            }
                        },2000);

                    }
                }
                else
                    responseError(frame, getApplicationContext());
            }

            @Override
            public void onFailure(Call<JsonArray> call, Throwable t) {
                nextButton.revertAnimation();
                userEdit.setEnabled(false);
                responseError(frame, getApplicationContext());
            }
        });
    }
}
