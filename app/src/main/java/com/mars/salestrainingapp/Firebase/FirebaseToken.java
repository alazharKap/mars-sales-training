package com.mars.salestrainingapp.Firebase;

import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.iid.FirebaseInstanceIdService;
import com.mars.salestrainingapp.Helper.Preference;

import static com.mars.salestrainingapp.Helper.Utility.log;

/**
 * Created by surve on 16-Oct-17.
 */

public class FirebaseToken extends FirebaseInstanceIdService {

    private static final String TAG = "FirebaseToken";

    @Override
    public void onTokenRefresh() {
        super.onTokenRefresh();

        String refreshedToken = FirebaseInstanceId.getInstance().getToken();
        log(TAG, "Refreshed Token - "+refreshedToken);
        Preference.getInstance(getApplicationContext()).setFcmid(refreshedToken);
    }
}
