package com.mars.salestrainingapp.Firebase;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.media.RingtoneManager;
import android.net.Uri;
import android.support.v4.app.NotificationCompat;
import android.util.Log;

import com.google.firebase.messaging.FirebaseMessagingService;
import com.google.firebase.messaging.RemoteMessage;
import com.mars.salestrainingapp.Activities.Home;
import com.mars.salestrainingapp.R;
import com.mars.salestrainingapp.Model.NotificationModel;
import io.realm.Realm;

/**
 * Created by surve on 16-Oct-17.
 */

public class FirebaseNotification extends FirebaseMessagingService {
    int id;
    Uri defaultSoundUri;
    String title="", description = "", empId= "", userName = "", role= "";
    Realm realm;
    final String TAG = "MARS NOTIFICATION";
    @Override
    public void onMessageReceived(RemoteMessage remoteMessage) {
        super.onMessageReceived(remoteMessage);
        Log.d(TAG,"Notification arrived");

        title = remoteMessage.getData().get("title");
        description = remoteMessage.getData().get("description");
//        empId = remoteMessage.getData().get("emp_id");
//        userName = remoteMessage.getData().get("user_name");
//        role = remoteMessage.getData().get("role");

        int requestID = (int) System.currentTimeMillis();
        id = requestID;

        Intent intent = new Intent(getApplicationContext(), Home.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP | Intent.FLAG_ACTIVITY_CLEAR_TOP);
        PendingIntent pendingIntent = PendingIntent.getActivity(getApplicationContext(), requestID,
                intent, PendingIntent.FLAG_UPDATE_CURRENT);
        defaultSoundUri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);

        Notification notification = new NotificationCompat.Builder(this)
                .setSmallIcon(R.drawable.marsphoto)
                .setContentTitle(title)
                .setContentText(description)
                .setSound(defaultSoundUri)
                .setContentIntent(pendingIntent)
                .setPriority(Notification.PRIORITY_MAX).build();
        notification.flags |= Notification.FLAG_ONLY_ALERT_ONCE | Notification.FLAG_AUTO_CANCEL;

        //Display Notification
        NotificationManager notificationManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
        notificationManager.notify(requestID,notification);

//        saveToRealm();

    }

    private void saveToRealm() {
        Realm.init(getApplicationContext());
        realm = Realm.getDefaultInstance();
        NotificationModel model = new NotificationModel();
        model.setId(id);
        model.setTitle(title);
        model.setDescription(description);
        model.setUserId(empId);
        model.setRole(role);
        model.setUserName(userName);
        realm.beginTransaction();
        realm.copyToRealmOrUpdate(model);
        realm.commitTransaction();
    }
}
