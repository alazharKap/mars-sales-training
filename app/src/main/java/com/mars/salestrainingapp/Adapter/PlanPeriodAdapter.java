package com.mars.salestrainingapp.Adapter;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;

import com.adevole.customresources.CustomTextView;
import com.mars.salestrainingapp.Activities.PlanPeriodDetail;
import com.mars.salestrainingapp.Model.SalesPersonModel;
import com.mars.salestrainingapp.R;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by surve on 07-Nov-17.
 */

public class PlanPeriodAdapter extends RecyclerView.Adapter<PlanPeriodAdapter.ViewHolder>{

    List<SalesPersonModel> list = new ArrayList<>();
 //   List<ScheduleModel> listSchedule=new ArrayList<>();
    Context context;
    boolean today;
    Activity activity;

    public PlanPeriodAdapter(Context context, Activity activity, List<SalesPersonModel> list) {
        this.context = context;
        this.activity = activity;
        this.list = list;

    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View rootView = LayoutInflater.from(context).inflate(R.layout.row_planperiod, parent, false);
        ViewHolder view = new ViewHolder(rootView);
        return view;
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, int position) {
        final SalesPersonModel model = list.get(position);

     //   final ScheduleModel modelSchedule =listSchedule.get(position);

        String numbers_to_display = model.getScheduledCount()+"/"+model.getNo_of_days();
        holder.dates.setText(numbers_to_display);
        if(model.getQuadrant_id()==0) {
            holder.name.setText(model.getUser_name());
            holder.name.setTextColor(ContextCompat.getColor(context, R.color.orange_plan));
        }
        else if (model.getQuadrant_id()==1)
        {
            holder.name.setText(model.getUser_name());
            holder.name.setTextColor(ContextCompat.getColor(context,R.color.green_plan));
        }
        else if (model.getQuadrant_id()==2)
        {
            holder.name.setText(model.getUser_name());
            holder.name.setTextColor(ContextCompat.getColor(context,R.color.blue_plan));
        }
        else if (model.getQuadrant_id()==3)
        {
            holder.name.setText(model.getUser_name());
            holder.name.setTextColor(ContextCompat.getColor(context,R.color.yellow_plan));
        }
        else if (model.getQuadrant_id()==4)
        {
            holder.name.setText(model.getUser_name());
            holder.name.setTextColor(ContextCompat.getColor(context,R.color.red_plan));
        }

        holder.row.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(context, PlanPeriodDetail.class);
                i.putExtra("empid", model.getEmpid());
                i.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                context.startActivity(i);
            }
        });
    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    protected class ViewHolder extends RecyclerView.ViewHolder {

        private LinearLayout row;
        private CustomTextView name, dates;

        public ViewHolder(View itemView) {
            super(itemView);

            row = (LinearLayout) itemView.findViewById(R.id.row);
            name = (CustomTextView) itemView.findViewById(R.id.name);
            dates = (CustomTextView) itemView.findViewById(R.id.dates);
        }
    }
}