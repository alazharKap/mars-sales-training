package com.mars.salestrainingapp.Adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.adevole.customresources.CustomTextView;
import com.mars.salestrainingapp.Model.TargetModel;
import com.mars.salestrainingapp.R;

import java.util.ArrayList;
import java.util.List;

import io.realm.Realm;

/**
 * Created by home on 05-01-2018.
 */

public class TargetAdapter extends RecyclerView.Adapter<TargetAdapter.ViewHolder> {

    private static final String TAG = "TargetAdapter";

    List<TargetModel> list = new ArrayList<>();
    Context context;

    public TargetAdapter(Context context, List<TargetModel> list) {
        this.context = context;
        this.list = list;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View rootView = LayoutInflater.from(context).inflate(R.layout.row_target, parent, false);
        ViewHolder view = new TargetAdapter.ViewHolder(rootView);
        return view;
    }

    @Override
    public void onBindViewHolder(TargetAdapter.ViewHolder holder, int position) {
        TargetModel model = list.get(position);
        holder.targetText.setText(model.getTarget());
        holder.parameterText.setText(model.getParameter());
    }

    @Override
    public int getItemCount() {
        return list.size();
    }


    protected class ViewHolder extends RecyclerView.ViewHolder {

        private CustomTextView parameterText, targetText;

        private ViewHolder(View itemView) {
            super(itemView);

            parameterText = (CustomTextView) itemView.findViewById(R.id.parameterText);
            targetText = (CustomTextView) itemView.findViewById(R.id.targetText);
        }
    }

}
