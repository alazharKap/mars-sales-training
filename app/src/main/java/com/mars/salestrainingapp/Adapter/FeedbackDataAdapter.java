package com.mars.salestrainingapp.Adapter;

import android.content.Context;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.adevole.customresources.CustomTextView;
import com.mars.salestrainingapp.Model.FeedbackModel;
import com.mars.salestrainingapp.Model.QuestionaryCategoryModel;
import com.mars.salestrainingapp.Model.QuestionaryModel;
import com.mars.salestrainingapp.R;

import java.util.ArrayList;
import java.util.List;

import io.realm.Realm;
import io.realm.RealmResults;

/**
 * Created by surve on 26-Dec-17.
 */

public class FeedbackDataAdapter extends RecyclerView.Adapter<FeedbackDataAdapter.ViewHolder> {

    Context context;
    List<QuestionaryCategoryModel> list = new ArrayList<>();
    int scheduleId;
    ArrayList<String> listDatesAapter = new ArrayList<String>();


    public FeedbackDataAdapter(Context context, List<QuestionaryCategoryModel> categoryModels, int scheduleId, ArrayList<String> listDatesAapter) {
        this.context = context;
        this.list = categoryModels;
        this.scheduleId = scheduleId;
        this.listDatesAapter = listDatesAapter;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View rootView = LayoutInflater.from(context).inflate(R.layout.row_feedback_cat, parent, false);
        ViewHolder viewHolder = new ViewHolder(rootView);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        QuestionaryCategoryModel model = list.get(position);

        Realm.init(context);
        Realm realm = Realm.getDefaultInstance();

        holder.categoryText.setText(model.getCategoryName());

        try {
            String dateCat1 = listDatesAapter.get(0);
            Log.i("Date1", dateCat1);
            String separatedDate1[] = dateCat1.split("-");
            holder.date1.setText(separatedDate1[2] + "-" + separatedDate1[1]);
        } catch (Exception e) {

        }
        try {
            String dateCat2 = listDatesAapter.get(1);
            String separatedDate2[] = dateCat2.split("-");
            holder.date2.setText(separatedDate2[2] + "-" + separatedDate2[1]);
        } catch (Exception e) {}

        try {
            String dateCat3 = listDatesAapter.get(2);
            String separatedDate3[] = dateCat3.split("-");
            holder.date3.setText(separatedDate3[2] + "-" + separatedDate3[1]);
        } catch (Exception e) {}


        RealmResults<QuestionaryModel> results = realm.where(QuestionaryModel.class)
                .equalTo("categoryId", model.getId())
                .findAll();
        Log.d("FeedbackData ", "results" + results.toString());

        holder.questionsRecycle.setAdapter(new FeedbackQuestionAdapter(context, results,listDatesAapter));
        //   holder.valueText.setText("");
    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    protected class ViewHolder extends RecyclerView.ViewHolder {

        CustomTextView categoryText, valueText;
        RecyclerView questionsRecycle;
        CustomTextView date1, date2, date3;

        public ViewHolder(View itemView) {
            super(itemView);

            categoryText = (CustomTextView) itemView.findViewById(R.id.question_title);
            questionsRecycle = (RecyclerView) itemView.findViewById(R.id.questions_recycle);
            questionsRecycle.setItemAnimator(new DefaultItemAnimator());
            questionsRecycle.setLayoutManager(new LinearLayoutManager(context));

            date1 = (CustomTextView) itemView.findViewById(R.id.date1);
            date2 = (CustomTextView) itemView.findViewById(R.id.date2);
            date3 = (CustomTextView) itemView.findViewById(R.id.date3);
//            questionText = (CustomTextView) itemView.findViewById(R.id.questionText);
            //     valueText = (CustomTextView) itemView.findViewById(R.id.valueText);
        }
    }

}
