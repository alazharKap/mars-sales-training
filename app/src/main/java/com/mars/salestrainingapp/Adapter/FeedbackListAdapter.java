package com.mars.salestrainingapp.Adapter;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.view.ViewGroup;

import com.mars.salestrainingapp.Model.FeedbackModel;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by surve on 15-Dec-17.
 */

public class FeedbackListAdapter extends RecyclerView.Adapter {

    List<FeedbackModel> list = new ArrayList<>();

    public FeedbackListAdapter(List<FeedbackModel> feedbackModelList) {
        list = feedbackModelList;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return null;
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {

    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    protected class ViewHolder extends RecyclerView.ViewHolder {

        public ViewHolder(View itemView) {
            super(itemView);
        }
    }
}
