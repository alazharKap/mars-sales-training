package com.mars.salestrainingapp.Adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RatingBar;

import com.adevole.customresources.CustomTextView;
import com.mars.salestrainingapp.Model.OutletModel;
import com.mars.salestrainingapp.Model.ScheduledOutlet;
import com.mars.salestrainingapp.R;

import java.util.List;

import io.realm.Realm;
import io.realm.RealmResults;

/**
 * Created by surve on 29-Nov-17.
 */

public class ScheduledOutletAdapter extends RecyclerView.Adapter<ScheduledOutletAdapter.ViewHolder>{

    Context context;
    List<ScheduledOutlet> list;

    public ScheduledOutletAdapter(Context context, RealmResults<ScheduledOutlet> list) {
        this.context = context;
        this.list = list;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View rootView = LayoutInflater.from(context).inflate(R.layout.row_scheduled_outlet, parent, false);
        ViewHolder view = new ViewHolder(rootView);
        return view;
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        ScheduledOutlet model = list.get(position);
        int outletId = model.getOutlet_id();
        Realm.init(context);
        Realm realm = Realm.getDefaultInstance();
        OutletModel outlet = realm.where(OutletModel.class).equalTo("id",outletId).findFirst();
        if (outlet != null) {
            holder.outletName.setText(outlet.getOutlet_name());
            holder.outletRating.setRating(model.getRating());
            if (model.getSpotAward() == 1)
                holder.spotAward.setVisibility(View.VISIBLE);
            else
                holder.spotAward.setVisibility(View.GONE);
        }
    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    protected class ViewHolder extends RecyclerView.ViewHolder {

        RatingBar outletRating;
        CustomTextView outletName;
        ImageView spotAward;

        public ViewHolder(View itemView) {
            super(itemView);

            outletRating = (RatingBar) itemView.findViewById(R.id.outlet_rating);
            outletName = (CustomTextView) itemView.findViewById(R.id.outlet_name);
            spotAward = (ImageView) itemView.findViewById(R.id.spot_award);
        }
    }
}
