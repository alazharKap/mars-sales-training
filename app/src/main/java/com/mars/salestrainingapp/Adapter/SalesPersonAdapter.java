package com.mars.salestrainingapp.Adapter;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;

import com.adevole.customresources.CustomButton;
import com.adevole.customresources.CustomTextView;
import com.mars.salestrainingapp.API.APIRequest;
import com.mars.salestrainingapp.Activities.Previous;
import com.mars.salestrainingapp.Helper.InternetConnection;
import com.mars.salestrainingapp.Helper.Preference;
import com.mars.salestrainingapp.Model.SalesPersonModel;
import com.mars.salestrainingapp.Model.ScheduleModel;
import com.mars.salestrainingapp.R;

import java.util.ArrayList;
import java.util.List;

import io.realm.Realm;

import static com.mars.salestrainingapp.Helper.Utility.convertToString;
import static com.mars.salestrainingapp.Helper.Utility.ddMMYYYY;
import static com.mars.salestrainingapp.Helper.Utility.internetError;
import static com.mars.salestrainingapp.Helper.Utility.log;
import static com.mars.salestrainingapp.Helper.Utility.toDDMMYY;
import static com.mars.salestrainingapp.Helper.Utility.toast;

/**
 * Created by surve on 28-Oct-17.
 */

public class SalesPersonAdapter extends RecyclerView.Adapter<SalesPersonAdapter.ViewHolder>{

    private static final String TAG = "SalesPersonAdapter";

    List<ScheduleModel> list = new ArrayList<>();
    Context context;
    View frame;
    boolean today;
    boolean apiCall = false;
    DeleteSchedule deleteScheduleListener;

    public SalesPersonAdapter(Context context, View frame, List<ScheduleModel> list, boolean today) {
        this.context = context;
        this.frame = frame;
        this.list = list;
        this.today = today;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View rootView = LayoutInflater.from(context).inflate(R.layout.row_today, parent, false);
        ViewHolder view = new ViewHolder(rootView);
        return view;
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        ScheduleModel model = list.get(position);

        Realm.init(context);
        Realm realm = Realm.getDefaultInstance();

        SalesPersonModel salesPerson;
        if (Preference.getInstance(context).getRoleId().equals("1") || Preference.getInstance(context).getRoleId().equals("2"))
            salesPerson = realm.where(SalesPersonModel.class).equalTo("empid",model.getBossId()).findFirst();
        else
            salesPerson = realm.where(SalesPersonModel.class).equalTo("empid",model.getSalesmanId()).findFirst();


        if (salesPerson!=null)
            holder.name.setText(salesPerson.getUser_name());

        if (today) {
            if (model.getStatus() == 1) {
                holder.start.setVisibility(View.VISIBLE);
                holder.start.setText(R.string.resume);
                holder.start.setBackgroundColor(0xFF8BC34A);
                holder.end.setVisibility(View.GONE);
                holder.delete.setVisibility(View.GONE);

            }
            else if (model.getStatus() == 2) {
                holder.start.setVisibility(View.GONE);
                holder.end.setVisibility(View.VISIBLE);
                holder.delete.setVisibility(View.GONE);
            }

            holder.date.setVisibility(View.GONE);
        }
        else {
            holder.start.setVisibility(View.GONE);
            holder.end.setVisibility(View.GONE);
            holder.date.setVisibility(View.VISIBLE);
            holder.date.setText(toDDMMYY(ddMMYYYY(convertToString(model.getScheduleDate()))));
//            holder.date.setText((convertToString(model.getScheduleDate())));
        }

        String role=  Preference.getInstance(context).getRoleId();
        if(role.equals("1") || role.equals("2")) {
            holder.delete.setVisibility(View.GONE);
            holder.start.setVisibility(View.GONE);
        }
    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    public void removeItem(int position) {
        Realm.init(context);
        Realm realm = Realm.getDefaultInstance();
        ScheduleModel model = list.get(position);
        realm.beginTransaction();
        model.deleteFromRealm();
        realm.commitTransaction();
        notifyItemRemoved(position);
    }

    public void restoreItem(ScheduleModel scheduleModel, int position) {
        Realm.init(context);
        Realm realm = Realm.getDefaultInstance();
        realm.beginTransaction();
        realm.copyToRealmOrUpdate(scheduleModel);
        realm.commitTransaction();
        notifyItemInserted(position);
    }

    public class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener{

        public CustomTextView name, date;
        public CustomButton start, end;
        public LinearLayout foreground;
        public RelativeLayout background;
        public ImageView delete;
        ProgressBar deleteProgress;

        public ViewHolder(View itemView) {
            super(itemView);

            foreground = (LinearLayout) itemView.findViewById(R.id.view_foreground);
            background = (RelativeLayout) itemView.findViewById(R.id.view_background);
            name = (CustomTextView) itemView.findViewById(R.id.name);
            date = (CustomTextView) itemView.findViewById(R.id.date);
            start = (CustomButton) itemView.findViewById(R.id.start);
            end = (CustomButton) itemView.findViewById(R.id.end);
            deleteProgress = (ProgressBar) itemView.findViewById(R.id.deleteProgress);
            delete = (ImageView) itemView.findViewById(R.id.delete_schdule);
            delete.setOnClickListener(this);
            start.setOnClickListener(this);
            end.setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {
            ScheduleModel scheduleModel = list.get(getAdapterPosition());
            if (InternetConnection.checkConnection(context)) {
                switch (v.getId()) {
                    case R.id.start:
                        if (!apiCall)
                            startDay(scheduleModel);
                        else
                            toast(frame, context, context.getString(R.string.hold_in_process));
                        break;
                    case R.id.end:
                        toast(frame, context, context.getString(R.string.day_already_ended));
                        break;
                    case R.id.delete_schdule:
                        if (InternetConnection.checkConnection(context)) {
                            deleteProgress.setVisibility(View.VISIBLE);
                            delete.setVisibility(View.GONE);
                            deleteScheduleListener.deleteSchedule(today, getAdapterPosition());
                        }
                        else
                            internetError(frame, context);
                        break;
                }
            }
            else
                internetError(frame, context);
        }
    }

    private void startDay(ScheduleModel scheduleModel) {

        final int empId = scheduleModel.getSalesmanId();

        Realm.init(context);
        Realm realm = Realm.getDefaultInstance();
        final SalesPersonModel salesPersonModel = realm.where(SalesPersonModel.class).equalTo("empid",empId).findFirst();

        final int bossId = salesPersonModel.getSuperior_emp_id();
        final int scheduleId = scheduleModel.getId();

        toast(frame, context, "Starting day for "+salesPersonModel.getUser_name());

        APIRequest apiRequest = new APIRequest();
        apiRequest.setChangeDayResponse(new APIRequest.ChangeDayStatusResponse() {
            @Override
            public void onStatusChanges(boolean status) {
                apiCall = false;
                if (status) {

                    log(TAG, "Selected EMP-Id " + empId);
                    log(TAG, "Selected BOSS-Id " + bossId);
                    log(TAG, "Selected SCHEDULES-Id " + scheduleId);

                    Intent i = new Intent(context, Previous.class);
                    i.putExtra("empId", empId);
                    i.putExtra("bossId", bossId);
                    i.putExtra("scheduleId", scheduleId);
                    i.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                    context.startActivity(i);
                }
            }
        });
        APIRequest.changeDayStatus(frame, context, scheduleId,1);
    }

    public void setOnDeleteListener(DeleteSchedule listener) {
        deleteScheduleListener = listener;
    }

    public interface DeleteSchedule {
        public void deleteSchedule(boolean today, int position);
    }
}