package com.mars.salestrainingapp.Adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.adevole.customresources.CustomTextView;
import com.mars.salestrainingapp.Model.OverallModel;
import com.mars.salestrainingapp.R;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by surve on 26-Dec-17.
 */

public class OverallAdapter extends RecyclerView.Adapter<OverallAdapter.ViewHolder>{

    Context context;
    List<OverallModel> list = new ArrayList<>();

    public OverallAdapter(Context context, List<OverallModel> list) {
        this.context = context;
        this.list = list;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View rootView = LayoutInflater.from(context).inflate(R.layout.feedback_row, parent, false);
        ViewHolder viewHolder = new ViewHolder(rootView);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
//        holder.questionText.setText(list.get(position).getCategoryName());
//        holder.valueText.setText(String.valueOf(list.get(position).getRating()));
    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    protected class ViewHolder extends RecyclerView.ViewHolder {
        CustomTextView questionText, valueText,valueText2,valueText3;
        public ViewHolder(View itemView) {
            super(itemView);

            questionText = (CustomTextView) itemView.findViewById(R.id.questionText);
            valueText = (CustomTextView) itemView.findViewById(R.id.valueText);
            valueText2 = (CustomTextView) itemView.findViewById(R.id.valueText1);
            valueText3 = (CustomTextView) itemView.findViewById(R.id.valueText);

        }
    }
}
