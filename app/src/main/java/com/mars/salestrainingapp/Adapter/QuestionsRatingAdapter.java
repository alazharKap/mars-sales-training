package com.mars.salestrainingapp.Adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RatingBar;

import com.adevole.customresources.CustomTextView;
import com.mars.salestrainingapp.Model.GetRatingQuestionsModel;
import com.mars.salestrainingapp.R;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

/**
 * Created by home on 03-03-2018.
 */


public class QuestionsRatingAdapter extends RecyclerView.Adapter<QuestionsRatingAdapter.ViewHolder> {
    Context context;
    List<GetRatingQuestionsModel> list = new ArrayList<>();
    ArrayList<Float> listRating = new ArrayList<>();
    public static  final HashMap<String,String> hashMap =new HashMap<>();;

    public class ViewHolder extends RecyclerView.ViewHolder {
        public CustomTextView queston;
        public RatingBar ratingBar;

        public ViewHolder(View view) {
            super(view);
            queston = (CustomTextView) view.findViewById(R.id.question);
            ratingBar = (RatingBar) view.findViewById(R.id.rating);
        }
        }



    public QuestionsRatingAdapter(Context context, List<GetRatingQuestionsModel> list) {
        this.context = context;
        this.list=list;
    }

    @Override
    public QuestionsRatingAdapter.ViewHolder onCreateViewHolder(ViewGroup parent,
                                                   int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.row_rating_questions, parent, false);
        return new ViewHolder(itemView);
    }


    @Override
    public void onBindViewHolder(final ViewHolder holder, final int position) {

        final GetRatingQuestionsModel model=list.get(position);

        holder.queston.setText(model.getQuestion());
        final int id=model.getId();
        String value =String.valueOf(holder.ratingBar.getRating());


      //  hashMap.put(model.getQuestion() , value);

        holder.ratingBar.setOnRatingBarChangeListener(new RatingBar.OnRatingBarChangeListener() {
            @Override
            public void onRatingChanged(RatingBar ratingBar, float v, boolean b) {
                hashMap.put(String.valueOf(model.getId()), String.valueOf(v));
            }
        });


//        holder.ratingBar.setOnTouchListener(new View.OnTouchListener() {
//            @Override
//            public boolean onTouch(View view, MotionEvent motionEvent) {
//                if (motionEvent.getAction() == MotionEvent.ACTION_UP) {
//                    // TODO perform your action here
//                    listRating.add(holder.ratingBar.getRating());
//
//
//                    if(context instanceof CreateOutlet){
//                        ((CreateOutlet)context).getRatingFromAdapter(listRating);
//                    }
//                }
//                return true;
//            }
//        });

//        holder.ratingBar.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                Intent intent = new Intent("custom-message");
//                //            intent.putExtra("quantity",Integer.parseInt(quantity.getText().toString()));
//                intent.putExtra("quantity",qty);
//                intent.putExtra("item",ItemName);
//                LocalBroadcastManager.getInstance(context).sendBroadcast(intent);
//            }
//        });



    }

    @Override
    public int getItemCount() {
        return list.size();
    }
}