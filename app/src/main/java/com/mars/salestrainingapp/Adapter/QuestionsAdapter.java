package com.mars.salestrainingapp.Adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.adevole.customresources.CustomTextView;
import com.mars.salestrainingapp.Activities.Questionary;
import com.mars.salestrainingapp.Model.QuestionaryModel;
import com.mars.salestrainingapp.R;
import com.trafi.ratingseekbar.RatingSeekBar;

import org.adw.library.widgets.discreteseekbar.DiscreteSeekBar;

import java.util.List;

/**
 * Created by surve on 05-Dec-17.
 */

class QuestionsAdapter extends RecyclerView.Adapter<QuestionsAdapter.ViewHolder>{

    Context context;
    List<QuestionaryModel> list;

    public QuestionsAdapter(Context context, List<QuestionaryModel> list) {
        this.context = context;
        this.list = list;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View rootView = LayoutInflater.from(context).inflate(R.layout.row_questions, parent, false);
        ViewHolder view = new ViewHolder(rootView);
        return view;
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        final QuestionaryModel model = list.get(position);
        holder.ratingSeekBar.setProgress(2);
        Questionary.feedback.put(model.getId(), 2);
        holder.questionText.setText(model.getQuestion());
//        holder.rating.setOnProgressChangeListener(new DiscreteSeekBar.OnProgressChangeListener() {
//            @Override
//            public void onProgressChanged(DiscreteSeekBar seekBar, int value, boolean fromUser) {
//                Questionary.feedback.put(model.getId(), value);
//            }
//
//            @Override
//            public void onStartTrackingTouch(DiscreteSeekBar seekBar) {
//
//            }
//
//            @Override
//            public void onStopTrackingTouch(DiscreteSeekBar seekBar) {
//
//            }
//        });
        holder.ratingSeekBar.setOnSeekBarChangeListener(new RatingSeekBar.OnRatingSeekBarChangeListener() {
            @Override
            public void onProgressChanged(RatingSeekBar ratingSeekBar, int i) {
                Questionary.feedback.put(model.getId(), i);
            }
        });
    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder{
        CustomTextView questionText;
        DiscreteSeekBar rating;
        RatingSeekBar ratingSeekBar;

        public ViewHolder(View itemView) {
            super(itemView);
            questionText = (CustomTextView) itemView.findViewById(R.id.question_text);
            //rating = (DiscreteSeekBar) itemView.findViewById(R.id.feedback_rating);
            ratingSeekBar = (RatingSeekBar) itemView.findViewById((R.id.feedback_seek_bar));
        }
    }
}