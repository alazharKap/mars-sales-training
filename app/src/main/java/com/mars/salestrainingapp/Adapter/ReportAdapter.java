package com.mars.salestrainingapp.Adapter;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;

import com.adevole.customresources.CustomTextView;
import com.mars.salestrainingapp.API.APIRequest;
import com.mars.salestrainingapp.Activities.Report;
import com.mars.salestrainingapp.Helper.InternetConnection;
import com.mars.salestrainingapp.Model.AllUsers;
import com.mars.salestrainingapp.Model.SalesPersonModel;
import com.mars.salestrainingapp.R;

import java.util.ArrayList;
import java.util.List;

import io.realm.RealmResults;

import static com.mars.salestrainingapp.API.APIRequest.sendMail;
import static com.mars.salestrainingapp.Helper.Utility.internetError;

/**
 * Created by surve on 26-Dec-17.
 */

public class ReportAdapter extends RecyclerView.Adapter<ReportAdapter.ViewHolder>{

    public static List<AllUsers> list = new ArrayList<>();
    Context context;
    LinearLayout frame;
    View view;
    public static int pos;

    public ReportAdapter(LinearLayout frame, Context context, List<AllUsers> list) {
        this.frame = frame;
        this.context = context;
        this.list = list;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View rootView = LayoutInflater.from(context).inflate(R.layout.row_reports, parent, false);
        ViewHolder view = new ViewHolder(rootView);
        return view;
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, int position) {
        final AllUsers model = list.get(position);
        holder.name.setText(model.getUserName());
    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    protected class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener{
        private CustomTextView name;
        private ImageView mail;
        public ViewHolder(View itemView) {
            super(itemView);
            name = (CustomTextView) itemView.findViewById(R.id.name);
            mail = (ImageView) itemView.findViewById(R.id.mail);
            mail.setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {
            switch (v.getId()) {
                case R.id.mail:
                     pos = getAdapterPosition();
//                        Report.alertDialog.show();
                    if (InternetConnection.checkConnection(context)) {
                        sendMail(frame, context, list.get(getAdapterPosition()).getId());
                    }
                    else
                        internetError(frame, context);
                    break;
            }
        }
    }
}