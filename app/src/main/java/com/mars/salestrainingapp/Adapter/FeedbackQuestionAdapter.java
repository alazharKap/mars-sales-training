package com.mars.salestrainingapp.Adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.adevole.customresources.CustomTextView;
import com.mars.salestrainingapp.Model.FeedbackModel;
import com.mars.salestrainingapp.Model.QuestionaryModel;
import com.mars.salestrainingapp.R;

import java.util.ArrayList;
import java.util.List;

import io.realm.Realm;

/**
 * Created by surve on 08-Jan-18.
 */

class FeedbackQuestionAdapter  extends RecyclerView.Adapter<FeedbackQuestionAdapter.ViewHolder>{

    Context context;
    List<QuestionaryModel> list;
    ArrayList<String> listDatesAapter = new ArrayList<String>();

    public FeedbackQuestionAdapter(Context context, List<QuestionaryModel> list, ArrayList<String> listDatesAapter) {
        this.context = context;
        this.list = list;
        this.listDatesAapter=listDatesAapter;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View rootView = LayoutInflater.from(context).inflate(R.layout.feedback_row, parent, false);
        ViewHolder view = new ViewHolder(rootView);
        return view;
    }
    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {

        Realm.init(context);
        Realm realm = Realm.getDefaultInstance();

        Log.i("FeedbackAdapterDates", String.valueOf(listDatesAapter));


        FeedbackModel data1 = null, data2 = null, data3 = null;

        final QuestionaryModel model = list.get(position);
        holder.questionText.setText(model.getQuestion());

        try {
            data1 = realm.where(FeedbackModel.class)
                    .equalTo("questionId", model.getId())
                    .and()
                    .equalTo("scheduleDate", listDatesAapter.get(0)).findFirst();


                data2 = realm.where(FeedbackModel.class)
                        .equalTo("questionId", model.getId())
                        .and()
                        .equalTo("scheduleDate", listDatesAapter.get(1)).findFirst();


                data3 = realm.where(FeedbackModel.class)
                        .equalTo("questionId", model.getId())
                        .and()
                        .equalTo("scheduleDate", listDatesAapter.get(2)).findFirst();
        }
        catch (Exception e) {}
        if(data1!=null)
        holder.valueText.setText(data1.getRating()+"");
        if(data2!=null)
        holder.valueText1.setText(data2.getRating()+"");
        if(data3!=null)
        holder.valueText2.setText(data3.getRating()+"");

    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder{
        CustomTextView questionText, valueText,valueText1,valueText2;

        public ViewHolder(View itemView) {
            super(itemView);
            questionText = (CustomTextView) itemView.findViewById(R.id.questionText);
            valueText = (CustomTextView) itemView.findViewById(R.id.valueText);
            valueText1 = (CustomTextView) itemView.findViewById(R.id.valueText1);
            valueText2 = (CustomTextView) itemView.findViewById(R.id.valueText2);
        }
    }
}