package com.mars.salestrainingapp.Adapter;

import android.content.Context;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.adevole.customresources.CustomTextView;
import com.mars.salestrainingapp.Model.QuestionaryCategoryModel;
import com.mars.salestrainingapp.Model.QuestionaryModel;
import com.mars.salestrainingapp.R;

import java.util.List;

import io.realm.Realm;
import io.realm.RealmResults;

/**
 * Created by surve on 05-Dec-17.
 */

public class QuestionaryAdapter extends RecyclerView.Adapter<QuestionaryAdapter.ViewHolder> {

    Context context;
    List<QuestionaryCategoryModel> list;

    public QuestionaryAdapter(Context context, List<QuestionaryCategoryModel> list) {
        this.context = context;
        this.list = list;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View rootView = LayoutInflater.from(context).inflate(R.layout.row_questionary, parent, false);
        ViewHolder view = new ViewHolder(rootView);
        return view;
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        QuestionaryCategoryModel category = list.get(position);
        holder.questionTitle.setText(category.getCategoryName());
        Realm.init(context);
        Realm realm = Realm.getDefaultInstance();
        RealmResults<QuestionaryModel> questionaryModels = realm.where(QuestionaryModel.class).equalTo("categoryId",category.getId()).and().equalTo("isDelete",0).findAll();
        holder.questionsRecycle.setAdapter(new QuestionsAdapter(context, questionaryModels));
    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder{

        CustomTextView questionTitle;
        RecyclerView questionsRecycle;

        public ViewHolder(View itemView) {
            super(itemView);

            questionTitle = (CustomTextView) itemView.findViewById(R.id.question_title);
            questionsRecycle = (RecyclerView) itemView.findViewById(R.id.questions_recycle);
            questionsRecycle.setItemAnimator(new DefaultItemAnimator());
            questionsRecycle.setLayoutManager(new LinearLayoutManager(context));
        }
    }
}
