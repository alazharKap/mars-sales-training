package com.mars.salestrainingapp.API;

import android.content.Context;
import android.util.Log;
import android.view.View;
import android.widget.Toast;

import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.mars.salestrainingapp.Helper.InternetConnection;
import com.mars.salestrainingapp.Helper.Preference;
import com.mars.salestrainingapp.Model.AllUsers;
import com.mars.salestrainingapp.Model.FeedbackModel;
import com.mars.salestrainingapp.Model.OutletModel;
import com.mars.salestrainingapp.Model.OverallModel;
import com.mars.salestrainingapp.Model.QuestionaryCategoryModel;
import com.mars.salestrainingapp.Model.QuestionaryModel;
import com.mars.salestrainingapp.Model.RegionModel;
import com.mars.salestrainingapp.Model.SalesPersonModel;
import com.mars.salestrainingapp.Model.ScheduleModel;
import com.mars.salestrainingapp.Model.ScheduledOutlet;
import com.mars.salestrainingapp.Model.TargetModel;
import com.mars.salestrainingapp.R;

import java.util.ArrayList;
import java.util.List;

import io.realm.Realm;
import io.realm.RealmResults;
import io.realm.Sort;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

import static com.mars.salestrainingapp.Helper.Preference.RESPONSE_DATA;
import static com.mars.salestrainingapp.Helper.Preference.STATUS;
import static com.mars.salestrainingapp.Helper.Preference.SUCCESS;
import static com.mars.salestrainingapp.Helper.Utility.YYYYMMdd;
import static com.mars.salestrainingapp.Helper.Utility.convertToDate;
import static com.mars.salestrainingapp.Helper.Utility.log;
import static com.mars.salestrainingapp.Helper.Utility.longToast;
import static com.mars.salestrainingapp.Helper.Utility.responseError;
import static com.mars.salestrainingapp.Helper.Utility.toast;

/**
 * Created by surve on 29-Nov-17.
 */

public class APIRequest {

    static UsersResponse usersResponse;
    static AllUsersResponse allUsersResponse;
    static ChangeDayStatusResponse changeDayStatusResponse;
    static OutletResponse outletResponse;
    static ScheduleOutletResponse scheduleOutletResponse;
    static QuestionaryResponse questionaryResponse;
    static FeedbackResponse feedbackResponse;
    static ChangePassword changePasswordResponse;
    static DeleteResponse scheduleDeleteListener;
    static PreviousData previousDataListener;
    static ValidatePlanPeriodResponse validatePlanPeriodResponse;
    static ExitResponse exitResponse;
    static OverAllResponse overAllResponseListener;

    static OverTargetAllResponse overTargetAllResponseListener;
    static SendMail sendMailResponse;
    private static LogoutResponse logoutResponse;

    public static void getPreviousData(View frame, Context context) {
        getPlanPeriod(context);
    }

    public static void getPlanPeriod(final Context context) {
        Retrofit retrofit = new Retrofit.Builder().baseUrl(APIList.BASE_URL).addConverterFactory(GsonConverterFactory.create()).build();
        API api = retrofit.create(API.class);
        Call<JsonObject> call = api.planPeriod(Preference.getInstance(context).getUserId());
        log("Plan Period", "Plan Period URL - " + call.request().url().toString());
        call.enqueue(new Callback<JsonObject>() {
            @Override
            public void onResponse(Call<JsonObject> call, Response<JsonObject> response) {
                if (response.isSuccessful()) {
                    JsonObject object = response.body();
                    Log.d("Plan Period details--->", object.toString());

                    if (object != null) {
                        String startDate = object.get("start_date").getAsString();
                        String endDate = object.get("end_date").getAsString();
                        String planPeriod = object.get("period").getAsString();


                        int spotAward = object.get("max_spot_award").getAsInt();

                        String[] x = startDate.split("-");
                        startDate = x[2] + "-" + x[1] + "-" + x[0];
                        x = endDate.split("-");
                        endDate = x[2] + "-" + x[1] + "-" + x[0];

                        Preference.getInstance(context).setStartDate(startDate);
                        Preference.getInstance(context).setEndDate(endDate);
                        Preference.getInstance(context).setPlanPeriod(planPeriod);
                        Preference.getInstance(context).setMaxSpot(spotAward);

                        if (object.has("regions")) {
                            Realm.init(context);
                            Realm mRealm = Realm.getDefaultInstance();
                            JsonArray regionArray = object.get("regions").getAsJsonArray();
                            for (int i = 0; i < regionArray.size(); i++) {
                                RegionModel regionModel = new RegionModel();
                                JsonObject regionObject = regionArray.get(i).getAsJsonObject();
                                regionModel.setRegion_id(regionObject.get("config_id").getAsInt());
                                regionModel.setConfig(regionObject.get("config").getAsString());
                                regionModel.setValue(regionObject.get("value").getAsString());

                                mRealm.beginTransaction();
                                mRealm.copyToRealmOrUpdate(regionModel);
                                mRealm.commitTransaction();
                            }
                            RealmResults<RegionModel> regionResults = mRealm.where(RegionModel.class).findAll();
                            Log.d("Received Region data", regionResults.toString());
                        }

//                        if(object.has("outlets")){
//                            JsonArray outletArray = object.get("outlets").getAsJsonArray();
//                            Realm.init(context);
//                            Realm mRealm = Realm.getDefaultInstance();
//                            OutletModel outletModel = new OutletModel();
//                            JsonObject outletObject;
//                            for(int i=0; i<outletArray.size(); i++){
//                                outletObject = outletArray.get(i).getAsJsonObject();
//                                outletModel.setUserId(outletObject.get("user_id").getAsInt());
//                                outletModel.setId(outletObject.get("outlet_id").getAsInt());
//                                outletModel.setEmpId(outletObject.get("empid").getAsString());
//                                outletModel.setRegion(outletObject.get("region").getAsString());
//                                outletModel.setOutlet_name(outletObject.get("outlet_name").getAsString());
//                                log("OUTLET NAME",outletObject.get("outlet_name").getAsString());
//                                outletModel.setAddress(outletObject.get("address").getAsString());
//                                outletModel.setRegionPrimaryId(outletObject.get("region_id").getAsInt());
//                                outletModel.setStoreCode(outletObject.get("store_code").getAsString());
//                                outletModel.setStoreName(outletObject.get("store_name").getAsString());
//                                outletModel.setCity(outletObject.get("city").getAsString());
//                                outletModel.setEmp_region(outletObject.get("emp_region").getAsString());
//
//                                mRealm.beginTransaction();
//                                mRealm.copyToRealmOrUpdate(outletModel);
//                                mRealm.commitTransaction();
//                            }
//                            RealmResults<OutletModel> outletResults = mRealm.where(OutletModel.class).findAll();
//                            Log.d("Received Outlet data",outletResults.toString());
//                        }
                        getUsers(context);
                    }
                }
            }

            @Override
            public void onFailure(Call<JsonObject> call, Throwable t) {

            }
        });
    }

    public static void getUsers(final Context context) {

        final String TAG = "Get User";

        if (InternetConnection.checkConnection(context)) {
            Retrofit retrofit = new Retrofit.Builder().baseUrl(APIList.BASE_URL).addConverterFactory(GsonConverterFactory.create()).build();
            API api = retrofit.create(API.class);
            Call<JsonObject> call;

            if (Preference.getInstance(context).getRoleId().equals("1") || Preference.getInstance(context).getRoleId().equals("2"))
                call = api.userListForDSR(Preference.getInstance(context).getUserId());
            else
                call = api.userList(Preference.getInstance(context).getUserId());

            log(TAG, "User List URL - " + call.request().url().toString());
            call.enqueue(new Callback<JsonObject>() {
                @Override
                public void onResponse(Call<JsonObject> call, Response<JsonObject> response) {
                    if (response.isSuccessful()) {
                        JsonObject object = response.body();
                        if (object != null) {
                            log(TAG, object.toString());
                            if (object.has("response_data")) {
                                Realm.init(context);
                                Realm mRealm = Realm.getDefaultInstance();

                                mRealm.beginTransaction();
                                mRealm.delete(SalesPersonModel.class);
                                mRealm.commitTransaction();
                                JsonArray array = object.get("response_data").getAsJsonArray();
                                for (int i = 0; i < array.size(); i++) {
                                    if (!array.get(i).isJsonNull()) {
                                        JsonObject innerObject = array.get(i).getAsJsonObject();
                                        SalesPersonModel model = new SalesPersonModel();
                                        if (innerObject.has("id"))
                                            model.setEmpid(innerObject.get("id").getAsInt());
                                        if (innerObject.has("user_name"))
                                            model.setUser_name(innerObject.get("user_name").getAsString());
                                        if (innerObject.has("no_of_days"))
                                            model.setNo_of_days(innerObject.get("no_of_days").getAsInt());
                                        if (innerObject.has("superior_emp_id"))
                                            model.setSuperior_emp_id(innerObject.get("superior_emp_id").getAsInt());
                                        if (innerObject.has("flag"))
                                            model.setIsDeleted(innerObject.get("flag").getAsInt());
                                        if (innerObject.has("quadrant_id"))
                                            model.setQuadrant_id(innerObject.get("quadrant_id").getAsInt());

                                        mRealm.beginTransaction();
                                        mRealm.copyToRealmOrUpdate(model);
                                        mRealm.commitTransaction();
                                    }
                                }
                                RealmResults<SalesPersonModel> results = mRealm.where(SalesPersonModel.class).findAll();
                                //log("Received Json data",results);
                                // getOutlets(context);
                            }
                        }
                    }
                    if (previousDataListener != null) {
                        previousDataListener.onDataReceived(true);
                        previousDataListener = null;
                    }

                    if (Preference.getInstance(context).getRoleId().equals("1") || Preference.getInstance(context).getRoleId().equals("2"))
                        getSchedule(context);
                }

                @Override
                public void onFailure(Call<JsonObject> call, Throwable t) {
                    t.printStackTrace();
                }
            });
        }
    }

    public static void getSchedule(final Context context) {

        String TAG = "Get Schedule";

        if (InternetConnection.checkConnection(context)) {

            Retrofit retrofit = new Retrofit.Builder().baseUrl(APIList.BASE_URL).addConverterFactory(GsonConverterFactory.create()).build();
            API api = retrofit.create(API.class);
            Call<JsonArray> call = api.getSchedule(Preference.getInstance(context).getUserId());
            log(TAG, "Schedule URL - " + call.request().url().toString());

            call.enqueue(new Callback<JsonArray>() {
                @Override
                public void onResponse(Call<JsonArray> call, Response<JsonArray> response) {
                    if (response.isSuccessful()) {
                        JsonArray array = response.body();

                        if (array.size() > 0) {
                            Realm.init(context);
                            Realm realm = Realm.getDefaultInstance();

                            for (int i = 0; i < array.size(); i++) {
                                ScheduleModel model = new ScheduleModel();
                                JsonObject object = array.get(i).getAsJsonObject();

                                model.setId(object.get("id").getAsInt());
                                model.setSalesmanId(object.get("salesmen_id").getAsInt());
                                model.setBossId(object.get("boss_id").getAsInt());
                                model.setScheduleDate(convertToDate(YYYYMMdd(object.get("schedule_date").getAsString())));
                                model.setPeriod(object.get("period").getAsInt());
                                model.setStatus(object.get("flag").getAsInt());
                                model.setQuestionFlag(object.get("question_flag").getAsInt());
                                model.setIsDeleted(object.get("is_delete").getAsInt());

                                realm.beginTransaction();
                                realm.copyToRealmOrUpdate(model);
                                realm.commitTransaction();
                            }
                        }
                        setScheduleCount(context);
                    }
                }

                @Override
                public void onFailure(Call<JsonArray> call, Throwable t) {
                    t.printStackTrace();
                }
            });
        }
    }

    public static void setScheduleCount(Context context) {
        Realm.init(context);
        Realm realm = Realm.getDefaultInstance();
        RealmResults<SalesPersonModel> salesPerson = realm.where(SalesPersonModel.class).notEqualTo("isDeleted", 1).findAll();

        for (SalesPersonModel model : salesPerson) {
            RealmResults<ScheduleModel> schedules = realm.where(ScheduleModel.class)
                    .equalTo("salesmanId", model.getEmpid())
                    .and()
                    .notEqualTo("isDeleted", 1)
                    .findAll();
            realm.beginTransaction();
            model.setScheduledCount(schedules.size());
            realm.copyToRealmOrUpdate(model);
            realm.commitTransaction();
        }
        if (usersResponse != null)
            usersResponse.onUserResponse();
        if (previousDataListener != null) {
            previousDataListener.onDataReceived(true);
            previousDataListener = null;
        }
    }

    public static void getAllUsers(final Context context) {

        final String TAG = "Get All User";

        if (InternetConnection.checkConnection(context)) {
            Retrofit retrofit = new Retrofit.Builder().baseUrl(APIList.BASE_URL).addConverterFactory(GsonConverterFactory.create()).build();
            API api = retrofit.create(API.class);

            log(TAG, "Id - " + Preference.getInstance(context).getUserId());
            Call<JsonObject> call = api.allUserList(Preference.getInstance(context).getUserId());
            log(TAG, "All User List URL - " + call.request().url().toString());
            call.enqueue(new Callback<JsonObject>() {
                @Override
                public void onResponse(Call<JsonObject> call, Response<JsonObject> response) {
                    if (response.isSuccessful()) {
                        JsonObject object = response.body();
                        if (object != null) {
                            log(TAG, object.toString());
                            if (object.has("response_data")) {
                                Realm.init(context);
                                Realm mRealm = Realm.getDefaultInstance();
                                JsonArray array = object.get("response_data").getAsJsonArray();

                                for (int i = 0; i < array.size(); i++) {
                                    if (!array.get(i).isJsonNull()) {
                                        JsonObject innerObject = array.get(i).getAsJsonObject();

                                        AllUsers model = new AllUsers();
                                        mRealm.beginTransaction();
                                        model.setEmpid(innerObject.get("empid").getAsString());
                                        model.setId(innerObject.get("id").getAsInt());
                                        model.setUserName(innerObject.get("user_name").getAsString());
                                        model.setIsDeleted(innerObject.get("flag").getAsInt());
                                        mRealm.copyToRealmOrUpdate(model);
                                        mRealm.commitTransaction();
                                    }
                                }
                                allUsersResponse.onAllUserResponse();
                            }
                        }
                    }
                }

                @Override
                public void onFailure(Call<JsonObject> call, Throwable t) {
                    t.printStackTrace();
                }
            });
        }
    }

    public static void changeDayStatus(final View frame, final Context context, final int scheduleId, final int status) {
        API api = APIClient.getAPI();
        Call<JsonObject> call = api.changeDayStatus(scheduleId, status);
        log("Change Day Status URL ", call.request().url().toString());
        call.enqueue(new Callback<JsonObject>() {
            @Override
            public void onResponse(Call<JsonObject> call, Response<JsonObject> response) {
                if (response.isSuccessful()) {
                    JsonObject object = response.body();

                    if (object != null) {
                        if (object.has(STATUS)) {
                            if (object.get(STATUS).getAsString().equals(SUCCESS)) {
                                Realm.init(context);
                                Realm realm = Realm.getDefaultInstance();
                                ScheduleModel model = realm.where(ScheduleModel.class).equalTo("id", scheduleId).findFirst();
                                realm.beginTransaction();
                                model.setStatus(status);
                                realm.copyToRealmOrUpdate(model);
                                realm.commitTransaction();
                                changeDayStatusResponse.onStatusChanges(true);
                            } else {
                                toast(frame, context, "Unable to change status");
                                changeDayStatusResponse.onStatusChanges(false);
                            }
                        } else {
                            responseError(frame, context);
                            changeDayStatusResponse.onStatusChanges(false);
                        }
                    } else {
                        responseError(frame, context);
                        changeDayStatusResponse.onStatusChanges(false);
                    }
                } else {
                    responseError(frame, context);
                    changeDayStatusResponse.onStatusChanges(false);
                }
            }

            @Override
            public void onFailure(Call<JsonObject> call, Throwable t) {
                responseError(frame, context);
                t.printStackTrace();
                changeDayStatusResponse.onStatusChanges(false);
            }
        });
    }

    public static void getOutlets(final Context context, int empId) {
        API api = APIClient.getAPI();
        Call<JsonObject> call = api.getOutlets(empId);
        call.enqueue(new Callback<JsonObject>() {
            @Override
            public void onResponse(Call<JsonObject> call, Response<JsonObject> response) {
                if (response.isSuccessful()) {
                    JsonObject object = response.body();

                    if (object != null) {
                        if (object.has(STATUS)) {
                            if (!object.get(STATUS).equals("empty")) {
                                if (object.has(RESPONSE_DATA)) {
                                    JsonArray responseArray = object.get(RESPONSE_DATA).getAsJsonArray();

                                    Realm.init(context);
                                    Realm realm = Realm.getDefaultInstance();
                                    clearOutlets(context);

                                    JsonObject outletObject;

                                    for (int i = 0; i < responseArray.size(); i++) {

                                        OutletModel outletModel = new OutletModel();

                                        outletObject = responseArray.get(i).getAsJsonObject();
                                        outletModel.setUserId(outletObject.get("user_id").getAsInt());
                                        outletModel.setId(outletObject.get("outlet_id").getAsInt());
                                        outletModel.setEmpId(outletObject.get("empid").getAsString());
                                        outletModel.setRegion(outletObject.get("region").getAsString());
                                        outletModel.setOutlet_name(outletObject.get("outlet_name").getAsString());
                                        outletModel.setAddress(outletObject.get("address").getAsString());
                                        outletModel.setRegionPrimaryId(outletObject.get("region_id").getAsInt());


                                        outletModel.setStoreCode(outletObject.get("store_code").getAsString());
                                        outletModel.setStoreName(outletObject.get("store_name").getAsString());
                                        outletModel.setCity(outletObject.get("city").getAsString());
                                        outletModel.setEmp_region(outletObject.get("emp_region").getAsString());

                                        realm.beginTransaction();
                                        realm.copyToRealmOrUpdate(outletModel);
                                        realm.commitTransaction();
                                    }
                                    if (previousDataListener != null) {
                                        previousDataListener.onDataReceived(true);
                                        previousDataListener = null;
                                    }
                                    if (outletResponse != null)
                                        outletResponse.onOutletReceived(true);
                                } else
                                    outletResponse.onOutletReceived(false);
                            } else
                                outletResponse.onOutletReceived(true);
                        } else
                            outletResponse.onOutletReceived(false);
                    } else
                        outletResponse.onOutletReceived(false);
                } else
                    outletResponse.onOutletReceived(false);
            }

            @Override
            public void onFailure(Call<JsonObject> call, Throwable t) {
                outletResponse.onOutletReceived(false);
                t.printStackTrace();
            }
        });

    }

    private static void clearOutlets(Context context) {
        Realm.init(context);
        Realm realm = Realm.getDefaultInstance();
        RealmResults<OutletModel> realmResults = realm.where(OutletModel.class).findAll();
        realm.beginTransaction();
        realmResults.deleteAllFromRealm();
        realm.commitTransaction();
    }

    public static void scheduleOutlet(final View frame, final Context context, final int outlet_id, final int scheduleId, float outletRating, String notes, int spotAward, String spotReason, String questionsArray) {

        API api = APIClient.getAPI();
        Call<JsonObject> call = api.addOutlet(outlet_id, scheduleId, outletRating, notes, spotAward, spotReason, questionsArray.toString());
        log("Creating Outlet Schedule ", call.request().url().toString());
        toast(frame, context, context.getString(R.string.adding_outlet));
        call.enqueue(new Callback<JsonObject>() {
            @Override
            public void onResponse(Call<JsonObject> call, Response<JsonObject> response) {
                if (response.isSuccessful()) {
                    JsonObject object = response.body();
                    if (object != null) {
                        if (object.has(STATUS)) {
                            if (object.has(RESPONSE_DATA)) {
                                JsonArray array = object.get(RESPONSE_DATA).getAsJsonArray();

                                if (array.size() > 0) {
                                    JsonObject innerObject = array.get(0).getAsJsonObject();

                                    if (innerObject != null) {
                                        Realm realm = Realm.getDefaultInstance();
                                        realm.beginTransaction();
                                        ScheduledOutlet outSch = new ScheduledOutlet();
                                        outSch.setId(innerObject.get("id").getAsInt());
                                        outSch.setOutlet_id(innerObject.get("outlet_id").getAsInt());
                                        outSch.setRating(innerObject.get("rating").getAsFloat());
                                        outSch.setNotes(innerObject.get("notes").getAsString());
                                        outSch.setSpotAward(innerObject.get("spot_award").getAsInt());
                                        outSch.setSpotReason(innerObject.get("spot_reason").getAsString());
                                        outSch.setSchedule_id(scheduleId);
                                        realm.copyToRealmOrUpdate(outSch);
                                        realm.commitTransaction();
                                        Preference.getInstance(context).calculateSpotAward(context);

                                        scheduleOutletResponse.onOutletScheduled();
                                    }
                                }
                            } else
                                toast(frame, context, "Error while adding outlet, Try again later");

                        } else
                            responseError(frame, context);
                    } else
                        responseError(frame, context);
                } else
                    responseError(frame, context);
            }

            @Override
            public void onFailure(Call<JsonObject> call, Throwable t) {
                responseError(frame, context);
                t.printStackTrace();
            }
        });
    }

    public static void getAllScheduleOutlet(final View frame, final Context context) {
        API api = APIClient.getAPI();
        Call<JsonObject> call = api.getAllScheduledOutlet(Preference.getInstance(context).getUserId());
        call.enqueue(new Callback<JsonObject>() {
            @Override
            public void onResponse(Call<JsonObject> call, Response<JsonObject> response) {
                if (response.isSuccessful()) {
                    JsonObject object = response.body();
                } else
                    responseError(frame, context);
            }

            @Override
            public void onFailure(Call<JsonObject> call, Throwable t) {
                responseError(frame, context);
            }
        });

    }

    public static void getQuestionaryCategory(final View frame, final Context context) {
        API api = APIClient.getAPI();
        Call<JsonObject> call = api.getQuestionaryCat();
        call.enqueue(new Callback<JsonObject>() {
            @Override
            public void onResponse(Call<JsonObject> call, Response<JsonObject> response) {
                if (response.isSuccessful()) {
                    JsonObject object = response.body();
                    if (object.has(STATUS)) {
                        if (object.has(RESPONSE_DATA)) {
                            JsonArray array = object.get(RESPONSE_DATA).getAsJsonArray();
                            Realm.init(context);
                            Realm realm = Realm.getDefaultInstance();

                            for (int i = 0; i < array.size(); i++) {
                                JsonObject innerObject = array.get(i).getAsJsonObject();
                                QuestionaryCategoryModel model = new QuestionaryCategoryModel();
                                model.setId(innerObject.get("id").getAsInt());
                                model.setCategoryName(innerObject.get("category_name").getAsString());
                                model.setIsDeleted(innerObject.get("is_delete").getAsInt());

                                realm.beginTransaction();
                                realm.copyToRealmOrUpdate(model);
                                realm.commitTransaction();
                            }

                            if (array.size() > 0)
                                getQuestionary(frame, context);
                        }
                    }
                }
            }

            @Override
            public void onFailure(Call<JsonObject> call, Throwable t) {
                t.printStackTrace();
                responseError(frame, context);
            }
        });
    }

    public static void getQuestionary(final View frame, final Context context) {
        API api = APIClient.getAPI();
        Call<JsonObject> call = api.getQuestionary();
        call.enqueue(new Callback<JsonObject>() {
            @Override
            public void onResponse(Call<JsonObject> call, Response<JsonObject> response) {
                if (response.isSuccessful()) {
                    JsonObject object = response.body();
                    if (object.has(STATUS)) {
                        if (object.has(RESPONSE_DATA)) {
                            JsonArray array = object.get(RESPONSE_DATA).getAsJsonArray();
                            Realm.init(context);
                            Realm realm = Realm.getDefaultInstance();

                            for (int i = 0; i < array.size(); i++) {
                                JsonObject innerObject = array.get(i).getAsJsonObject();
                                QuestionaryModel model = new QuestionaryModel();
                                model.setId(innerObject.get("id").getAsInt());
                                model.setCategoryId(innerObject.get("category_id").getAsInt());
                                model.setQuestion(innerObject.get("question").getAsString());
                                model.setIsDelete(innerObject.get("flag").getAsInt());

                                realm.beginTransaction();
                                realm.copyToRealmOrUpdate(model);
                                realm.commitTransaction();
                            }

                            if (questionaryResponse != null) {
                                questionaryResponse.onQuestionary();
                                questionaryResponse = null;
                            }

                        }
                    }
                } else
                    responseError(frame, context);
            }

            @Override
            public void onFailure(Call<JsonObject> call, Throwable t) {
                responseError(frame, context);
            }
        });

    }

    public static void submitFeedback(final View frame, final Context context, final int scheduleId, String feedbackArray, String strength, String development) {
        API api = APIClient.getAPI();
        Call<JsonObject> call = api.submitFeedback(scheduleId, feedbackArray, strength, development);
        log("Submit Feedback", call.request().url().toString());
        call.enqueue(new Callback<JsonObject>() {
            @Override
            public void onResponse(Call<JsonObject> call, Response<JsonObject> response) {
                if (response.isSuccessful()) {
                    JsonObject object = response.body();
                    if (object != null)
                        if (object.has(STATUS))
                            if (object.get(STATUS).getAsString().equals(SUCCESS)) {
                                Realm.init(context);
                                Realm realm = Realm.getDefaultInstance();
                                ScheduleModel model = realm.where(ScheduleModel.class).equalTo("id", scheduleId).findFirst();
                                realm.beginTransaction();
                                model.setQuestionFlag(1);
                                realm.commitTransaction();
                                feedbackResponse.onFeedbackSubmitted(true);
                            } else {
                                toast(frame, context, object.get(STATUS).getAsString());
                                feedbackResponse.onFeedbackSubmitted(false);
                            }
                        else {
                            responseError(frame, context);
                            feedbackResponse.onFeedbackSubmitted(false);
                        }
                    else {
                        responseError(frame, context);
                        feedbackResponse.onFeedbackSubmitted(false);
                    }
                } else {
                    responseError(frame, context);
                    feedbackResponse.onFeedbackSubmitted(false);
                }
            }

            @Override
            public void onFailure(Call<JsonObject> call, Throwable t) {
                responseError(frame, context);
                feedbackResponse.onFeedbackSubmitted(false);
                t.printStackTrace();
            }
        });
    }

    public static void changePassword(final View frame, final Context context, String currentPass, String newPass) {
        API api = APIClient.getAPI();
        Call<JsonObject> call = api.changePassword(Preference.getInstance(context).getUserId(), currentPass, newPass);
        log("Change Password ", call.request().url().toString());

        call.enqueue(new Callback<JsonObject>() {
            @Override
            public void onResponse(Call<JsonObject> call, Response<JsonObject> response) {
                if (response.isSuccessful()) {
                    JsonObject object = response.body();
                    if (object != null) {
                        if (object.has(STATUS)) {
                            if (object.get(STATUS).getAsString().equals(SUCCESS))
                                changePasswordResponse.onPasswordChanged(true);
                            else
                                changePasswordResponse.onPasswordChanged(false);
                        } else
                            responseError(frame, context);
                    } else
                        responseError(frame, context);
                } else
                    responseError(frame, context);
            }

            @Override
            public void onFailure(Call<JsonObject> call, Throwable t) {
                responseError(frame, context);
                t.printStackTrace();
            }
        });
    }

    public static void deleteSchedule(final View frame, final Context context, final int id) {
        API api = APIClient.getAPI();
        Call<JsonObject> call = api.deleteSchedule(id, 1);
        log("Deleteing Schedule", call.request().url().toString());
        call.enqueue(new Callback<JsonObject>() {
            @Override
            public void onResponse(Call<JsonObject> call, Response<JsonObject> response) {
                if (response.isSuccessful()) {
                    JsonObject object = response.body();
                    if (object != null) {
                        if (object.has(STATUS)) {
                            if (object.get(STATUS).getAsString().equals("0")) {
                                Realm.init(context);
                                Realm realm = Realm.getDefaultInstance();
                                ScheduleModel model = realm.where(ScheduleModel.class).equalTo("id", id).findFirst();
                                realm.beginTransaction();
                                model.setIsDeleted(1);
                                realm.copyToRealmOrUpdate(model);
                                realm.commitTransaction();
                                scheduleDeleteListener.onSchdeuleDeleted(true);
                            } else
                                scheduleDeleteListener.onSchdeuleDeleted(false);
                        } else {
                            responseError(frame, context);
                            scheduleDeleteListener.onSchdeuleDeleted(false);
                        }
                    } else {
                        responseError(frame, context);
                        scheduleDeleteListener.onSchdeuleDeleted(false);
                    }
                } else {
                    responseError(frame, context);
                    scheduleDeleteListener.onSchdeuleDeleted(false);
                }
            }

            @Override
            public void onFailure(Call<JsonObject> call, Throwable t) {
                t.printStackTrace();
                responseError(frame, context);
                scheduleDeleteListener.onSchdeuleDeleted(false);
            }
        });

    }

    public static void validatePeriod(View frame, final Context context) {
        final String oldPeriod = Preference.getInstance(context).getPlanPeriod();
        longToast(frame, context, "Validating Plan Period");
        Retrofit retrofit = new Retrofit.Builder().baseUrl(APIList.BASE_URL).addConverterFactory(GsonConverterFactory.create()).build();
        API api = retrofit.create(API.class);
        Call<JsonObject> call = api.planPeriod(Preference.getInstance(context).getUserId());
        log("Plan Period", "Plan Period URL - " + call.request().url().toString());
        call.enqueue(new Callback<JsonObject>() {
            @Override
            public void onResponse(Call<JsonObject> call, Response<JsonObject> response) {
                if (response.isSuccessful()) {
                    JsonObject object = response.body();
                    if (object != null) {
                        String startDate = object.get("start_date").getAsString();
                        String endDate = object.get("end_date").getAsString();
                        String planPeriod   = object.get("period").getAsString();
                        int spotAward = object.get("max_spot_award").getAsInt();

                        String[] x = startDate.split("-");
                        startDate = x[2] + "-" + x[1] + "-" + x[0];
                        x = endDate.split("-");
                        endDate = x[2] + "-" + x[1] + "-" + x[0];

                        Preference.getInstance(context).setStartDate(startDate);
                        Preference.getInstance(context).setEndDate(endDate);
                        Preference.getInstance(context).setPlanPeriod(planPeriod);
                        Preference.getInstance(context).setMaxSpot(spotAward);

                        if (planPeriod.equals(oldPeriod))
                            validatePlanPeriodResponse.changeInPlanPeriod(false);
                        else
                            validatePlanPeriodResponse.changeInPlanPeriod(true);
                    }
                }
            }

            @Override
            public void onFailure(Call<JsonObject> call, Throwable t) {

            }
        });
    }

    public static void exitUser(final View frame, final Context context, int empId, String date, String note, int userId) {
        API api = APIClient.getAPI();
        Call<JsonObject> call = api.userExit(empId, date, note, userId);
        call.enqueue(new Callback<JsonObject>() {
            @Override
            public void onResponse(Call<JsonObject> call, Response<JsonObject> response) {
                if (response.isSuccessful()) {
                    JsonObject object = response.body();
                    if (object != null) {
                        if (object.has(STATUS)) {
                            if (object.get(STATUS).getAsString().equals(SUCCESS)) {
                                exitResponse.exitResponse(true);
                            } else {
                                toast(frame, context, object.get(STATUS).getAsString());
                                exitResponse.exitResponse(false);
                            }
                        } else
                            error();
                    } else
                        error();
                } else
                    error();
            }

            @Override
            public void onFailure(Call<JsonObject> call, Throwable t) {
                t.printStackTrace();
                error();
            }

            private void error() {
                responseError(frame, context);
                exitResponse.exitResponse(false);
            }
        });
    }

    public static void getRecentFeedback(final View frame, final Context context, int empId) {
        final String TAG = "GetRecentFeedback";
        API api = APIClient.getAPI();
        Call<JsonObject> call = api.getRecentFeedback(empId);
        log(TAG, "URL - " + call.request().url().toString());
        call.enqueue(new Callback<JsonObject>() {
            @Override
            public void onResponse(Call<JsonObject> call, Response<JsonObject> response) {
                if (response.isSuccessful()) {
                    JsonObject array = response.body();
                    log(TAG, "Response - " + array.toString());

                    Realm.init(context);
                    Realm realm = Realm.getDefaultInstance();
                    realm.beginTransaction();
                    realm.delete(FeedbackModel.class);
                    realm.commitTransaction();

                    if (array != null) {
                        if (array.size() > 0) {


                            List<FeedbackModel> list = new ArrayList<>();

//                            for (JsonElement jsonElement : array) {

//                                JsonObject object = array.get("").getAsJsonObject();
                            JsonArray dataArray = array.get("data").getAsJsonArray();

                            for (JsonElement dataElement : dataArray) {
                                JsonObject dataObject = dataElement.getAsJsonObject();

                                FeedbackModel model = new FeedbackModel();
                                model.setId(dataObject.get("id").getAsInt());
                                model.setBossName(dataObject.get("boss_name").getAsString());
                                model.setQuestionId(dataObject.get("question_id").getAsInt());
                                model.setCategoryId(dataObject.get("category_id").getAsInt());
                                model.setRating(dataObject.get("rating").getAsInt());
                                model.setQuestion(dataObject.get("question").getAsString());
                                model.setScheduleId(dataObject.get("schedule_id").getAsInt());
                                model.setScheduleDate(dataObject.get("schedule_date").getAsString());

                                realm.beginTransaction();
                                realm.copyToRealmOrUpdate(model);
                                list.add(model);
                                realm.commitTransaction();
                            }
//                            }

                            RealmResults<FeedbackModel> results = realm.where(FeedbackModel.class).findAll();
                            results = results.sort("scheduleId", Sort.DESCENDING);

                            feedbackResponse.onFeedbackReceived(results);
                        } else
                            toast(frame, context, "No record found");
                    } else
                        responseError(frame, context);
                } else
                    responseError(frame, context);
            }

            @Override
            public void onFailure(Call<JsonObject> call, Throwable t) {
                responseError(frame, context);
                t.printStackTrace();
            }
        });
    }

    public static void getOverallData(final View frame, final Context context, int empId) {
        API api = APIClient.getAPI();
        Call<JsonObject> call = api.getOverAllData(empId);
        log("GETOVERALL DATA URL", call.request().url().toString());
        call.enqueue(new Callback<JsonObject>() {
            @Override
            public void onResponse(Call<JsonObject> call, Response<JsonObject> response) {
                if (response.isSuccessful()) {
                    JsonObject object = response.body();
                    if (object != null) {

                        if (object.has("response_data")) {
                            JsonArray array = object.get("response_data").getAsJsonArray();
                            if (array.size() > 0) {
                                List<OverallModel> list = new ArrayList<>();

                                for (int i = 0; i < array.size(); i++) {
                                    JsonObject innerObject = array.get(i).getAsJsonObject();
                                    OverallModel model = new OverallModel();
                                    model.setId(innerObject.get("id").getAsInt());
                                    model.setEmpId(innerObject.get("emp_id").getAsInt());
                                    model.setEmpCode(innerObject.get("emp_code").getAsString());
                                    model.setName(innerObject.get("name").getAsString());
                                    model.setGsv(innerObject.get("gsv").getAsString());
                                    model.setProductivity(innerObject.get("productivity").getAsString());
                                    model.setVisit_overage(innerObject.get("visit_overage").getAsString());
                                    model.setFoucs_course(innerObject.get("focus_course").getAsString());
                                    model.setIncentive(innerObject.get("incentive").getAsInt());
                                    model.setAcc_addition(innerObject.get("acc_addition").getAsInt());
                                    model.setLast_qtr_ratings(innerObject.get("last_qtr_ratings").getAsString());
                                    model.setPeriod_id(innerObject.get("period_id").getAsInt());
                                    model.setYear(innerObject.get("year").getAsString());
                                    model.setUpdated_at(innerObject.get("updated_at").getAsString());

                                    list.add(model);
                                }
                                if (!list.isEmpty())
                                    overAllResponseListener.onOverAllReceived(list);
                            } else
                                toast(frame, context, context.getString(R.string.no_record_found));
                        }
                    } else {
                        toast(frame, context, context.getString(R.string.no_record_found));
                    }

//                    else
//                        responseError(frame, context);
                }
//                else
//                    responseError(frame, context);
            }

            @Override
            public void onFailure(Call<JsonObject> call, Throwable t) {
//                responseError(frame, context);
                t.printStackTrace();
            }
        });
    }

    public static void sendMail(final View frame, final Context context, int empId) {
        API api = APIClient.getAPI();
        toast(frame, context, "Generating Mail");
        Call<JsonObject> call = api.sendMail(Preference.getInstance(context).getUserId(), empId);
        call.enqueue(new Callback<JsonObject>() {
            @Override
            public void onResponse(Call<JsonObject> call, Response<JsonObject> response) {
                if (response.isSuccessful()) {
                    JsonObject object = response.body();

                    toast(frame, context, object.get(STATUS).getAsString());
                }
            }

            @Override
            public void onFailure(Call<JsonObject> call, Throwable t) {
                t.printStackTrace();
//                sendMailResponse.onMailSend(false);
            }
        });
    }

    public static void getAllTargetUsers(final View frame, final Context context) {

        final String TAG = "Get All Target User";

        if (InternetConnection.checkConnection(context)) {
            Retrofit retrofit = new Retrofit.Builder().baseUrl(APIList.BASE_URL).addConverterFactory(GsonConverterFactory.create()).build();
            API api = retrofit.create(API.class);
            Call<JsonObject> call = api.allTargetUserList(Preference.getInstance(context).getUserId());
            log(TAG, "All  Target User List URL - " + call.request().url().toString());
            call.enqueue(new Callback<JsonObject>() {
                @Override
                public void onResponse(Call<JsonObject> call, Response<JsonObject> response) {
                    if (response.isSuccessful()) {
                        JsonObject object = response.body();
                        if (object != null) {

                            List<TargetModel> targetList = new ArrayList<>();
                            log(TAG, object.toString());
                            if (object.has("response_data")) {
                                Realm.init(context);
                                Realm mRealm = Realm.getDefaultInstance();
                                JsonArray array = object.get("response_data").getAsJsonArray();
                                for (int i = 0; i < array.size(); i++) {
                                    JsonObject innerObject = array.get(i).getAsJsonObject();
                                    TargetModel model = new TargetModel();

                                    model.setId(innerObject.get("id").getAsInt());
                                    model.setEmp_id(innerObject.get("emp_id").getAsInt());

                                    model.setEmp_code(innerObject.get("emp_code").getAsString());
                                    model.setTarget(innerObject.get("target").getAsString());

                                    model.setPeriod(innerObject.get("period").getAsString());
                                    model.setPeriod_id(innerObject.get("period_id").getAsInt());

                                    model.setYear(innerObject.get("year").getAsString());
                                    model.setParameter(innerObject.get("parameter").getAsString());


                                    mRealm.beginTransaction();
                                    mRealm.copyToRealmOrUpdate(model);
                                    mRealm.commitTransaction();
                                    targetList.add(model);
                                }
                                if (!targetList.isEmpty())
                                    overTargetAllResponseListener.onTargetAllReceived(targetList);
                            } else
                                toast(frame, context, context.getString(R.string.no_record_found));
                        }

                    }
                }

                @Override
                public void onFailure(Call<JsonObject> call, Throwable t) {
                    t.printStackTrace();
                }
            });
        }
    }

    public static void logoutUser(final Context context) {
        final String TAG = "Logout";
        if (InternetConnection.checkConnection(context)) {
            Retrofit retrofit = new Retrofit.Builder().baseUrl(APIList.BASE_URL).addConverterFactory(GsonConverterFactory.create()).build();
            API api = retrofit.create(API.class);
            Call<JsonObject> call = api.logout(Preference.getInstance(context).getUserId());
            log(TAG, "Logout URL - " + call.request().url().toString());
            call.enqueue(new Callback<JsonObject>() {
                @Override
                public void onResponse(Call<JsonObject> call, Response<JsonObject> response) {
                    if (response.body().get(Preference.STATUS).getAsString().equalsIgnoreCase(Preference.SUCCESS)) {
                        Toast.makeText(context, "Logged out!", Toast.LENGTH_SHORT).show();
                        logoutResponse.onLogout();
                    } else {
                        Toast.makeText(context, "Error!!", Toast.LENGTH_SHORT).show();
                    }
                }

                @Override
                public void onFailure(Call<JsonObject> call, Throwable t) {
                    t.printStackTrace();
                }
            });

        }
    }

    public interface SendMail {
        public void onMailSend(boolean status);
    }

    public interface UsersResponse {
        public void onUserResponse();
    }

    public interface AllUsersResponse {
        public void onAllUserResponse();
    }

    public interface ChangeDayStatusResponse {
        public void onStatusChanges(boolean status);
    }

    public interface OutletResponse {
        public void onOutletReceived(boolean status);
    }

    public interface ScheduleOutletResponse {
        public void onOutletScheduled();
    }

    public interface QuestionaryResponse {
        public void onQuestionary();
    }

    public interface FeedbackResponse {
        public void onFeedbackSubmitted(boolean status);

        public void onFeedbackReceived(List<FeedbackModel> list);
    }

    public interface ChangePassword {
        public void onPasswordChanged(boolean status);
    }

    public interface DeleteResponse {
        public void onSchdeuleDeleted(boolean status);
    }

    public interface PreviousData {
        public void onDataReceived(boolean status);
    }

    public interface ValidatePlanPeriodResponse {
        public void changeInPlanPeriod(boolean status);
    }

    public interface ExitResponse {
        public void exitResponse(boolean status);
    }

    public interface OverAllResponse {
        public void onOverAllReceived(List<OverallModel> list);
    }

    public interface LogoutResponse {
        public void onLogout();
    }

    public void setLogoutResponse(LogoutResponse listener) {
        logoutResponse = listener;
    }

    public void setSendMailResponseListener(SendMail listener) {
        sendMailResponse = listener;
    }

    public void setOverAllResponseListener(OverAllResponse listener) {
        overAllResponseListener = listener;
    }

    public interface OverTargetAllResponse {
        public void onTargetAllReceived(List<TargetModel> list);
    }

    public void setOverTargetAllResponseListener(OverTargetAllResponse listener) {
        overTargetAllResponseListener = listener;
    }

    public void setExitResponse(ExitResponse listener) {
        this.exitResponse = listener;
    }

    public void setValidatePlanPeriod(ValidatePlanPeriodResponse listener) {
        validatePlanPeriodResponse = listener;
    }

    public void setOnSchduleDeleteListener(DeleteResponse listener) {
        scheduleDeleteListener = listener;
    }

    public void setOnFeedbackListener(FeedbackResponse listener) {
        feedbackResponse = listener;
    }

    public void setQuestionaryResponse(QuestionaryResponse listener) {
        this.questionaryResponse = listener;
    }

    public void setChangeDayResponse(ChangeDayStatusResponse changeDayStatusResponse) {
        this.changeDayStatusResponse = changeDayStatusResponse;
    }

    public void setUsersResponse(UsersResponse listener) {
        usersResponse = listener;
    }

    public void setAllUsersResponse(AllUsersResponse listener) {
        allUsersResponse = listener;
    }

    public void setOutletResponse(OutletResponse outletResponse) {
        this.outletResponse = outletResponse;
    }

    public void setSchduleOutletResponse(ScheduleOutletResponse listener) {
        scheduleOutletResponse = listener;
    }

    public void setChangePasswordListener(ChangePassword listener) {
        changePasswordResponse = listener;
    }

    public void setPreviousResponse(PreviousData listener) {
        previousDataListener = listener;
    }
}