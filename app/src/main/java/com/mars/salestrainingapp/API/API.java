package com.mars.salestrainingapp.API;

import com.google.gson.JsonArray;
import com.google.gson.JsonObject;

import retrofit2.Call;
import retrofit2.http.POST;
import retrofit2.http.Query;

/**
 * Created by surve on 16-Oct-17.
 */

public interface API {

    @POST(APIList.LOGIN)
    Call<JsonArray> login(@Query("email") String username,
                          @Query("password") String password,
                          @Query("fcmid") String fcmid,
                          @Query("device_id") String deviceId);

    @POST(APIList.FORGOT)
    Call<JsonArray> forget(@Query("username") String username);

    @POST(APIList.RESET)
    Call<JsonObject> reset(@Query("appkey") String appkey,@Query("password") String password,@Query("username") String username);

    @POST(APIList.USER_LIST)
    Call<JsonObject> userList(@Query("emp_id") int user_id );

    @POST(APIList.USER_LIST_FOR_DSR)
    Call<JsonObject> userListForDSR(@Query("emp_id") int user_id );

    @POST(APIList.ALL_USER_LIST)
    Call<JsonObject> allUserList(@Query("emp_id") int user_id );

    @POST(APIList.ALL_TARGET_USER_LIST)
    Call<JsonObject> allTargetUserList(@Query("emp_id") int user_id );

    @POST(APIList.PLAN_PERIOD)
    Call<JsonObject> planPeriod(@Query("id") int id);

    @POST(APIList.GET_SCHEDULE)
    Call<JsonArray> getSchedule(@Query("emp_id") int user_id);

    @POST(APIList.CREATE_SCHEDULE)
    Call<JsonObject> ceateSchedule(@Query("emp_id") int emp_id,
                                   @Query("boss_id") int boss_id,
                                   @Query("schedule_date_arr") String schedule_date_arr,
                                   @Query("schedule_id") String deleted_schedule);



    @POST(APIList.CREATE_USER_INDUCTION)
    Call<JsonObject> createUserInduction(@Query("user_name") String user_name, @Query("notes") String notes, @Query("date_of_join") String date, @Query("induction_id") int ind_id,@Query("distributor_name") String dis_name
    ,@Query("joinee_type") String joinee_type,@Query("dsr_type") String dsr_type,@Query("joinee_id") String joinee_id);

    @POST(APIList.CREATE_USER_EXIT)
    Call<JsonObject> userExit(@Query("emp_id") int empId, @Query("date") String date, @Query("notes") String notes, @Query("exit_id") int exit_id);

    @POST(APIList.CHANGE_DAY_STATUS)
    Call<JsonObject> changeDayStatus(@Query("id") int id, @Query("flag") int status);

    @POST(APIList.GET_OUTLETS)
    Call<JsonObject> getOutlets(@Query("emp_id") int empId);

    @POST(APIList.ADD_OUTLET)
    Call<JsonObject> addOutlet(@Query("outlet_id") int outlet_id,
                               @Query("schedule_id") int scheduleId,
                               @Query("rating") float outletRating,
                               @Query("notes") String notes,
                               @Query("spot_award") int spotAward,
                               @Query("spot_reason") String spotReason,
                               @Query("questions_array") String questionsArray);

    @POST(APIList.GET_ALL_SCHEDULED)
    Call<JsonObject> getAllScheduledOutlet(int userId);

    @POST(APIList.GET_QUESTIONARY)
    Call<JsonObject> getQuestionary();

    @POST(APIList.GET_QUESTIONARY_CATEGORY)
    Call<JsonObject> getQuestionaryCat();

    @POST(APIList.SUBMIT_FEEDBACK)
    Call<JsonObject> submitFeedback(@Query("schedule_id") int scheduleId,
                                    @Query("feedback_array") String feedbackArray,
                                    @Query("strength") String strength,
                                    @Query("development") String development);

    @POST(APIList.CHANGE_PASSWORD)
    Call<JsonObject> changePassword(@Query("id") int id,
                                    @Query("old_password") String oldPass,
                                    @Query("new_password") String newPass);

    @POST(APIList.DELETE_SCHEDULE)
    Call<JsonObject> deleteSchedule(@Query("id") int id,
                                    @Query("flag") int flag);

    @POST(APIList.GET_RECENT_FEEDBACK)
    Call<JsonObject> getRecentFeedback(@Query("salesmen_id") int empId);

    @POST(APIList.GET_OVERALL_DATA)
    Call<JsonObject> getOverAllData(@Query("emp_id") int empId);

    @POST(APIList.SEND_MAIL)
    Call<JsonObject> sendMail(@Query("emp_id") int userId,
                              @Query("user_id") int empId);

    @POST(APIList.LOGOUT)
    Call<JsonObject> logout(@Query("user_id") int userId );

    @POST(APIList.GET_QUESTION_RATING)
    Call<JsonObject> getQuestionsData();
}
