package com.mars.salestrainingapp.API;

/**
 * Created by surve on 16-Oct-17.
 */

public class APIList {

    public static final String BASE_URL = "http://52.172.26.109/api/";

    public static final String LOGIN = "login.php";
    public static final String FORGOT = "forget_password.php";
    public static final String RESET = "reset_password.php";
    public static final String USER_LIST = "user_list.php";
    public static final String USER_LIST_FOR_DSR = "user_list_new.php";
    public static final String PLAN_PERIOD = "configuration.php";
    public static final String CREATE_SCHEDULE = "create_schedule_changes.php";

    //create_user_induction.php
    public static final String CREATE_USER_INDUCTION = "create_induction.php";

    public static final String CREATE_USER_EXIT = "create_user_exit.php";
    public static final String GET_SCHEDULE = "get_schedule.php";
    public static final String CHANGE_DAY_STATUS = "change_day_status.php";
    public static final String GET_OUTLETS = "get_outlet.php";
    public static final String ADD_OUTLET = "add_outlet.php";
    public static final String GET_ALL_SCHEDULED = "";
    public static final String GET_QUESTIONARY = "get_questionnaire.php";
    public static final String GET_QUESTIONARY_CATEGORY = "get_questionnairy_category.php";

    public static final String SUBMIT_FEEDBACK = "store_questionnaire_response.php";

    public static final String CHANGE_PASSWORD = "change_password.php";

    public static final String DELETE_SCHEDULE = "delete_schedule.php";

    public static final String GET_RECENT_FEEDBACK = "get_recent_feedback_copy.php";

    public static final String GET_OVERALL_DATA = "get_performance_data.php";

    public static final String ALL_USER_LIST = "get_all_user.php";


    public static final String ALL_TARGET_USER_LIST = "get_target_details.php";

    public static final String SEND_MAIL = "send_mail_performace_data.php";

    public static final String LOGOUT = "logout.php";

    public static final String GET_QUESTION_RATING="get_outlet_rating_questions.php";
}
